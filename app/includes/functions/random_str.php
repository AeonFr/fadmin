<?php 
/**
 * Generate a random string, using a cryptographically secure 
	 * pseudorandom number generator (random_int)
	 * @param int $length      How many characters do we want?
	 * @param string $keyspace A string of all possible characters
	 *                         to select from
	 * @return string
 */
function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'){
	/**
	 * Require PHP < 7.0 polyfill for random_int()
	 */
	global $config;
	require_once "$config[root]/vendor/random_compat/lib/random.php";
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
}