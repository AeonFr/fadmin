
THE IDEA:
Varius CSV files are uploaded with the names "holdings", "ledgers", and "openposition". On the moment they're uploaded, this variable is loaded and the files are interpreted according to the content of this variable.

The interpretation, or "parsing", includes the following advantages:

- Filter some rows for a specific user, exclude those rows for other users.	E.g. Filter rows where "client_code" (value in "users" table) is equal to "Client" (value in the newly upload CSV file) 
- Upload tables with variable amount of columns without changing the database structure nor the Information Architecture
- The system admin could change this settings when the table structure changes, or create new tables,
  only by knowing a little bit of PHP syntax.
- The uploaded CSV files could be stored in the website or not, but what the user would really see is a "cached" version of those tables. There's no risk of data leaking simply because every user will be served with a unique and customized "version" of the original table.

The disadvantages of this are:

- A badly placed "comma" could cause a fatal error and the page would simply "break" completely the interface.

# Configure

To configure the table schema, simply update the file `table_schema.php`, until it looks something like the example above. In this example you will see "commented" lines with information about each variable and what does in mean. 

```
$table_schema = [
	# A unique identifier for each table. Can be anything. It's used for urls, so no spaces.
	"holdings" => [
		# what the user actually sees as the table name.
		"nicename" => "Holdings",
		# An icon's class of any of this font icon libraries: font awesome or ionicon.
		# A quick google search will get you to a full list of all the possible values.
		# The example above would output the following HTML: <i class="fa fa-line-chart"></i>
		# Resulting in this icon: http://fortawesome.github.io/Font-Awesome/icon/line-chart/
		"icon" => "fa fa-line-chart",
		# The user *needs* to have this permission to view data from this table.
		# This is mandatory, so if you wan't to make this table "open access" you will need to write a permission and then add it to all roles. 
		"permission_required" => "view_table_holdings",
		# the columns of the table that are going to be excluded. (they are never displayed on the page!)
		# I added an empty string ("") at the end just in case there's blank columns at the beginning of the file. Hadn't really tested if this would be a problem (so this string could be unnecessary).
		"exclude_columns" => ["Branch", "SB", "Client", "Client_Name", "ISIN", "Script Code", ""],
		# whether or not to filter content. either true or false. More explained below.
		"filter_content" => true,
		# user variables are "client_code", "email", "name", "phone_num", "client_pan" & "role"
		"filter_content_by_variable" => "client_code",
		# the columns are determined by the tables you will upload in the future.
		# uploaded tables, in form of .csv files, have a table "header" or "title",
		# that is a row that determines the title of each column.
		# This can be set every time you upload a table.
		"filter_content_column" => "Client"
		# If the row of the table you upload for "holdings" has a value in the column "Client" (the column with a table header of "Client") that matches "client_code" (the user variable), then the row will be displayed for user, else it won't. Of course this rules are only applied if "filter_content" is set to true.
	],
	"ledgers" => [
		"nicename" => "Ledgers",
		"icon" => "fa fa-line-chart",
		"permission_required" => "view_table_ledgers",
		"exclude_columns" => ["Client", "Client Name", "Client Type", "Priority", "Report type", ""],
		"filter_content" => true,
		"filter_content_by_variable" => "client_code",
		"filter_content_column" => "Client"
	],
	"openpositions" => [
		"nicename" => "Open Positions",
		"icon" => "fa fa-line-chart",
		"permission_required" => "view_table_openpositions",
		"exclude_columns" => ["Party Code", "Inst Type", ""],
		"filter_content" => false
	]
];
```