The javascript main's file contains an API to show modals and create Ajax requests.

In the future, it will have the ability to start a communication with the server and check for notifications and comments every certain amount of seconds.

# ajaxNvars
---
This class can create a request to the API, and render a handlebar template, and both (create a request and render a template acording to the response).  
Please, do check the [handlebars.js documentation](http://handlebarsjs.com) if you want to design templates.  
The advantage of using the `ajaxNvars.renderTemplate()` method instead of writing the handlebars code manually is that the class will hold internally a record of all the parsed templates so they are only parsed one time on page load.  
To declare a template, simply place the attribute `template` on any element, followed by a name. Use the same name to render that template.

```
<!-- template content -->

<script template="template-list" type="text/x-handlebars-template">
	<div class="box-body">
		{{#each list}}
		<div class="btn btn-default" id="{{id}}">{{role}}</div>
		{{/each}}
	</div>
</script>

<!-- where the template will be rendered -->

<div id="target"></div>

<!-- the data for the template and the call to render it -->

<script>
var data = {
	"list":
		{"id": 1, "role": "admin"},
		{"id": 2, "role": "user"}
};

//usage: renderTemplate(template_name, template_data, renderInSelector);
ajaxNvars.renderTemplate("template-list", data, "#target");

/* this alternative usage is also valid:*/
ajaxNvars.renderTemplate({
	"template": "template-list,
	"data": data, // (inyected into template)
	"selector": "#target"
});

</script>
```
The three elements outlined above (a template, a target selector, and the data) are all required to render a template via the `ajaxNvars.renderTemplate()` method. The example above would produce an output like the following in the "#target" selector.

```
<!-- the output -->

<div id="target">
	<div class="box-body">
		<div class="btn btn-default" id="1">admin</div>
		<div class="btn btn-default" id="2">user</div>
	</div>
</div>

```

The class is not only used to render templates, it's also used to send and receive Ajax requests. If you are familiar with jQuery's `$.post()`, this example is in fact a *proxy* function of this method, but includes error handling and makes a background check into the chat and notifications API, to use the same request to query notifications and chats.

```
ajaxNvars.request = function(endpoint, request_data, callback);
``` 
- **endpoint** is a url relative to "/app/API/" that can be called to retrieve some data.
- **request_data** is an object containing the keys that will consist of the $_POST variables.
- **callback** is a function that goes like this: `callback(data)` where data is the response from the server. If the server responded with a json object, this variable would be alread `JSON.parse()`'d


`ajaxNvars.requestNrender(config, callback)` is a function that simply links the last two together:
it will...

- In the selector in which data will be rendered, add the class `o-50` (opacity: 50)
- make a POST call to the API into `config.endpoint` passing `config.data` (if any).
- interpret the JSON response as a javascript object, and use it as the data to render the `config.template` template.
- Put the content of the rendered template in the selector, which is obtained from the `config.selector` variable. Also, remove the `o-50` class.

```
ajaxNvars.requestNrender({
	endpoint: "module/operation.json",
	template: "single",
	selector: "#target"
	}, function(response){ (...) });
```

# virtualForm
---
 [virtualForm description]
 class to render a "virtual form" that submits via ajax
 from certain attributes of the page.
 For example, the "endpoint" attribute on an form element
 determines that every child with a "name" attribute
 will be submitted via POST to that endpoint
 when the "[type='submit']" child is clicked.

Note that the [type="submit"] selector will also obtain
the "disabled" class while the form is being sent.

Additionally, we can create a [renderResponse] selector.
This selector, when matching [renderResponse=""], will simply output the hole responseText as innerHTML. Yet, if there's a different value for `renderResponse` **and** if responseText is an object (json response), the selector will render the object's attribute specified as value for `renderResponse`.

.

.

.

<style>
:root {
	--blockquote: #05CFB5;
}
a{ color: lightblue!important }
#preview{ line-height: 2 }
pre{ word-wrap: break-word; white-space: pre-wrap; line-height: 1.25; background-color: #202120 }
code{color: #eee;background-color: #212521;}
blockquote{ border-color: var(--blockquote); font-size: 1.1em; }
body, #preview{background-color: #121312; color: lightgreen; padding: 1rem; box-sizing:border-box; }</style>