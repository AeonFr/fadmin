<?php include "__header.php"; ?>
<p>Hi <strong><?php echo $vars['data']['to_name'] ?></strong>,
<br> You have a new message at
<a href="<?php echo $vars['config']["full_url"] ?>"><?php echo $vars['config']["page_title"] ?>.</a></p>
<div style="padding:12px;background:#eee">
	<p><b>From:</b> <?php echo $vars['data']['template_vars']['from'] ?></p>
	<p><?php echo $vars['data']['template_vars']['body'] ?></p>

</div>
<p>You can reply to this email by following this link: <a href="<?php echo $config['full_url'] ?>/messages" target="_blank">My Messages</a>.</p>
<small>To stop receiving notifications on new messages, <a href="<?php echo $vars['config']['full_url']    ?>/API/email-settings/?ref=<?php echo md5($vars['data']['to_email']) ?>&disable_chats_by_mail=true" target="_blank">please click here.</a></small>

<?php include "__footer.php"; ?>