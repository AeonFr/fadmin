<?php include "__header.php"; ?>
<p>Hi <?php echo $vars['data']['to_name'] ?>,
<br> I'm sending this email to notify of your new account at 
<a href="<?php echo $vars['config']["full_url"] ?>"><?php echo $vars['config']["page_title"] ?>.</a></p>
<p>Your username is: <b><?php echo $vars['data']['to_email'] ?></b>
</p>
<p>
Your password is: <b><?php echo $vars['data']['template_vars']['password'] ?></b>
</p>
<p>
This information is private and can be only viewed by you.
<br>
You can change your password once you are logged in in the site, by following this link: <a href="<?php echo $vars['config']["full_url"] ?>/my-account"><?php echo $vars['config']["full_url"] ?>/my-account</a>
</p>

<?php include "__footer.php"; ?>
<?php 
// For now, $subject is the only setting available :P
$subject = "This is your new account Information";
 ?>