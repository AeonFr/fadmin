<?php 
/**
 * Render the template inside a "black box" where $this is not accessible
 * Accessible variables inside templates? $vars['config'] and $vars['data']...
 * @return $data['html']
 */
function interpret_template($template, $vars){
	global $config;
	/*Generate a proxy name for vars, in case shit stops working...*/
	$data = $vars['data'];
	ob_start(null, 0,  PHP_OUTPUT_HANDLER_CLEANABLE ^ PHP_OUTPUT_HANDLER_REMOVABLE);
	include $config["root"]."/_noSQL/email_templates/". $template .".php";
	$return = [ "html"=>ob_get_contents() ];
	ob_end_clean();
	if (isset($subject))
		$return['subject'] = $subject;
	return $return;
}
/**
 * Send emails, store them in logs, retrieve those logs...
 */
class Email {
	function __construct(){
		global $config;
		$this->config = $config;
	}
	/**
	 * Send an email
	 * @param  [type] $data [description]
	 * data[to_email] = recipent's email
	 * data[to_name] = optionally, specify a name
	 * data[subject] = subject
	 * data[html] = (OPTIONAL IF text is set) HTML content of email, required if useTemplate=false
	 * data[text] = (OPTIONAL IF html is set) Text content of the email
	 * data[template] = IF SET, "html" and "text" are ignored, instead, the content of this file is evaluated.
	 * 					Pass a string with the name of the file as located in "includes/email_templates/"  folder, 
	 * 					without the (.php) extension
	 * 					Please notice you can place anything under "$data" object and make a template render it =)
	 */
	function send($transaction_name, $transaction_group, $data, $sendBCCToAdmin=true){
		require_once $this->config["root"]."/vendor/PHPMailer/PHPMailerAutoload.php";
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = $this->config["smtp_host"];
		$mail->SMTPAuth = $this->config["smtp_auth"];
		$mail->Username = $this->config["smtp_user"];
		$mail->Password = $this->config["smtp_pass"];
		$mail->SMTPSecure = $this->config["smtp_secure"];
		$mail->Port = $this->config["smtp_port"];
		if (isset($data['to_name']))
			$mail->addAddress($data['to_email'], utf8_decode($data['to_name']));
		else
			$mail->addAddress($data['to_email']);
		if (isset($this->config['test_email']) && $sendBCCToAdmin)
			$mail->addBCC($this->config['test_email']);//Send a blind carbon copy to the sender, to debug output
		$mail->setFrom($this->config["send_from_email"], utf8_decode($this->config['admin_name']));
		$mail->addReplyTo($this->config['send_from_email'], utf8_decode($this->config['admin_name']));
		$mail->CharSet = 'UTF-8';
		if (!isset($data['template'])){
			if (isset($data['html'])){
				$mail->isHTML(true);
				$mail->Body = $data['html'];
				if (isset($data['text'])) $mail->AltBody = $data['text'];
			}else{
				$mail->isHTML(false);
				$mail->Body = $data['text'];
			}
		}else{
			$mail->isHTML(true);
			$interpret_template = interpret_template($data['template'], array("config"=>$this->config, "data"=>$data));
			$data['html'] = $interpret_template['html'];
			$mail->Body = $data['html'];
			// if new subject is set, overwrite old subject
			if (isset($interpret_template['subject']))
				$data['subject'] = $interpret_template['subject'];
		}
		$mail->Subject = utf8_decode($data['subject']);
		

		if ($this->config["disable_all_email"] == true){
			$status = "Email disabled via \$config.";
		}else{
			if (!$mail->send()){
				$status = "Error sending: " . $mail->ErrorInfo;
			}else{
				$status = "No error.";
			}			
		}


		if (!$sendBCCToAdmin){
			// the data["html"] var is stripped because it can contain sensitive information,
			// like passwords.
			// This is why the $sendBCCToAdmin option is set as false.
			if (isset($data['html'])) unset($data['html']);
			if (isset($data['text'])) unset($data['text']);
			if (isset($data['template_vars'])) unset($data['template_vars']);
		}else{
			//else, it is stored but only as less than 1500 characters.
			if (isset($data['html'])){
				if (isset($data['text'])) unset($data['text']);
				$data['html'] = substr($data['html'], 0, 1500);
			}else{
				$data['text'] = substr($data['text'], 0, 1500);
			}
		}
		$transaction_id = $this->store_transaction_in_log($transaction_name, $transaction_group, $data, $status);
		return array(
			"transaction_id" => $transaction_id,
			"email"=> $data['to_email'],
			"status" => $status
		);
	}
	function bulkMail($to, $subject, $template, $transaction_group){
		$exceptions = array();
		$success = array();
		foreach($to as $to_arr){
			$data = array();
			$data['to_email'] = $to_arr['toemail'];
			if (!$this->is_subscribed($data['to_email'])){
				array_push($exceptions, array("status" => "user is unsubscribed", "email"=>$data['to_email']));
				continue;
			}
			if (isset($to_arr['toname'])) $data['to_name'] = $to_arr['toname'];
			$data['subject'] = $_POST['subject'];
			$data['template'] = $_POST['template'];
			if (endsWith($data['template'],'.php')) $data['template'] = substr($data['template'], 0, 4);
			try{
				array_push($success, $this->send($data['to_email'], $_POST['transaction_group'], $data));
			}catch(Exception $e){
				array_push($exceptions, $e->getMessage());
			}
		}
		return ["success" => $success, "error" => $exceptions];
	}
	function is_subscribed($email){
		global $db;
		global $module;
		$get = $module['users']->getUsersBy('email', $email);
		if(count($get)==0){
			//check inside the _noSQL/unsubscribes.json file. Read the documentation on email templating (at the end: bulkmail section) on a brief explanation of this database and why is it necessary.
			require_module('unsubscribe-for-unregistered-users');
			return $module['unsubscribe4unregistered']->is_subscribed(md5($email));
		}else{
			$user = $get[0];
			if ($module['users']->get_meta($user['client_code'], 'unsuscribe_from_mailing_list')) return false;
			else return true;
		}
	}
	function store_transaction_in_log($transaction_name, $transaction_group, $data, $status){
		global $db;
		$db->query_escaped(
		"INSERT INTO email_transactions
		(transaction_name, transaction_group, transaction_data, status, sent_at)
		VALUES
		(?,?,?,?,?)",
		array(
		$transaction_name, $transaction_group, json_encode($data), $status, date('Y-m-d H:i:s')
		));
		return $db->lastInsertId();
	}
	function get_logs(){
		global $db;
		return $db->getAssoc("SELECT * FROM email_transactions ORDER BY sent_at DESC");
	}
	function get_transaction($transaction_id){
		global $db;
		$stm = $db->getAssoc("SELECT * FROM email_transactions WHERE transaction_id=? LIMIT 1", array($transaction_id));
		return $stm[0];
	}
	/**
	 * @param  object $options
	 * ["list"] ->if false, the list of transactions under each group won't be retrieved.If set to the name of a group of transactions, then the only thing returned will be the list of that group.
	 * ["html"] ->if false, the html of each individual transaction under the list of each transaction group will be omitted. This can save a lot of bloatware in the request - html strings are generally really long. If set to the id of a transaction, only the html content of that transaction will be returned.
	 */
	function fetchLogs($options){
		$allData = $this->get_logs();
		$groupedData = array();
		foreach($allData as $indexOf => $value){
			$i = $value['transaction_group'];
			$value['transaction_data'] = json_decode($value['transaction_data'], true);
			if (isset($options['html']) && $options['html'] != false){
				if ($options['html']==$value['transaction_id']){
					return isset($value['transaction_data']['html']) ? $value['transaction_data']['html'] : '';
				}else continue;
			}
			if (!isset($groupedData[$i])){
				$groupedData[$i] = array(
					"group_name" => $i,
					"count" => 1,
					"template_used" => $value['transaction_data']['template'],
					"last_sends" => array(
						"date"=>$value['sent_at'],
						"count" => 1
					)
				);
			}else{
				// add up to the global count
				$groupedData[$i]['count']++;
				if (isset($value['transaction_data']['template']) && $groupedData[$i]['template_used'] !== $value['transaction_data']['template']){
					// more than one template was used in this group of transactions...
					$groupedData[$i]['template_used'] = ">1"; }
				if ($value['sent_at'] > $groupedData[$i]['last_sends']['date']){
					// more recent, override
					$groupedData[$i]['last_sends'] = array(
						'date' => $value['sent_at'],
						'count' => 1
					);
				}else if ($value['sent_at'] == $groupedData[$i]['last_sends']['date']){
					//equal, add +1 to count
					$groupedData[$i]['last_sends']['count']++;
				}
			}
			// now that value was created (in case it weren't) or updated, let's add the item to the list
			if (!isset($options['list']) || $options['list']!=false){
				$list_item = array(
					'transaction_id' => $value['transaction_id'],
					'date' => $value['sent_at'],
					'transaction_name' => $value['transaction_name'],
					'to' => array( 'email'=>$value['transaction_data']['to_email'] ),
					'subject' => $value['transaction_data']['subject'],
					'status' => $value['status']
				);
				if (isset($value['transaction_data']['to_name'])) {
					$list_item['to']['name'] = $value['transaction_data']['to_name']; }
				if (isset($value['transaction_data']['template'])) {
					$list_item['template'] = $value['transaction_data']['template']; }
				if (isset($value['transaction_data']['html'])
					&& isset($options['html']) && $options['html'] == false ) {
					$list_item['html'] = $value['transaction_data']['html']; }
				if (!isset($groupedData[$i]['list'])){
					$groupedData[$i]['list'] = array( $list_item );
				}else array_push($groupedData[$i]['list'], $list_item);
			}
		}
		if (isset($options['list']) && $options['list']!=false) return $groupedData[$options['list']]['list'];
		return $groupedData;
	}
}

$module['email'] = new Email();