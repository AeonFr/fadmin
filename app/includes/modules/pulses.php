<?php
function humanTiming ($time){
    $time = time() - strtotime($time); // to get the time since that moment

    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second');

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }
}
/**
 * Send and fetch notifications and chat messages
 */
class Pulses {
	function __construct(){
		global $config;
		$this->config = $config;
	}
	function get_name_of($client_code){
		global $db;
		$result = $db->getAssoc("SELECT name FROM users WHERE client_code=?", $client_code);
		if (!isset($result[0])) return false;
		return $result[0]['name'];
	}
	function user_is_active($client_code){
		/**
		 * If the user that is receiving the notification hasn't been active in the last minute,
		 * send him the notification via email
		 */
		//	- "pulse" updates auth_data every 15 - 18 seconds by default... (actually, this is set via $config['pulse_frequency'] - in milliseconds) We are counting on that to know if a user is off-line.
		//	- A request to the pulse endpoint might take 2 - 3 seconds, being a little bit conservative.
		//	We dont' want to annoy the user unnecessarily, but we also don't want him to miss things out!
		$seconds_since_last_pulse_timeframe = ($this->config['pulse_frequency'] / 1000 )+1;/*convert pulse_frequency to seconds and add 1 second just in case*/
		//	"less than" in SQL's time objects
		//http://stackoverflow.com/questions/4541205/counting-rows-where-a-timestamp-is-less-than-24-hours-old
		global $db;
		$check_if_user_is_active = $db->num_rows("SELECT * FROM user_auth_data WHERE client_code=? AND last_request >= DATE_SUB(NOW(), INTERVAL ". $seconds_since_last_pulse_timeframe ." SECOND)", array($client_code));
		return $check_if_user_is_active;
	}
	/**
	 * [send] ~ Send a NOTIFICATION or CHAT MESSAGE to a user or group of users
	 * data[sender] = client code of the sender. If it's omitted, it will create a notification (instead of a chat).
	 * data[receiver] = client code of the receiver. If this is an array, multiple users will receive it
	 * data[body] = the body of the chat message or notification.
	 * data[meta] = a key/value array that can set metadatas for the recently added pulse. This metadatas are stored in a separate table but are retrieved by getPulse function.
	 * 
	 */
	public function send($data){
		global $db;
		global $module;
		$sender_client_code = (isset($data['sender'])) ? $data['sender'] : "";
		$body = $data['body']; // if this value needs to be echoed in handlebars, using {{{}}} will prevent handlebars from escaping HTML tags. in a future if html is ever needed it may be best to invalidate script tags via a regex
		$timestamp = date('Y-m-d H:i:s');
		$SQL = "INSERT INTO pulses (sender, receiver, body, was_received, was_read, sent_at) VALUES (?,?,?,?,?,?)";
		if (is_array($data['receiver'])){
			$pulse_ids = array();
			foreach($data['receiver'] as $receiver_client_code){
				if ($receiver_client_code == '') continue;
				$clone_data = $data;
				$clone_data['receiver'] = $receiver_client_code;
				array_push($pulse_ids,$this->send($clone_data));
			}
			return $pulse_ids;
		}else{
			$db->query_escaped($SQL,array($sender_client_code, $data['receiver'], $body,0,0, $timestamp));
		}
		$pulse_id = $db->lastInsertId();
		// lastInsertId() was resulting in 0 every time... so here's a polyfill
		if ($pulse_id == 0){
			$query_pulse_id=$db->getAssoc("SELECT pulse_id FROM pulses WHERE sender=? AND receiver=? AND body=? AND sent_at=? LIMIT 1", array($sender_client_code, $receiver_client_code, $body, $timestamp));
			$pulse_id = $query_pulse_id[0]['pulse_id'];
		}
		if (!isset($data['meta'])) return $pulse_id;
		foreach($data['meta'] as $key=>$value){
			$this->setMeta($pulse_id, $key, $value);
		}
		//...
		return $pulse_id;
	}
	private function setMeta($pulse_id, $meta_key, $meta_value){
		global $db;
		$db->query_escaped("INSERT INTO pulse_meta (pulse_id, meta_key, meta_value) VALUES (?,?,?)",
		array($pulse_id, $meta_key, $meta_value));
		return true;
	}
	private function getMeta($pulse_id, $meta_key){
		global $db;
		$assoc = $db->getAssoc("SELECT meta_value FROM pulse_meta WHERE pulse_id=? AND meta_key=?", array($pulse_id, $meta_key));
		if (count($assoc) == 0) return false;
		if (count($assoc) == 1) return $assoc[0]['meta_value'];
		$return = array();
		foreach ($assoc as $key=>$value) {
			array_push($return, $value['meta_value']);
		}
		return $return;
	}
	/**
	 * Query the last pulses of type...
	 * @param  array $options
	 *         ["chat"] => boolean, whether to query for chats or not
	 *         ["chats_with"] => filter by rows where receiver is equal to...
	 *         (IN ADITTION TO THE CURRENT USER... USED TO QUERY CHATS WITH OTHER USERS)
	 *         ["distinct_sender"] => set it as whatever you like or don't mention it,
	 *         				determines if chats are grouped by sender
	 *         				(and only last chat of each sender is returned)
	 *         ["limit"] => integrer, the number of pulses to query (default: last 10 pulses)
	 *         ["only_read"] => to return only read messages?
	 *         ["only_unread"] => to return only unread messages? ignored if only_read
	 *         ["only_received"] => to return only received messages?
	 *         ["only_unreceived"] => to return only unreceived messages? ignored if only_received
	 * 
	 * @return associative array
	 */
	function getLast($options){
		global $db;
		$client_code = $_SESSION['user']['client_code'];
		if ($options["chat"] && isset($options['chats_with'])){
			$whereSender =  "(sender='" .$options['chats_with']. "' AND receiver='$client_code') OR (sender='$client_code' AND receiver='" .$options['chats_with']. "')";
		}else if($options["chat"] && !isset($options['chats_with'])){
			$whereSender = "(sender!='' AND receiver='$client_code') OR (sender='$client_code')";
		}else{
			$whereSender =  "sender='' AND receiver='$client_code'";
		}

		$limit = isset($options["limit"]) ? $options["limit"] : 10;
		$whereRead = isset($options['only_unread']) ? "was_read=0" : "";
		$whereRead = isset($options["only_read"]) ? "was_read=1" : $whereRead;
		$whereReceived = isset($options["only_unreceived"]) ? "was_received=0" : "";
		$whereReceived = isset($options["only_received"]) ? "was_received=1" : $whereReceived;
		if (isset($options["distinct_sender"])){
			// only the last message from each sender...
			if ($whereRead!='') $whereRead .=" AND";
			if ($whereReceived!='') $whereReceived .= " AND";
			$SQL = "SELECT DISTINCT sender FROM pulses WHERE $whereRead $whereReceived (sender!='' AND receiver=?) OR (sender=?) LIMIT $limit"; /*when sender was equal to receiver, the chat arrived every time... wtf?*/
			$this->sql = $SQL;
			$query = $db->getAssoc($SQL, array($client_code,$client_code));
			$return = array();
			foreach($query as $row){
				$sender = $row['sender'];
				// get only the last one
				$push = $db->getAssoc("SELECT * FROM pulses WHERE $whereRead $whereReceived (sender='$sender' AND receiver=?) OR (sender=? AND receiver='$sender') ORDER BY sent_at DESC LIMIT 1", array($client_code, $client_code));
				if (isset($push[0]))
					array_push($return,$push[0]); 
			}
			return $return;
		}else{
			if ($whereRead!="") $whereRead = " AND ".$whereRead;
			if ($whereReceived!="") $whereRead .= " AND";
			$SQL = "SELECT * FROM (SELECT * FROM pulses WHERE $whereSender $whereRead $whereReceived ORDER BY sent_at DESC LIMIT $limit) as alias ORDER BY alias.sent_at";
			if ($options['chat']==false) $SQL .= " DESC";
		}
		$this->sql = $SQL;
		$return = $db->getAssoc($SQL);
		return $return;
	}
	public function markAllAsRead($is_chat=true, $custom_where=""){
		global $db;
		$where = "sender=''";
		if ($is_chat) $where = "sender!=''";
		$client_code = $_SESSION['user']['client_code'];
		$db->query_escaped("UPDATE pulses SET was_read=1 WHERE receiver=? AND $where $custom_where", $client_code);
		return true;
	}
	public function getCacheOBJ($requestTemplates=false, $onlyChanges=false){
		global $db;
		$chats_list = array();
		$chats_label = array();
		$notifs_list = array();
		/**
		 * NOTIFICATIONS
		 */
		if ($onlyChanges) $notifs = $this->getLast(["chat"=>false,"only_unreceived"=>true]);
		else $notifs = $this->getLast(["chat"=>false,"only_unread"=>true]);
		foreach($notifs as $notif){
			array_push($notifs_list, $this->buildNotifRow($notif));
		}
		$notifs_count = $db->num_rows("SELECT pulse_id FROM pulses WHERE sender='' AND was_read=0 AND receiver=?", $_SESSION['user']['client_code']);
		// if this function is being called, the user is online and received all his pulses...
		$db->query_escaped("UPDATE pulses SET was_received=1 WHERE sender='' AND was_received=0 AND receiver=?", $_SESSION['user']['client_code']);
		/**
		 * CHATS
		 */
		if ($onlyChanges) $chats = $this->getLast(["chat"=>true, "distinct_sender"=>true, "only_unreceived"=>true]);
		else $chats = $this->getLast(["chat"=>true, "distinct_sender"=>true]);
		//die(var_dump($this->sql));
		foreach($chats as $chat){
			$chatRow = $this->buildChatRow($chat);
			if ($chatRow) array_push($chats_list, $chatRow);
			$chatLabel = $this->buildChatLabel($chat);
			if ($chatLabel) $chats_label = array_merge($chats_label, $chatLabel);
		}
		$chats_count = $db->num_rows("SELECT pulse_id FROM pulses WHERE sender!='' AND was_read=0 AND receiver=?", $_SESSION['user']['client_code']);
		// if this function is being called, the user is online and received all his pulses...
		$db->query_escaped("UPDATE pulses SET was_received=1 WHERE sender!='' AND was_received=0 AND receiver=?", $_SESSION['user']['client_code']);
		
		if ($onlyChanges){
			/**
			 * MINIMIZE DATA (on mobile phones) by sending only what's essential. everything else can be inferred.
			 */
			$return_chats = array();
			$return_notifs = array();
			if ($chats_count != 0) $return_chats["count"] = $chats_count;
			if ($notifs_count != 0) $return_notifs["count"] = $notifs_count;
			if ($chats_list != array()) $return_chats["list"] = $chats_list;
			if ($chats_label != array()) $return_chats["label"] = $chats_label;
			if ($notifs_list != array()) $return_notifs["list"] = $notifs_list;
			if ($return_chats !== array())
				$return = array("chats"=>$return_chats);
			if ($return_notifs !== array()) 
				$return["notifs"] = $return_notifs;
			if (isset($return)) return $return;
			else return [];
		}

		$return = [
			"chats"=>[
				"count"=>$chats_count,
				"list"=>$chats_list,
				"label"=>$chats_label
			],
			"notifs"=>[
				"count"=>$notifs_count,
				"list"=>$notifs_list
			]
		];

		//request the templates for "chat" and "notifications"? (done only once per session).
		if ($requestTemplates){
			global $config; // so the variable can be used inside this templates...
			ob_start();
			include $this->config["root"]."/pages/layout/pulse-template-chats.php";
			$chats = ob_get_clean();
			ob_start();
			include $this->config["root"]."/pages/layout/pulse-template-notifs.php";
			$notifs = ob_get_clean();
			$return["templates"] = [
				"chats" => $chats,
				"notifs" => $notifs
			];
		}

		return $return;
	}
	function getChanges(){
		global $db;
		return $this->getCacheOBJ(false, true); /*request only changes from the cacheOBJ*/
	}
	private function numUnreadChats($from){
		global $db;
		return $db->num_rows("SELECT pulse_id FROM pulses WHERE was_read=0 AND sender=? AND receiver=?", array($from,$_SESSION['user']['client_code']));
	}
	/**
	 * This functions accept a $row as argument
	 */
	function buildNotifRow($notif){
		$return = array();
		if ($notif["was_read"]==1) $return["read"] = true;
		$return["sent_at"] = humanTiming($notif["sent_at"]);
		$return["body"] = $notif["body"];
		if ($this->getMeta($notif["pulse_id"], "icon") != false) $return["icon"] = $this->getMeta($notif["pulse_id"], "icon");
		if ($this->getMeta($notif["pulse_id"], "link") != false) $return["link"] = $this->getMeta($notif["pulse_id"], "link");
		if ($this->getMeta($notif["pulse_id"], "custom_html") != false) $return["custom_html"] = $this->getMeta($notif["pulse_id"], "custom_html");
		return $return;
	}
	function buildChatRow($chat){
		global $db;
		$return = array();
		$return["sent_at"] = humanTiming($chat["sent_at"]);
		$return["body"] = $chat["body"];
		$return["sender_code"] = $chat["sender"];
		$query_sender_name = $this->get_name_of($chat['sender']);
		if (!$query_sender_name) /*user was deleted...?*/ return false;
		$return["sender"] = $query_sender_name;
		return $return;
	}
	// chat label is a resume of the last chats with each user, in a way that is presentable for the "taskbar"
	function buildChatLabel($chat){
		global $db;
		$client_code = $_SESSION['user']['client_code'];
		$return = array();
		$chat_with = ($chat['sender'] == $client_code) ? $chat['receiver'] : $chat['sender'];
		$chat_data = array();
		$chat_data["ClC"] = $chat_with;
		if (has_permission("chat_with_all")) $chat_data["is_online"] = $this->user_is_active($chat_with);
		$chat_data["name"] = $this->get_name_of($chat_with);
		$chat_data["last_msg_of"] = $this->get_name_of($chat['sender']);
		if (!$chat_data["last_msg_of"]) return false; /* user doesn't exist */
		$chat_data["last_msg_of"] = explode(" ", $chat_data["last_msg_of"])[0];
		if ($chat_data["name"] == $chat_data["last_msg_of"]) $chat_data["last_msg_of"] = "You";
		$chat_data["last_msg_fragment"] = strlen($chat['body']) > 30 ? substr($chat['body'], 0, 30)."..." : $chat['body'];
		$chat_data["sent_at"] = humanTiming($chat["sent_at"]);
		$chat_data["unread_count"] = $this->numUnreadChats($chat_with);
		if ($chat_data["unread_count"] == 0) unset($chat_data["unread_count"]);
		$return[$chat_with] = $chat_data;
		return $return;
	}
}

$module['pulses'] = new Pulses();