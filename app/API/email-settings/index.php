<?php 
require_once '../../config.php';
require_once "$config[root]/includes/general-functions.php";

if (isset($_REQUEST['unsubscribeFromAll'])){
	$_REQUEST['unsubscribe_from_mailing_list']=true;
	$_REQUEST['disable_notifs_by_mail']=true;
	$_REQUEST['disable_chats_by_mail']=true;
}

/**
 * This module can be accessed both by logged in as not logged in users.
 * It's a simple interface to output an "unsuscribe" button on emails.
 * This prevents users to attaching the address as SPAM, so it's great practice
 * if you are looking for a sustainable way to deliver emails.
 * We can check which user settings is relative to this page by:
 * - checking $_SESSION['user']['code'] (only used for logged in users: they can access this page from a link in "/my-account")
 * OR (ELSE)
 * - checking the $_GET['ref']=md5($email); and retrieving the client_code with that value (this method is slower)
 */
// PDO connection
require_once "$config[root]/includes/classes/database.php";
$db = new database("mysql:host=$config[db_host];dbname=$config[db_table]", $config["db_user"], $config["db_password"]);
$db->query("SET time_zone = '$config[sql_time_zone]'");
//is session set?
if (!empty($_SESSION) && isset($_SESSION['user']) && isset($_SESSION['user']['client_code'])){
	//user exists.
	$userData = $_SESSION['user'];
}else if (isset($_GET['ref'])){
	$query_all_users = $db->getAssoc("SELECT client_code, email FROM users");
	foreach ($query_all_users as $user) {
		if ($_GET['ref'] == md5($user['email'])){
			$userData = $module['users']->getUserData($user['client_code']);
		}
	}
	if (!isset($userData)){
		include "unsubscribe-for-unregistered-users.php";
		exit();
	}
}

if (!isset($userData)){
	//wtf? no user with that $_GET['ref'], or session is not active
	die('
	<h1>Fatal error</h1>
	<p>Your request is invalid. That is all we know.</p>
	');
}

$client_code = $userData['client_code'];
 /*
READ TO AVOID CONFUSION:
Settings are "inversed": a setting checked when a user has that option as FALSE. That's because, in the database,
THIS SETTINGS ARE STORED INVERSELY AS WHAT THEY ARE "NAMED" IN THE GUI. "DISABLE-NOTIFS-BY-MAIL" WILL BE NAMED AS
"SEND ME AN EMAIL WHEN I RECEIVE A NEW NOTIFICATION", AND SO ON.
IN DATABASE, VALUES ARE NOT SET BY DEFAULT. SO, BY DEFAULT, "DISABLE-NOTIFS-BY-MAIL" WILL BE FALSE BECAUSE THAT
META_KEY IS NOT SET WHEN THE USER IS REGISTERED, SO BY DEFAULT THEY ARE *NOT* *DISABLED*
 */
global $module;require_module('users');
/**
 * GET THE DATA
 * @var array
 */
$user_settings = array(
	"disable_notifs_by_mail" => $module['users']->get_meta($client_code, "disable_notifs_by_mail"),
	"disable_chats_by_mail" => $module['users']->get_meta($client_code, "disable_chats_by_mail"),
	"unsuscribe_from_mailing_list" => $module['users']->get_meta($client_code, "unsuscribe_from_mailing_list")
);
/**
 * PROCESS THE FORM.
 */
foreach($user_settings as $key => $value){
	if (isset($_REQUEST[$key])){
		$SUCCESS_MESSAGE = "Your preferences have been updated.";
		//update both in the database and in user_settings
		$module['users']->set_meta($client_code, $key, $_REQUEST[$key]);
		$user_settings[$key] = $_REQUEST[$key];
	}
}


// Detect the ?source GET parameter, and write the data in a file under the noSQL folder. This is just for testing and to collect data, it's not important for the site functionality. This is intended to provide feedback on how users reach the pages they reach.
if (isset($_GET['source'])){
	$fileAddr = "$config[root]/_noSQL/click_sources.json";
	$json = json_decode(file_get_contents($fileAddr), true);
	$host = $_SERVER['HTTP_HOST'];
	$request = str_replace($_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);
	if (!isset($json[$host])) $json[$host] = array();
	if (!isset($json[$host][$request])) $json[$host][$request] = array();
	if (!isset( $json[$host][$request][$_GET['source']] )) $json[$host][$request][$_GET['source']] = 1;
	else $json[$host][$request][$_GET['source']] += 1;

	$file = fopen($fileAddr, 'w');
	fwrite($file, json_encode($json, JSON_PRETTY_PRINT));
	fclose($file);
}



if (!isset($_SESSION['user']))
{
	require_once "$config[root]/layout.php";	
	include "form.php";
}
else
{
	ob_start();
	include "form.php";
	$layout = [
	"header" => "a"
	];
	$layout['content'] = ob_get_clean();
	include "$config[root]/layout_adminLTE.php";
}