<?php
/**
 * Authentification / session handling class
 */

require_module('users');

class Auth {
	function __construct (){}
	public function sign_in(){
		if (empty($_POST) || !isset($_POST['user']) || !isset($_POST['user']['email']) || !isset($_POST['user']['password'])) return "You can't log in because your request is missing essential fields: email and passord are required!";
		global $config;
		global $db;
		$query_rows = $db->query("SELECT client_code, email, md5_hash FROM users");
		foreach ($query_rows as $row){
			if ($_POST['user']['email'] !== $row['email']) continue;
			if (md5($_POST['user']['password']) !== $row['md5_hash']) continue;
			// user and password match ☺
			$client_code = $row['client_code'];
			$email = $_POST['user']['email'];
			require_once "$config[root]/includes/functions/random_str.php";
			$auth_key = random_str(15);
			// set the session cookie. This will be useful to check if user is loged in.
			$cookie_expire = 0;
			if (isset($_POST['user']['remember']) && $_POST['user']['remember'] == '1')
				$cookie_expire = time()+86400*30; // 30 days
			setcookie($auth_key, md5( $client_code . $email ), $cookie_expire, $config['base_url']);
			// update the user's "auth_data" in database
			$db->query_escaped(
			"INSERT INTO user_auth_data (client_code, auth_key, last_request, user_agent)
			VALUES (?, ?, ?, ?)",
			array($client_code,	$auth_key,	date('Y-m-d H:i:s'), $_SERVER['HTTP_USER_AGENT']));
			return $client_code; //it's a match ♥
		}
		return "Incorrect username or password";
	}
	private function authentificate(){
		global $config;
		global $db;
		// before loging the user in, we will delete all the auth_keys that have expired (more than 30 days without any activity)
		$db->query("DELETE FROM user_auth_data WHERE last_request < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY))");
		// Now, we will query all the active users
		$query_auth_data = $db->getAssoc("SELECT client_code, auth_key FROM user_auth_data");
		foreach ($query_auth_data as $auth_row) {
			//To check if the user is logged in, first:
			//	- Compare the auth data && cookie name
			$client_code = $auth_row['client_code'];
			$auth_key = $auth_row['auth_key'];
			if (!isset($_COOKIE[$auth_key])) continue;
			$query_user = $db->getAssoc("SELECT email FROM users WHERE client_code=? LIMIT 1", array($client_code));
			$user = $query_user[0];
			$email = $user['email'];
			if ($_COOKIE[$auth_key] !== md5( $client_code . $email )) continue;
			// it's a match! ♥
			$db->query_escaped("UPDATE user_auth_data SET last_request=? WHERE client_code=?", array(date('Y-m-d H:i:s'), $client_code));
			//this variable might get unset by a session expiring sooner than a cookie...
			if (!isset($_SESSION['user'])){
				global $module;
				require_module('users');
				$module['users']->getUserData($client_code);
			}
			return true;
		
		}
		return false; // best luck next time! ♦♣♠
	}
	public function authentificateOrRequestSignInForm(){
		global $config;
		if (!$this->authentificate()){
			global $log_in_error;
			include "$config[root]/pages/sign-in.php";
			exit();
		}else{
			// ...
		}
	}
	/**
	 * Log any user out, for the current device (user_agent).
	 * If the user_agent doesn't match with any active session, there's a change
	 * the user has updated his browser and this value changed.
	 * So we simply delete all the sessions assigned to that user.
	 */
	public function log_out(){
		global $db;
		global $config;
		$client_code = (isset($_SESSION['user']['client_code'])) ? $_SESSION['user']['client_code'] : false;
		if (!$client_code){
			// unset all cookies
			if (isset($_SERVER['HTTP_COOKIE'])) {
			    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			    foreach($cookies as $cookie) {
			        $parts = explode('=', $cookie);
			        $name = trim($parts[0]);
			        setcookie($name, '', time()-1000);
			        setcookie($name, '', time()-1000, '/');
			    }
			}
			return;
		}
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$query_auth_data = $db->getAssoc("SELECT auth_key FROM user_auth_data WHERE client_code='$client_code'");
		foreach ($query_auth_data as $row) {
			//delete the user authentification data
			$db->query("DELETE FROM user_auth_data WHERE client_code='$client_code'");
			//unset the cookie
			$auth_key = $row['auth_key'];
			unset($_COOKIE[$auth_key]);
			setcookie($auth_key, null, time()-3600, $config['base_url']);
			setcookie($auth_key, null, time()-3600);
		}
		//destroy session
		session_destroy();
		// redirect / refresh page
		header("Location: $config[base_url]");
		exit();
	}
}