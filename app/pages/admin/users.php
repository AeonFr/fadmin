<?php
$template['header'] = "Users";
$template['breadcrumb'] = ["Admin" => "/admin"];
if (!has_permission("manage_other_users")){ die("You don't have permission to access this page"); }
// this variables will be handy
$allUserData = $module['users']->listAll();
require_module('permissions');
$allRoles = $module['permissions']->listAll();
?>
<script template="list" type="text/x-handlebars-template">
	<div class="box box-solid box-default">
		<div class="box-header with-border">
			All users
		</div>
		<div class="box-body">
			<div class="flex">
				<div><div class="input-group">
	              <span class="input-group-btn">
	                <button type="button" class="btn btn-flat"><i class="fa fa-search"></i></button>
	              </span>
		          <input type="search" name="q" class="form-control" placeholder="Search...">
		        </div></div>
				<a onclick="add_user()" class="btn btn-success btn-flat mx1 inline-block"><i class="fa fa-plus"></i> New User</a>
			</div>
			<div class="table-responsive my2">
				<table id="search-valid" class="table table-stripped">
					<tr>
						<th style="width:100px">Client Code</th>
						<th>Name</th>
						<th>Email</th>
						<th><i class="fa fa-phone black-50"></i> Phone</th>
						<th style="width:110px">PAN</th>
						<th style="width:150px">Role</th>
						<th style="width: 140px">Admin stuff</th>
					</tr>
					{{#each .}}
					<tr class="search-valid">
						<td class="nowrap search-valid">{{client_code}}</td>
						<td class="nowrap search-valid">{{name}}</td>
						<td class="nowrap search-valid">{{email}}</td>
						<td class="nowrap search-valid">{{phone_num}}</td>
						<td onclick-search="{{client_pan}}" class="nowrap search-valid pointer text-primary">{{client_pan}}</td>
						<td onclick-search="#{{role}}" class="nowrap search-valid pointer text-primary">#{{role}}</td>
						<td class="nowrap">
							<a data-edit="{{client_code}}" class="pointer text-success px1 inline-block"><i class="fa fa-pencil"></i> Edit</a>
							{{#xif " this.client_code !== '<?php echo $_SESSION['user']['client_code'] ?>' "}}
								<a data-confirm-remove="{{client_code}}" class="pointer text-warning px1 inline-block"><i class="fa fa-trash"></i> Remove</a>
								{{else}}
								<?php // can't delete one self... ?>
								<a href="<?php echo $config['base_url'] ?>/my-account">My Account</a>
								{{/xif}}
						</td>
					</tr>
					{{/each}}
				</table>
			</div>
		</div>
	</div>
</script>
<script template="single" type="text/x-handlebars-template">
	<div class="btn btn-flat btn-default my2" onclick="list_users()"><i class="fa fa-arrow-left"></i> Back to user list</div>
	<form id="edit-single" endpoint="users/edit.json" class="box box-solid box-default">
		<div class="box-header">
			Edit <b>{{name}}</b>
		</div>
		<div class="box-body">
			<div class="form-group">
				Code: {{client_code}}
				<input type="hidden" name="client_code" value="{{client_code}}">
			</div>
			<div class="form-group">
				Name:
				<input type="text" name="name" value="{{name}}" required class="form-control">
			</div>
			<div class="form-group">
				Email:
				<input type="email" name="email" value="{{email}}" required class="form-control">
			</div>
			<div class="form-group">
				PAN: <small class="black-50">Optional</small>
				<input type="text" max="25" list="client_pan" name="client_pan" value="{{client_pan}}" class="form-control">
			</div>
			<div class="form-group">
				Phone Number: <small class="black-50">Optional</small>
				<input type="tel" max="25" name="phone_num" value="{{phone_num}}" class="form-control">
			</div>
			<div class="form-group alert">
				<i class="fa fa-lock"></i> Role:
				<select name="role">
					<option disabled>Choose an option</option>
					<?php foreach($allRoles as $role): ?>
					<option value="<?php echo $role['role'] ?>" {{#xif " this.role=='<?php echo $role['role'] ?>' "}} selected {{/xif}}>
						<?php echo $role['role'] ?>
					</option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group well">
				<label class="block"><input type="checkbox" name="send_password_by_email" value="true"> ¿Do you want to send a new password via email to {{email}}?</label>
				<small class="black-50">* If you leave this empty but the box above is checked, a new password will be generated randomly. To send the already existing password is impossible, because they're stored in a MD5 hash (impossible to decode).</small>
				<input type="password" name="password" placeholder="Enter New Password, Or leave blank to generate it randomly" class="form-control">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
			</div>
		</div>
	</form>
	<datalist id="client_pan">
		<?php foreach($allUserData as $user): ?>
		<option value="<?php echo $user['client_pan'] ?>" />
		<?php endforeach; ?>
	</datalist>
</script>
<!-- Add A New User (Handlebars Template) -->
<script template="add" type="text/x-handlebars-template">
		<div class="btn btn-flat btn-default my2" onclick="list_users()"><i class="fa fa-arrow-left"></i> Back to user list</div>
		<form id="add-single" endpoint="users/add.json" class="box box-solid box-success">
		<div class="box-header">
			<i class="fa fa-user"></i> New User
		</div>
		<div class="box-body">
			<div class="form-group">
				Code<span class"text-warning">*</span>: <small class="black-50">Must be unique</small>
				<input type="text" autocomplete="off" name="client_code" value="" required class="form-control">
			</div>
			<div class="form-group">
				Name<span class"text-warning">*</span>:
				<input type="text" name="name" value="" required class="form-control">
			</div>
			<div class="form-group">
				Email<span class"text-warning">*</span>:
				<input type="email" name="email" value="" required class="form-control">
			</div>
			<div class="form-group">
				PAN: <small class="black-50">Optional</small>
				<input type="text" max="25" list="client_pan" name="client_pan" class="form-control">
			</div>
			<div class="form-group">
				Phone Number: <small class="black-50">Optional</small>
				<input type="tel" max="25" name="phone_num" class="form-control">
			</div>
			<div class="form-group alert">
				<i class="fa fa-lock"></i> Role<span class"text-warning">*</span>:
				<select name="role" required>
					<option disabled selected>Choose an option</option>
					<?php foreach($allRoles as $role): ?>
					<option value="<?php echo $role['role'] ?>">
						<?php echo $role['role'] ?>
					</option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group">
				Password:
				<input type="password" name="password" placeholder="Enter New Password, Or leave blank to generate it randomly" class="form-control">
				<small class="black-50">* If you leave this empty, a new password will be generated randomly.</small>
				<label class="block"><input checked type="checkbox" name="send_password_by_email" value="true"> Send the new password via email to the user.</label>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Add</button>
			</div>
		</div>
	</form>
	<datalist id="client_pan">
		<?php foreach($allUserData as $user): ?>
		<option value="<?php echo $user['client_pan'] ?>" />
		<?php endforeach; ?>
	</datalist>
</script>
<!-- Remove a User (confirmation) -->
<script template="remove" type="text/x-handlebars-template">
	<div class="flex my1">
		<big class="fa fa-question px1"></big>
		<div class="flex-grow-1">Are you sure you want to remove the user with CODE <strong>{{client_code}}</strong>? <br><em>This can't be undone!</em></div>
		
	</div>
	<br>
	<div class="btn btn-warning btn-flat" data-remove="{{client_code}}"><i class="fa fa-trash"></i> Confirm</div>
	<div class="btn btn-default btn-flat" onclick="close_message()">Cancel</div>
</script> 

<div id="target">
	<div class="px2 py2 mx2 my2"><i class="fa fa-coffee"></i> Loading data...</div>
</div>

<?php footer_section_start(); ?>
<script>


	list_users();
	function list_users(){
		ajaxNvars.requestNrender({
			"template": "list",
			"endpoint": "users/list.json",
			"selector": "#target"
		}, function(response){
			/**
			 * Search input
			 */
			$('[type="search"]').on('input', function(){
				var value = $(this).val();
				$('#target table#search-valid tr.search-valid').addClass('hidden');
				$('#target table#search-valid tr.search-valid td.search-valid:contains("'+value+'")').closest('tr.search-valid').removeClass('hidden');
			});
			/**
			 * Placing "[onclick-search]" on a table's element will trigger a search.
			 */
			$('[onclick-search]').click(function(){
				$('[type="search"]').val($(this).attr('onclick-search')).trigger("input").focus();
			});
			/**
			 * Edit a user... send me to "single" (template)
			 */
			$('[data-edit]').click(function(){
				var client_code = $(this).attr('data-edit');
				single_user(client_code);
			});
			/**
			 * Remove a user... show me "confirmation" message ("remove" template)
			 */
			$('[data-confirm-remove]').click(function(){
				var client_code = $(this).attr('data-confirm-remove');
				// send to "confirmation" message
				remove_user(client_code);
			});
		})
	}
	function single_user(client_code, client_data){
		ajaxNvars.requestNrender({
			"template": "single",
			"endpoint": "users/single.json",
			"selector": "#target",
			"data": { "client_code": client_code }
		}, function (client_data){
			/**
			 * Create a virtualForm
			 */
			virtualForm.create('#edit-single', function(response){
				show_message("<b class='block'>All set.</b>"+response.message, {class: "alert-success"});
				single_user(client_code);
			});
		});
	}
	function add_user(){
		ajaxNvars.renderTemplate({"template": "add", "selector": "#target"});
		virtualForm.create('#add-single', function(response){
			show_message("<b class='block'>Sweet! </b>"+response.message, {class: "alert-success"});
			list_users();
		});
	}
	/**
	 * Remove a user by it's client_code. The template is a confirmation message.
	 */
	function remove_user(client_code){
		// show a message and render a template inside it.
		show_message('Confirm the removal of the user with client code: '+client_code);
		ajaxNvars.renderTemplate({
			"template": "remove",
			"data": { "client_code": client_code },
			"selector": "#Message"});
		$('#Message [data-remove]').click(function(){
			$('#Message .btn').html('Processing...');
			ajaxNvars.request({
				"endpoint": "users/remove.json",
				"data": { "client_code": client_code }
			}, function(response){
				show_message("<b class='block'>Good Bye! </b>"+response.message, {class: "alert-warning"});
				list_users();
			});
		})
	}
</script>
<?php footer_section_end(); ?>