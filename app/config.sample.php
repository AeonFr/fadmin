<?php 
if (session_status() == PHP_SESSION_NONE){ session_start(); }
/**
 * CHANGE ALL THIS CONFIGURATIONS BEFORE GOING INTO PRODUCTION.
 */
$config = [];
// base_url: The folder where all files are stored, to be used in the front-end to retrieve certain files. {{base_url}}/AdminLTE/dist... If the application is directly on the root level, this variable should be "" (empty string)
$config["base_url"] = "/app";
// page_title: The title of the site. It's used to reference the site in emails and also determines what will appear as the title of the "tab" or "window" of the browser.
$config["page_title"] = "Financial App";
/**
 * full_url: used to reference the site in emails.
 * DON'T finish this url with a "/"
 * For e.g. {{full_url}}/my-account would send a link to a user's account.
 */
$config["full_url"] = "http://localhost/app";
/**
 * navbar_title: This text/html will appear on the site's navbar when it's expanded
 */
$config["navbar_title"] = "<b>F</b>inancial <b>A</b>pp";
/**
 * navbar_short_title: This title will appear on the top navbar when it's collapsed. No more than 3 characters (else it won't fit). It might be a good idea to include an image as well.
 * Note: When the "fixed layout" is active, this title is never used (full title displays all the time). In that case it's better (for S.E.O.) to leave this as an empty string.
 */
$config["navbar_short_title"] = "<b>FA</b>";
/**
 * AdminLTE_skin: The skin of "AdminLTE" theme. All skins can be explored in the folder AdminLTE/dist/css/skins/ - The minified version will always be used (no need to add .min at the end)
 */
$config["AdminLTE_skin"] = "skin-blue";
/**
 * AdminLTE_fixed_navbar: Whether the navbar sticks to the bottom of the page. "SlimScroll"(js plugin) is activated alongside this option (required)
 */
$config["AdminLTE_fixed_navbar"] = true;
/* MYSQL CONFIGURATION ********************************************** */
/**
 * @var db_host: the mysql host
 * @var db_table: the name of the database. initialize a new database with "uf8_general_ci" encoding and export the bootstrap "_sql.sql" database. You can then enter with cretentials admin@admin.com ~ 123456 (recommended to change this values afterwards).
 * @var db_user: mysql username.
 * @var db_password: the password for the username.
 * 
 */
$config["db_host"] = "localhost";
$config["db_table"] = "_fiverr_adminfa";
$config["db_user"] = "root";
$config["db_password"] = "";
/* EMAIL CONFIGURATION ********************************************** */
/**
 * @var disable_all_email: [boolean] for debugging emails on localhost. This option should be false in production. While this is true, emails are still logged into the system, they're just not sent.
 * @var test_email: Every sent email will also be sent here, through a "BCC" or "Blind Carbon Copy". Leave as an empty string ("") to disable the option. Note: emails with password are not sent at this address.
 * @var send_from_email: This email will be the sender of all outgoing messages. Must be in the same domain as the site. E.g. a site hosted in http://rand-domain.com will have an email like email@rand-domain.com. There are ways to validate through SMTP to other services (like Gmail or Hotmail) but require custom solutions, and even some configuration from the Google/Outlook account, so they are not covered for this application. Also, sending the email from the same domain as the site looks neater.
 * @var admin_name: Add your name so recipients see who sent them the message. This is used as a signature for some email templates. You can add a corporation name here as well.
 * @var admin_email: in some email templates this might be the "reply-here" call to action. Also, in "my account" page it says: Want to change your email? Please contact me at {{admin_email}} (this text can be changed in /pages/my-account.php)
 * @var smtp_host: The host that will handle SMTP emails. This is specific for different servers, there is no general rule on what should go here (e.g. godaddy has it's own SMTP host)
 * @var smtp_auth: [boolean] Whether or not to use SMTP *with* authentication. I don't know of any case where this should be false.
 * @var smtp_user: The SMTP username.
 * @var smtp_pass: The SMTP password.
 * @var smtp_secure: Either "ssl" or "tls". This, again, is specific of every server.
 * @var smtp_post: [number] The SMTP port.
 * 
 */
$config["disable_all_email"] = false;
$config["test_email"] = "";
$config["send_from_email"] = "example@email.com";
$config["admin_name"] = "Root";
$config["admin_email"] = "eg@eg.com";
$config["smtp_host"] = "";
$config["smtp_auth"] = true;
$config["smtp_user"] = "";
$config["smtp_pass"] = "";
$config["smtp_secure"] = "ssl";
$config["smtp_port"] = 465;
/* PULSE CONFIGURATION ********************************************** */
/**
 * Pulses are request the client(user) sends every certain ammount of time to check for new unread chat messages, and new unread notifications.
 * @var pulse_frequency: [number] The frequency of the requests that check for unread chats and notifications, In milliseconds. The amount will be multiplied by 4 if the window is not currently focused, to improve performance (Mostly, for users that navigate with several times open at the same time). This value is divided by 4 in the "messages" page (the page where you actually chat with other users).
 */
$config["pulse_frequency"] = 15000;
/*********************************************** */
/**
 * debug_mode: [boolean] When it's set to true, the page will show "verbose" exceptions, even if the currently logged in user doesn't have the "verbose_exceptions" permission. (This exceptions include really detailed info, like file path and line number where the error occurred, and the full SQL queries -in case a query fails. This is not very user friendly).
 */
$config["debug_mode"] = false;
/**
 * Time zones, in different formats.
 */
$config["sql_time_zone"] = "+05:30";
date_default_timezone_set("Asia/Calcutta");
/*********************************************** *//**
 * Don't change below this line
 */
$config["root"] = dirname(__FILE__);
$config["pTemplVersion"] = 4; // pulse templates version.