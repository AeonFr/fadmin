# Pulses

Pulses are small request that take place every a certain amount of time, determined by `$config['pulse_frequency']` in milliseconds.

These kind of requests will keep the user updated over certain events on the page:

- Receiving a new Private Message or Chat (the word *chat* is used instead of *Private Message* because is shorter, yet in the front-end of the site the word "Message" might be preferred: it encourages the users to send them even without knowing if the receiver is online).
- Receiving a **notification**. This notifs are sent by an administrator, generally. They communicate important events on the site. Unlike chats, they can have any HTML tag inside. That's a very good reason **not to let users create notifications directly**, although it would be possible to make certain actions made by users to create notifications. They are also **anonymous**. Unlike chats, they're not linked to any *sender*, only to a *receiver*.
- Both chat and notifications will automatically emit emails if the user is not online. This emails can be canceled individually (e.g. a user can cancer chats but keep notifications, or vice versa).

# IA 

## Pulses metadata

Pulses can additionally have *meta keys* linked to them. Meta fields are a *heterarchical* way of relating a different ammount of *taxonomys*, *attachments* or any kind of *data* to a particular pulse.
This is the complete list of valid *meta keys* (they are considered valid simply because they have an actual effect on the front-end/interface of the site).

### Valid for a chat message

| `meta_key` | description 
| --- | ---
| `attachment` | **NOT IMPLEMENTED YET** if this is set, the program that evaluates chat messages and displays them assumes an attachment was sent with the message.

## Valid for a notification

| `meta_key` | description 
| --- | ---
| `icon` | if this is set, the value of this will be added into a `<i>` html element's class, before the notification's body. This can output an icon if the `<i>`'s class attribute matchs anyone from FontAwesome or Ionicons (both of this iconfonts are loaded in the page).
| `custom_html` |  if this is set, the custom html will be appended at the end of the notification (by default all html tags are escaped in the notification's body)
| `link` | if this is set, clicking the notif will send to this url (meta_value should be a valid url!)

## Possible responses from the pulses REST API

```

var response = {
	// if the templates are not stored in "localStorage", the server returns them as strings.
	// this is queried only once per device until the user decides to clear the cache in that device.
	"templates": {
		"chats":
		"<div template='chats'>chat template content, in handlebars</div>",
		"notifs":
		"<div template='notifs'>notifs template content, in handlebars</div>"
	}
	/*the notifs object. this might not be sent at all if there's nothing new to update.*/
	"notifs":{
		"count": 3, //the number of unread notifications
		"list": [ //an array containing last notifications
			{
				"icon": "", //class of the icon
				"sent_at": "some time ago...", //a string representing relative time since the notif was sent
				"body": "body of the notification",
				/*optionals*/
				"link": "http://localhost/app/etc", // a link to send user somewhere when notif is clicked
				"custom_html": "<div></div>", //custom html to be appended to the notification
				"read": true // if this is not mentioned, or false, notif is unread. If this is mentioned or true, notif was read
			},
			{
				#...
			},
			{
				#...
			}
		]
	},
	"chats":{
		"count": 1,
		"list": [
			{
				"body": "body of the message",
				"sent_at": "some time ago",
				"sender": "Admin", //name of the sender
				"read": true
			}
		],
		// optionally, return a resume of last chat with each user (or a fragment)
		// this is called the "chat label"
		"lasts":{
			// grouped by client code
			"M0001": {
				"ClC": "M0001", 
				"name": "Admin",
				"last_msg_of": "Admin", //name of the other user or "You"
				"last_msg_fragment": "Hey, mind if we...",//a resume of the last message
				"sent_at": "some time ago",
				"unread_count": 5 //this won't be set if value == 0
			},
			"M0002": {
				# ...
			}
		}
	}
}

```


# Client-side

## How pulses work

Pulses are this units of data that can have at least three states:

- When they are registered on the server, they are both **un-received** and **unread**.
- When they enter into a user's notification inbox (when they are online), they are **unread**, but **received**.
- When the user clicks on the notification's inbox, they become **read** and **received**.

On the client side, the pulses are queried in the following way:

1. When every page is loaded, the client sends a request to the server for the last 10 chats and notifications. If some of them are unread, they will be marked as unread in the request. This is called the `cacheOBJ`. (Additionally, the content of the handlebar templates that render the pulses are retrieved once and stored in localStorage, next time they will be queried from localStorage directly).
2. Every 15 seconds, pulses are queried from the database. If the window is not currently visible in the browser, this number of seconds between requests is multiplied by four. Please notice this is still a rough implementation: a more advanced one would not only consider if the window is focused or not, it may also consider if there's more than one tab opened at the time, to make for an optimal resource use.
3. The latest chat messages and notifications are received in this request, and the client merges that data into the `cacheOBJ`. They are also rendered in the "inbox" - this is done with a *library/template language* called `handlebars`. If a new item was just pulled, a sound warning should advice the user as well.
4. When the user clicks the "inbox" icon, a request advices the server that all notifications and chats should be marked as read. This also updates them in the `cacheOBJ` as read, since it's supposed to act as an offline mirror of the server status of pulses.

The pulses API in the side of the client is written in vanillaJS + jQuery, in the file scripts.js in object/class `pulse`- the public pulse's class API is described below:

## pulse(.js)

As per the inner working of the class is described above, this is an attempt to show the classes' behaviors can be modified.

### Disable pulses for a page

To disable pulses for a page, you must add the following code in the *footer section* (at the end of the `body` tag):

```
pulse.active = false;
```

You can set this variable to false at any time to disable pulses. To enable them again, you may have to *not only* set `pulse.active` to true, also initialize the pulse timeout loop again. 

```
pulse.active = true;
pulse.requestCacheOBJ();
```

### Add an event every time a pulse is received by the client.

You can also add a custom event when pulses are retrieved, simply by setting the `pulse.onRender` function at the end of the `body` tag.

```
pulse.onRender = function(reply_from_server){
	# your code...
	pulse.render(); // call this function or the default function's behavior won't execute.
}
```

You can get the data from the pulse in the `pulse.cacheOBJ` variable, but the `reply_for_server` will have only the recently sent notifs and chats.