pulseFrequency = 3000;
var currChatCache = {},
ClC = "";
function openChatAttr(){
	$('[open-chat]').click(function(){
		var otherUserClC = $(this).attr('open-chat');
		single_chat(otherUserClC);
	});
}
ajaxNvars.requestNrender({
	"template": "list-contacts",
	"endpoint": "chats/list-contacts.json",
	"selector": "#target-new-chat"
}, function(data){
	openChatAttr();
});
function requestChatWindow(data, callback){
	ajaxNvars.requestNrender({
		"template": "chat-window",
		"endpoint": "chats/chat-window.json",
		"data": data,
		"selector": "#target-chat"
	}, callback);
}
function single_chat(otherUserClC){
	// if there's a direct chat with contact opens, close them
	$('.direct-chat-contacts-open').removeClass('direct-chat-contacts-open');
	requestChatWindow({"ClC": otherUserClC}, function(data){
		ClC = otherUserClC;
		currChatCache = data;
		updateChatWindow(false, otherUserClC);
		ajaxNvars.renderTemplate('chat-box', currChatCache, "#target-chat-box");
		virtualForm.create("#send-chat", function(data){
			_("#send-chat").reset();
			updateChatWindow(data, otherUserClC);
		});
		$('#send-chat [name="body"]').keydown(function(e){
			if(e.keyCode == 13 && !e.shiftKey) {
				e.preventDefault();
				$(this).parent().submit(); // Submit form it belongs to
			}
		})
		pulse.onRender = function(data){ //overrides function above...
			if (data.chats.list){
				for (var i = 0; i < data.chats.list.length; i++) {
					if (data.chats.list[i].sender_code != ClC) continue;
					//currChatCache.chats.push(data.chats.list[i]);
					updateChatWindow(data.chats.list[i], ClC);
				}
			}
			pulse.render();
			// rewrite behavior of "chat widget" on navbar, we don't want it to refresh the page.
			openChatAttr();
		}
	});
}
pulse.onRender = function(data){ // when there's no chat selected.
	pulse.render();
	// rewrite behavior of "chat widget" on navbar, we don't want it to refresh the page.
	openChatAttr();
}
function updateChatWindow(data, otherUserClC, scroll){
	if (data!==false){
		currChatCache.chats.push(data);
		ajaxNvars.renderTemplate("chat-window", currChatCache, "#target-chat");
	}
	if (typeof scroll === "undefined" || scroll == true){
		$('.direct-chat-messages').scrollTop(999999999999);
	}
	if ($('.direct-chat-messages .older')){
		$('.direct-chat-messages .older').click(function(){
			requestChatWindow({"ClC": otherUserClC, "limit": 9999}, function(data){
				currChatCache = data;
				updateChatWindow(false, otherUserClC, false);
				show_message('All messages were loaded.', {duration: 4000});
			});
		});
	}
}

$(document).ready(function(){
	if (window.location.hash && window.location.hash.startsWith("#/")){
		var ClC = window.location.hash.substr(2);
		single_chat(ClC);
	}else{
		//open contact panel
		$('[data-widget="chat-pane-toggle"]').click();
	}
})