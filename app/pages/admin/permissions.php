<?php
$template['header'] = "Roles";
$template['breadcrumb'] = ["Admin" => "/admin"];
if (!has_permission("manage_user_permissions")){ die("You don't have permission to access this page"); } ?>
<script template="list" type="text/x-handlebars-template">
	<div class="row">
		<div class="col-xs-12 col-md-10 col-lg-8">
			<div class="box box-warning">
				<div class="box-header with-border">
					All roles
				</div>
				<div class="box-body">
					<div class="alert alert-warning">
						If you change your own role (<?php echo $_SESSION['user']['role'] ?>),
						be careful not to deny yourself access to the administration of roles,
						else you won't be able to undo your actions.
					</div>
					{{#each .}}
					<div class="btn btn-default btn-block" id="{{user_role_id}}">{{role}}</div>
					{{/each}}
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-lg-4">
			<div class="box box-default">
				<div class="box-header with-border">
					Add a new role.
				</div>
				<form class="box-body" id="virtualForm-add" endpoint="permissions/add-user-role.txt">
					<label class="block">Role Name</label>
					<input type="text" name="role" class="form-control my1">
					<label class="block">Permission List</label>
					<textarea class="form-control my2" name="permission_list">{{permission_list}}</textarea>
					<div class="black-75">Insert the name of each permission separated by a <code>;</code> - Check the permission list below for a reference!</div>
					<button type="submit" class="btn btn-primary my1">
						<i class="fa fa-save"></i> Save
					</button>
					<div class="my1" renderResponse></div>
				</form>
			</div>
		</div>
	</div>
</script>
<script template="single" type="text/x-handlebars-template">
	<a class="btn btn-default my2" id="return"><span class="fa fa-arrow-left"></span> Return to the List </a>
	<input type="hidden" name="currentRole" id="currentRole" value="{{role}}">
	<div class="row">
		<div class="col-md-8">
			<div class="box box-success">
				<div class="box-header with-border">
					Edit role: <strong>{{role}}</strong>
				</div>
				<form endpoint="permissions/update-permission-list.json" id="save-permission-list" class="box-body">
					<input type="hidden" name="role" value="{{role}}">
					<textarea class="form-control my2" rows="10" name="permission_list">{{permission_list}}</textarea>
					<div class="black-75">Insert the name of each permission separated by a <code>;</code> - Check the permission list below for a reference!</div>
					<button type="submit" class="btn btn-primary my1"><i class="fa fa-save"></i> Save</button>
				</form>
			</div>
		</div>
		<div class="col-md-4">
			<form id="virtualForm-rename" class="box box-success" endpoint="permissions/rename-user-role.txt">
				<div class="box-header with-border">
					Rename role: <strong>{{role}}</strong>
				</div>
				<div class="box-body">
					<label class="block">
						Edit the name of this role
					</label>
					<input type="hidden" name="role" value="{{role}}">
					<input type="text" class="form-control my2" name="new_name" value="{{role}}">
					<blockquote>
						The "role" attribute of all users with this role will be updated to point up to the new name.
					</blockquote>
					<button type="submit" class="btn btn-primary my1" id="save-role"><i class="fa fa-save"></i> Save</button>
					<div class="renderResponse" renderResponse></div>
				</div>
			</form>
			<form id="virtualForm-delete" class="box box-danger collapsed-box" endpoint="permissions/remove-user-role.txt">
				<div class="box-header with-border">
					<h3 class="box-title">Delete role</h3>
					<div class="box-tools pull-right">
				      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
				    </div><!-- /.box-tools -->
				</div>
				<div class="box-body">
					<label >
						<input required type="checkbox" name="role_id" value="{{user_role_id}}">
						I understand this action can't be undone, and that by doing this
						I might loose access to parts of the site or make other users loose access.
					</label>
					<blockquote>Tip: Make sure you are not leaving users without a role, or you may break the site for them!</blockquote>
					<button class="my1" type="submit"><span class="fa fa-trash"></span> Confirm delete</button>
					<div class="renderResponse" renderResponse></div>
				</div>
			</form>
		</div>
	</div>
</script>



<div id="target"></div>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-default">
			<div class="box-header with-border">
				Table of permissions
				<div class="box-tools pull-right">
			      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			    </div><!-- /.box-tools -->
			</div>
			<div class="box-body">
				<blockquote>Use this table to choose which permission will be assigned to the roles you create or modify.</blockquote>
				<?php try{ echo file_get_contents("$config[root]/DOCS/all-permissions.html"); }catch(Exception $e){} ?>
			</div>
		</div>
	</div>
</div>

<?php footer_section_start(); ?>
<script>
	renderList();
	function renderList(){
		ajaxNvars.requestNrender({
			endpoint: "permissions/list.json",
			template: "list",
			selector: "#target"
		}, function(response){
			// create the links between the rendered list and other states of the API
			$('#target [id]').click(function(){
				renderSingle(response, $(this).attr('id'));
			});
			virtualForm.create('#virtualForm-add', function(response){
				show_message(response, 5000);
				renderList();
			});
		});
	}
	function renderSingle(list, id){
		for (var i = list.length - 1; i >= 0; i--) {
			if (list[i].user_role_id == id){
				//list[i].permission_list = list[i].permission_list.split(";");
				ajaxNvars.renderTemplate("single", list[i], "#target");
				var currentRole = $('#currentRole').val();
				$('#return').click(function(){
					renderList();
				});
				/*$('#save-permission-list').click(function(){
					$(this).addClass('disabled');
					var data = {
						role: currentRole,
						permission_list: $('[name="permission_list"]').val()
					};
					ajaxNvars.request("permissions/update-permission-list", data, function(response){
						$('#save-permission-list').removeClass('disabled').after('Saved a moment ago');
					});
				});*/
				virtualForm.create('#save-permission-list', function(response){
					show_message("Updated!", 5000);
					renderList();
				})
				virtualForm.create('#virtualForm-rename', function(response){
					show_message(response, 5000);
					renderList();
				});
				virtualForm.create('#virtualForm-delete', function(response){
					show_message(response, 5000);
					renderList();
				})
			}
		}
	}
</script>
<?php footer_section_end(); ?>