<?php 
// if (!has_permission("manage_self"))
// 	throw new Exception("It seems like you don't have permission to do this. Please contact the administrator.");

require_module('users');
switch ($vars['endpoint']) {
	case 'change-password.json':
		check_required(['old_password', 'new_password']);
		if ($module['users']->change_my_password($_POST['old_password'], $_POST['new_password'])){
			reply("Your new password is now active.");
		}
		break;
	default:
		throw new Exception("Endpoint doesn't exist");
		break;
}