<li class="header">{{count}} unread notifications {{#xif "this.count > 0"}}<a onclick="pulse.markAllNotifsAsRead()" class="label label-default pull-right text-xs">mark as read</a>{{/xif}}</li>
<li>
	<ul class="menu">
		{{#each list}}
		<li>
			<a {{#if link}} href="{{link}}?source=notification" {{/if}} class="p-relative"><div class="text-muted text-sm p-absolute bottom-0 right-0 px1"><i class="fa fa-clock-o"></i> {{sent_at}}</div>
			<span class="pre-line block py1 {{#if read}}text-muted{{/if}}" style="padding-top:0">{{#if icon}}<i class="{{icon}}"></i>{{/if}} {{body}}</span></a>{{{custom_html}}}
		</li>
		{{/each}}
	</ul>
</li>
<li class="footer"><a href="<?php echo $config['base_url'] ?>/notifications?source=navbar">View all</a></li>
