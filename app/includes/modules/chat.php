<?php 

class Chat{
	function __construct(){
		$this->client_code = $_SESSION['user']['client_code'];
	}
	private function canChatWith($client_code){
		if (has_permission('chat_with_all')){
			return true;
		}else{
			global $db;
			$query_Role = $db->getAssoc("SELECT role FROM users WHERE client_code='$client_code'");
			if (!isset($query_Role[0])) throw new Exception ("User $client_code doesn't exist");
			$role = $query_Role[0]['role'];
			global $module;
			require_module('permissions');
			$permission_list = $module['permissions']->getList($role);
			if (in_array("chat_with_all",$permission_list)) return true;
			else return false;
		}
	}
	function listContacts(){
		global $db;
		if (has_permission('chat_with_all')){
			return $db->getAssoc("SELECT client_code, name FROM users WHERE client_code!=?", $this->client_code);
		}else{
			$rolesWithReceiveChatsPermission = $db->getAssoc("SELECT role FROM user_roles WHERE permission_list LIKE '%receive_chats_from_all%'");
			$return = array();
			foreach ($rolesWithReceiveChatsPermission as $row) {
				$role = $row['role'];
				array_push($return, $db->getAssoc("SELECT client_code, name FROM users WHERE client_code!=? AND role='$role'", $this->client_code));
			}
			return $return[0];
		}
	}
	function lastChatsWith($client_code, $limit=10){
		global $db; global $module;
		if (!$this->canChatWith($client_code)){
			throw new Exception("You don't have permission to chat with that user");	
			return false;
		}
		require_module('pulses');
		$last_chats=$module['pulses']->getLast([
			"chat"=>true,
			"chats_with"=>$client_code,
			"limit"=>$limit
		]);
		$return = [];

		foreach($last_chats as $chat){
			array_push($return, $module['pulses']->buildChatRow($chat));
		}
		return $return;
	}
	function newMessage($to, $body){
		global $db; global $module;
		require_module('pulses');
		$module['pulses']->send([
			"sender" => $this->client_code,
			"receiver" => $to,
			"body" => $body
		]);
		// if user is not active, email him.
		if (!$module['pulses']->user_is_active($to)){
			$userData = $module['users']->getUserData($to);
			require_module('email');
			$module['email']->send("Chat message received - $userData[name].", "Automatic chat message reminder", [
				"to_name" => $userData['name'],
				"to_email" => $userData['email'],
				"subject" => "New message received",
				"template" => "_Automatic chat message reminder",
				"template_vars" => array(
					"from"=> $module['pulses']->get_name_of($_SESSION['user']['name']),
					"from_client_code"=>$_SESSION['user']['name'],
					"body"=>$body
				)
			], false);
		}
		return array("sent_at"=>"just now", "body"=>$body, "sender_code"=>$this->client_code, "sender"=>$_SESSION['user']['name']);
	}
	function markConversationAsRead($other_user_client_code){
		global $db; global $module;
		$client_code = $_SESSION['user']['client_code'];
		$module['pulses']->markAllAsRead(true, "AND (sender='$other_user_client_code' AND receiver='$client_code')");
	}
}

$module['chat'] = new Chat();