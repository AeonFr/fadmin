
# Admin Web Application

This application offers an interface to view data, interact with other users (chat), recieve/send notifications, send bulkmail, and (in the future) there will be more options...

> __This documentation has no specific order. Just find the content related to what you're looking. Sorry if it's a little bit "Messy"__. To view any file included, switch files in the left "select" field. You can also grav your favourite markdown editor and open the files located in `app/DOCS/`

> If you view this documentation online, you will notice some chunks of code have a weird "`>`" character instead of  this character: <code>&gt;</code>. This is due to the markdown parser and I don't know how to fix it yet.

## Important files:

- __config.php__: This file contains a lot of configuration options necessary to run the application.
- __table_schema.php__: This file contains a php object that handles logic to update data from tables and then display that data to users.
- __vendor/phpList/config/config.php__: This is the configuration file of [phpList], a bulkmail software installed as a back-up option to test bulkmails. It's not quite integrated into the app (it can be runned from it's absolute url in the integrated back-end), but in the future the API of the software might be used to replace the current bulkmail script, while making the script maintain the same user friendly and simple interface that it currently has.

## Folder Structure

```
+ /app
	- /_noSQL
	- /AdminLTE
	- /API 
	- /assets
	- /DOCS
	+ /includes
		- email_templates
		- modules
		- (...) 
	- /pages
	- /vendor
```
### /app 
This is the folder that contains the hole application. The name of this folder can be changed in the file "config.php" by changing the variable $config['base_url']

### /_noSQL
This folder contains database that follow the .json format (thus, they are no .sql databases). Simple data is stored here. currently:

- a small database that tracks the source of any link that ends with "?source=XxXxX". This is a test to view what navigational parts of the site generate more clicks, and what options most users choose to browse content. It might be "completed" and deactivated by deleting some lines in "includes/general-functions.php"
- "email_templates": This folder contains templates in `.php` format that are used to send any type of outbound email. There's a documentation created specifically for template designers, go check it out.
- "dataview": a folder that contains data uploaded in csv folders for tables "ledgers", "holdings" and "openpositions", and any other that might be declared in "table_schema.php"

### /AdminLTE
The assets for the [AdminLTE theme] are located here.

### /API
This folder consists of a RESTful API service that runs in the background and emits responses via JSON. Almost every data is loaded through this API.

### /assets
Public access: Unlike other folders under /app, the files inside this folder can be accessed through knowing their _url_. This is the reason why this folder is used to store all the assets: .css, .js, images... Some of them are external libraries (like handlebars.js), others are custom built.

### /DOCS
This folder contains the documents you are reading right now, known as the "documentation".

### /includes
This folder contains the .php scripts that make the web application work. It's the "back-end", the "server-side part". Even if the REST api is located in `/API`, this folder is still the location of the code that handles all the logics that the REST API executes.  This is the "core" of the app.
It contains the following sub-folders:
```
+ /includes
	- classes
	- email_templates
	- functions
	- modules
```
The folders `classes` and `functions` basically contain what their names suggest: classes and functions.

#### /includes/modules
This folder contains php classes that are loaded automatically into the `$module` object when requested by any script. More on this in the "Modules" documentation.

### /pages
This folder contains all the rough content of the pages. Every page is mostly structured into a series of [handlebars] templates that are rendered client-side after some calls into the `REST API`. This is the reason why most pages include at least two or tree states or levels of content. This files sometimes also load statically, like the privacy policy, that is just an HTML file.

### /vendor
This folder contains external PHP libraries that help the application run.

## Installation

Copy the content of `/app` into whatever folder you want. Configure the `/app/config.php` file with your own variables. If you remove comments from this file (recommended) keep a copy of it with all the options commented: the comments help you understand what is it that the option do. *Note: create a config.backup.php file commented, and a config.php file without comments*

## Routing

The app starts is way in `index.php`. This little file checks if the're an active session, else it will send the user to `login.php`...
If there _is_ an active session, the routing will be handled by [FastRoute] (php routing system).


This is the kind of markdown that determines which handler will process each request...
```
// Matches /user/42, but not /user/xyz
$r->addRoute('GET', '/user/{id:\d+}', 'handler');

// Matches /user/foobar, but not /user/foo/bar
$r->addRoute('GET', '/user/{name}', 'handler');

// Matches /user/foo/bar as well
$r->addRoute('GET', '/user/{name:.+}', 'handler');

// This route
$r->addRoute('GET', '/user/{id:\d+}[/{name}]', 'handler');
// Is equivalent to these two routes
$r->addRoute('GET', '/user/{id:\d+}', 'handler');
$r->addRoute('GET', '/user/{id:\d+}/{name}', 'handler');

// This route is NOT valid, because optional parts can only occur at the end
$r->addRoute('GET', '/user[/{id:\d+}]/{name}', 'handler'); 
```

__Read the full [FastRoute Documentation][FastRoute] to see how it works__. It's important if you wan't to add a custom page. 

## Log in / security

### Security of files location and structure
The user data displayed on the files is secured via `.htaccess` (a file located in application's root (`/app/.htaccess`) that can control the way the server respond to requests).  
This file will __send any request to index.php, except those that point to the folders "AdminLTE" and "assets"__ (because they contain only static files, that don't compromise personal data).  
The folder **/API** is also not protected, because it has an internal routing system that works independently.

### Security of sessions, for logged in users

> The way the script handles security is documented in the authentication file. Any system security is inversely proportional to the amount of knowledge about the system any potential attacker has. **Don't share this information with anyone but a future developer.**

<!--- Links --->

[AdminLTE theme]: https://github.com/almasaeed2010/AdminLTE
[FastRoute]: https://github.com/nikic/FastRoute
[PHPAuth]: https://github.com/PHPAuth/PHPAuth

<style>
:root {
	--blockquote: #05CFB5;
}
#preview{ line-height: 2 }
pre{ word-wrap: break-word; white-space: pre-wrap; line-height: 1.25 }
blockquote{ border-color: var(--blockquote); font-size: 1.1em; }
</style>