<?php 
/**
 * Users Module
 * Documented in "modules-API.md"
 */
class Users{
	function __construct(){}
	/**
	 * This function is both called externally as internally.
	 * It's also handy to check if the user exists.
	 * The $include_hash option is set as false for security (possible accidental leak of data?)
	 */
	public function getUserData($client_code, $include_hash=false){
		global $db;
		try{
			$query_user_data = $db->getAssoc("SELECT client_code, email, name, role, client_pan, phone_num, registered_at, updated_at, md5_hash FROM users WHERE client_code=? LIMIT 1", $client_code);
		}catch(Exception $e){
			throw new Exception($e->getMessage() . " ~ " . $e->getFile() . "[". $e->getLine() . "]");
		}
		if (!isset($query_user_data[0])
		|| !is_array($query_user_data[0])
		|| $query_user_data[0] == NULL
		|| $query_user_data[0] == false){
			throw new Exception("Error retrieving user data: There's no such user with client code <b>$client_code</b>");
			return false;
		}
		$user_data = $query_user_data[0];
		// get the list of permissions for the user (as a standard array of strings)
		require_module('permissions');
		global $module;
		$user_data['permission_list'] = $module['permissions']->getList($user_data['role']);
		if (!$include_hash) unset($user_data['md5_hash']);
		return $user_data;
	}
	public function getUsersBy($column_name, $column_value){
		global $db;
		return $db->getAssoc("SELECT * FROM users WHERE $column_name=?", $column_value);
	}
	/**
	 * Set the data for any user.
	 * @param [type] $client_code  [description]
	 * @param [type] $updated_data [description]
	 */
	public function setUserData($client_code, $updated_data){
		if (!has_permission("manage_users")){
			throw new Exception ("You don't have the *manage_other_users* permission");
			return false;
		}
		global $db;
		$current_data = $this->getUserData($client_code);
		/**
		 * Check if the email is duplicated
		 */
		if (isset($updated_data['email']) && $updated_data['email'] != $current_data['email']){
		//the email is being updated into a new one
			$getUsersBy = $this->getUsersBy('email', $updated_data['email']);
			if (count($getUsersBy) > 0){
				throw new Exception("You are trying to update the email, but there's already an user with that email. (User: $getUsersBy[0])", 1);
				return false;
			}
		}
		//Merge new data with existing data
		$data = array_merge($current_data, (array) $updated_data);
		//query
		$query_user_data = $db->query_escaped(
		"UPDATE users SET
		email=?,
		name=?,
		role=?,
		client_pan=?,
		phone_num=?
		WHERE client_code=?",
		[
			$data["email"],
			$data["name"],
			$data["role"],
			$data["client_pan"],
			$data["phone_num"],
			$client_code
		]);
		if (isset($updated_data["md5_hash"])){
			$query_md5_hash = $db->query_escaped(
			"UPDATE users SET md5_hash=? WHERE client_code=?",
			array( $updated_data["md5_hash"], $client_code ));
		}
	}
	public function listAll(){
		global $db;
		return $db->getAssoc("SELECT * FROM users");
	}
	/**
	 * data[password] = (optional) if it's not set, a random password will be generated
	 * data[client_code], [email], [name], [role] and [client_pan] are all mandatory
	 * $isEdit  is for INTERNAL USE only! ♥☺☻
	 */
	public function add($data, $send_password_by_email=false, $isEdit=false){
		if (!has_permission("manage_other_users")){
			throw new Exception("You don't have the *manage_other_users* permission");
			return false;
		}
		global $db;
		global $config;
		if (isset($data['password']) && $data['password'] != ""){
			$password = $data['password'];
		}else if /* password is not set, and... */ (!$isEdit || ($isEdit &&  $send_password_by_email ) ){
			// In edit mode, we will only generate a password if we need to send a password by email
			require_once "$config[root]/includes/functions/random_str.php";
			$password = random_str(10, "abcdknbiefguoqrsu0123456789");
		}
		/**
		 * If the client code is not unique, throw new exception.
		 */
		$num_users_with_code = $db->num_rows("SELECT client_code FROM users WHERE client_code=?", array($data["client_code"]));
		if ((!$isEdit && $num_users_with_code > 0)){
			throw new Exception("There's already an user with the client code $data[client_code]");
			return false;
		}
		/**
		 * If the email is not unique, throw new exception
		 */
		$num_users_with_email = $db->num_rows("SELECT email FROM users WHERE email=? AND client_code!=?", array($data["email"], $data['client_code']));
		if ((!$isEdit && $num_users_with_email > 0) || ($isEdit && $num_users_with_email > 1)){
			throw new Exception("There's already an user with the email $data[email]");
			return false;
		}
		/**
		 * If there's no such a role, throw new exception
		 */
		$num_roles = $db->num_rows("SELECT role FROM user_roles WHERE role=?", array($data["role"]));
		if ($num_roles == 0){
			throw new Exception("There's no such role as $data[role]");
			return false;
		}
		if ($isEdit === false){
			try{
				// query...
				$query = $db->query_escaped(
				"INSERT INTO users
				(client_code, email, name, role, client_pan, phone_num, registered_at)
				VALUES
				(?,?,?,?,?,?,?)", array(
				$data['client_code'], $data['email'], $data['name'], $data['role'], $data['client_pan'], $data['phone_num'], date('Y-m-d H:i:s')
				));
			}catch(Exception $e){
				throw new Exception($e->getMessage());
			}
		}else{
			/**
			 * If $isEdit, it means we are using the function to edit the user. We will query all the actual user's data and fill the missing pieces of $data object with that
			 */
			try{
				$current_data = $this->getUserData($data['client_code']);
				$data = array_merge($current_data, $data);
				$query = $db->query_escaped(
				"UPDATE users SET
				email=?, name=?, role=?, client_pan=?, phone_num=?
				WHERE client_code=?",
				array(
				$data['email'],
				$data['name'],
				$data['role'],
				$data['client_pan'],
				$data['phone_num'],
				$data['client_code']
				));
			}catch(Exception $e){}
		}
		if (isset($password)){
			try{
				$db->query_escaped("UPDATE users SET md5_hash=? WHERE client_code=?", array(md5($password), $data['client_code']));
			}catch(Exception $e){
				throw new Exception($e->getMessage());
			}
		}
		if ($send_password_by_email){
			require_module('email');
			global $module;
			global $config;
			if (!$isEdit){
				$module['email']->send("Email to user $data[name] (id:$data[client_code])", "Your new Account Information",
				array(
					"template" => "_New Account - send password by mail",
					"template_vars" => array( "password" => $password ),
					"to_email" => $data['email'],
					"to_name" => $data['name'],
					"subject" => "Hi $data[name], this is your new account Information"
				), false);
			}else{
				$module['email']->send("Email to user $data[name] (id:$data[client_code])", "Your Password at $config[page_title]",
				array(
					"template" => "_Existing Account - send password by mail",
					"template_vars" => array( "password" => $password ),
					"to_email" => $data['email'],
					"to_name" => $data['name'],
					"subject" => "Hi $data[name], this is your new password"
				), false);
			}
		}
		$userData = $this->getUserData($data['client_code']);
		return $userData;
	}
	/**
	 * data[client_code] = (required) the id of user to edit.
	 * Everything else is optional.
	 * Same options as add()
	 */
	public function edit($data, $send_password_by_email=false){
		return $this->add($data, $send_password_by_email, true);
	}
	/**
	 * remove a user, by client_code
	 */
	public function remove($client_code){
		if (!has_permission("manage_other_users")){
			throw new Exception("You don't have the *manage_other_users* permission");
			return false;
		}
		global $db;
		// let's try to get the user data. If the user doesn't exist, a new error will be thrown.
		$this->getUserData($client_code);
		// you can't delete yourself. just in case, let's check
		if ($_SESSION['user']['client_code'] == $client_code){
			throw new Exception("You can't delete yourself!");
			return false;
		}
		$db->query_escaped("DELETE FROM users WHERE client_code=? LIMIT 1", array($client_code));
		$db->query_escaped("DELETE FROM user_meta WHERE client_code=?", array($client_code));
		return true;
	}
	/**
	 * Let users change their own password!
	 */
	public function change_my_password($old_password, $new_password){
		global $db;
		$data = $this->getUserData($_SESSION['user']['client_code'], true);
		if ($data['md5_hash'] !== md5($old_password)){
			throw new Exception("Your old password doesn't match!");
			return false;
		}
		$db->query_escaped("UPDATE users SET md5_hash=? WHERE client_code=?",
		array(md5($new_password), $_SESSION['user']['client_code']));
		/**
		 * Send an email to the user to notify the changes.
		 */
		global $config;
		global $module;
		require_module('email');
		$module['email']->send("Email to user $data[name] (id:$data[client_code])", "You have changed your password at $config[page_title]",
			array(
				"template" => "_Password Changed Notification",
				"template_vars" => array( "password" => $new_password ),
				"to_email" => $data['email'],
				"to_name" => $data['name'],
				"subject" => "Hi $data[name], this is your new password"
			), false);
		return true;
	}
	/**
	 * Metadata are small "keys" of data associated with a user by client_code.
	 * Each meta_key can only have one value, so this function allways RETURNS A STRING.
	 * (Unlike models like wordpress user_meta table where a multiple key can be repeated through the database, returning an array on check)
	 * @return   false if meta doesn't exists, a string containing the meta_value otherwise.
	 */
	public function get_meta($client_code, $meta_key){
		global $db;
		//check if the user exists
		if ($client_code != $_SESSION['user']['client_code'] && !$this->getUserData($client_code)) return false;

		//get meta
		$meta = $db->getAssoc("SELECT meta_value FROM user_meta WHERE client_code=? AND meta_key=? LIMIT 1", array($client_code, $meta_key));
		if (count($meta) == 0) return false;
		else return $meta[0]['meta_value'];
	}
	/**
	 * Add, update or remove metadata (empty strings as $meta_value will tell the script to remove them)
	 */
	public function set_meta($client_code, $meta_key, $meta_value){
		global $db;
		if ($meta_value === NULL) $meta_value = "";
		if ($client_code != $_SESSION['user']['client_code'] && !has_permission('manage_other_users')){
			/**
			 * Error returned when you are trying to set the meta_key for other user's account
			 * IF your role doesn't have the proper permission to do so.
			 */
			throw new Exception("You don't have the *manage_other_users* permission.");
			return false;
		}
		//check if the user exists
		if ($client_code != $_SESSION['user']['client_code'] && !$this->getUserData($client_code)) return false;
		//add, update or remove?
		if ( $this->get_meta($client_code, $meta_key) == false ){
			$query = 'add';
		}else{
			$query = 'update';
		}
		if ($meta_value == '') $query = 'delete';
		switch ($query) {
			case 'add':
				$db->query_escaped("INSERT INTO user_meta (client_code, meta_key, meta_value) VALUES (?,?,?)",
				array($client_code, $meta_key, $meta_value));
				break;
			case 'update':
				$db->query_escaped("UPDATE user_meta SET meta_value=? WHERE client_code=? AND meta_key=?",
				array($meta_value, $client_code, $meta_key));
				break;
			case 'delete':
				$db->query_escaped("DELETE FROM user_meta WHERE client_code=? AND meta_key=? LIMIT 1",
				array($client_code, $meta_key));
				break;
		}
		return true;
	}
}
/**
 * Initialize module
 */
$module['users'] = new Users();