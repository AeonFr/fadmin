<?php $template['header'] = "Send Bulkmail";
// $template['sidebar-collapse'] = true;
$template['breadcrumb'] = ["Admin" => "/admin", "Mails"=>"/admin/bulkmail"];
if (!has_permission('send_bulkmail')) die("You don't seem to have permission to access this particular page.");
?>
<style>
	[parsed="1"]{border-color: #3c763d;box-shadow:inset 0px 0px 2px #3c763d}
	[parsed="0"]{border-color: #a94442;box-shadow:inset 0px 0px 2px #a94442}
</style>
<script template="template-list" type="text/x-handlebars-template">
	{{#each .}}
	<label class="block pointer py1 border black-25">
		<table>
			<tr>
				<td class="px1"><input type="radio" name="template" value="{{.}}"></td>
				<td>
					{{#xif "this.startsWith('__')"}}
						<i class="fa fa-code text-warning px1"></i>
					{{else xif "this.startsWith('_')"}}
						<i class="fa fa-file-code-o text-warning px1"></i>
					{{else}}
						<i class="fa fa-file text-success px1"></i>
					{{/xif}}
				</td>
				<td class="TEMPLATE-NAME black-75">{{#x "this.substr(0,this.length - 4)"}}{{/x}}</td>
			</tr>
		</table>

	</label>
	{{/each}}
</script>
<form id="sendForm" endpoint="bulkmail/massiveSend.json" class="box box-default">
<div class="box-header">
	<h3 class="box-title">Configure and send a new massive email.</h3>
</div>
	<div class="box-body">
		<h3>To:</h3>
		<p>Insert one or more emails, separated by <code>;</code> and -optionally- enter the name before the email, separating them with a <code>:</code>.</p>
<div class="row">
	<div class="col-xs-12 col-lg-6">
		<div class="box box-default collapsed-box">
			<div class="box-header with-border">
				<h3 class="box-title">Examples</h3>
				<div class="box-tools pull-right">
			      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			    </div><!-- /.box-tools -->
			</div>
			<div class="box-body">
				<p>Example:</p>
				<pre><code>Bruce Lee:
bruce.lee@gmail.com;
Tom Hanks:
tom.hanks@hotmail.com;</code></pre>
				<p>You can also use it like this:</p>
				<pre><code>Bruce Lee: bruce.lee@gmail.com;
Tom Hanks: tom.hanks@hotmail.com;</code></pre>
				<small class="italic block text-muted">(Enters and spaces at the beginning or end of a string will be deleted)</small>
				<p>If you want, you can ommit the name for some users:</p>
				<pre><code>Bruce Lee: bruce.lee@gmail.com;
tom.hanks@hotmail.com;</code></pre>
			</div>
		</div>
	</div>
</div>
		<textarea id="email_list" class="form-control" rows="10" placeholder="Name:
email;
Name:
email;"></textarea>
		<h3>Use Template:</h3>
		<p>Remember you can preview that everything is O.K. with a template <em>before sending</em>, by using the "preview" feature in the <a target="_blank" href="<?php echo $config['base_url'] ?>/admin/bulkmail/edit-templates">"Edit Email templates" site <i class="fa fa-external-link"></i></a>.</p>
		<a onclick="generate_list()" role="button" class="btn btn-xs btn-default"><i class="fa fa-refresh"></i> Refresh template list</a>
		<div id="template-list">
			<div class="loading p-relative" style="height:300px"></div>
		</div>
		<h3>Subject of the message:</h3>
		<p>If you leave this empty, the template's subject will be used (not all templates have a subject declared, if that's the case, the user might receive an email without a subject).</p>
		<input type="text" name="subject" class="form-control">


		<p class="block my1">
			<h3>Group this sendings as:</h3>
			<p>This is how the emails are grouped in the <a href="<?php echo $config['base_url'] ?>/admin/bulkmail?email-logs" target="_blank">outbound email log <i class="fa fa-external-link"></i></a></p>
			<input required type="text" name="transaction_group" value="sent at <?php echo date('d/m/Y H:i') ?>" class="form-control">
		</p>
		
		<h3 class="my2">Ready?
		</h3>
		<p class="my2"><button type="submit" class="btn btn-lg btn-flat btn-success">Send<i class="ion-paper-airplane"></i></button></p>
		

		<hr>
		More options:
		<h4>Save this settings (offline) for next time: <a id="saveSettings" role="button" class="btn btn-default btn-flat">save settings</a>
		<small class="block text-muted"><i class="fa fa-info-circle"></i> A new window will open; hit <kbd>Ctrl</kbd> + <kbd>S</kbd> and save with .json extension (like: example.json). You can then send emails with the same template, subject and to the same list of users by retrieving this data.</small></h4>
		<h4>Retrieve settings from a .json file: <label class="btn btn-default btn-flat" role="button">retrieve settings <input type="file" id="retrieveSettings" class="hidden"></label>
		<small class="block text-muted"><i class="fa fa-info-circle"></i> You must choose a file obtained from the "save settings" command, stored in .json format.</small></h4>
	</div>
</form>

<script template="onSend" type="text/x-handlebars-template">
	<div class="p-fixed bg-white-75 top-0 bottom-0 left-0 right-0 text-center jumbotron py2" style="margin:0;padding-top:125px;">
		<div style="max-width: 520px;margin-left:auto">
			<big CLOSETHIS class="pull-right pointer btn btn-flat bg-white mx1"><i class="ion-close"></i></big>
			<h2 class="my2">Request Processed successfully</h2>
			<div class="px2 mx2 flex bg-white text-left">
				<div class="px2 flex-grow">
					<h4>Success:</h4>
					{{#if success}}
					<ul>
						{{#each success}}
						<li>{{email}}
						<br>status: <b>{{status}}</b></li>
						{{/each}}
					</ul>
					{{else}}
					<p><i>Nothing to show here :(</i></p>
					{{/if}}
				</div>
				<div class="px2 flex-grow">
					<h4>Errors:</h4>
					{{#if error}}
					<ul>
						{{#each error}}
						<li>{{email}}
						<br>status: <b>{{status}}</b></li>
						{{/each}}
						{{#xif "this.status == 'user is unsubscribed'"}}"User is unsubscribed" type of transactions are not stored in the logs.{{/xif}}
					</ul>
					{{else}}
					<p><i>Nothing to show here ☺</i></p>
					{{/if}}
				</div>
			</div>
			<div class="px2 mx2 flex">
				<div CLOSETHIS class="btn btn-flat flex-grow btn-default">
					<i class="ion-close"></i> Stay in this page 
				</div>
				<a class="btn btn-flat flex-grow btn-info" href="<?php echo $config['base_url'] ?>/admin/bulkmail?email-logs">
					<i class="ion-android-send"></i> Open logs 
					</a>
			</div>
		</div>
	</div>
</script>
<div id="onSend"></div>

<?php footer_section_start(); ?>
<script src="<?php echo $config['base_url'] ?>/assets/js/pages/sendEmail.js"></script>
<?php footer_section_end(); ?>