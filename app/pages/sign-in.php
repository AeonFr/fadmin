<?php include "./layout.php"; ?>
<div class="row">
<div class="col-xs-12">
<h1>Sign in to continue</h1>
<form method="post" action="<?php echo $config['base_url'] ?>/" class="row">
	<div class="col-sm-6 jumbotron">
		<?php if (isset($log_in_error)){ ?>
			<div class="alert alert-danger shake"><?php echo $log_in_error ?></div>
		<?php } ?>
		<div class="form-group">
			<label for="user[email]">Email</label>
			<input type="email" name="user[email]" class="form-control"
			<?php echo (isset($_POST['user']['email'])) ? 'value="' . $_POST['user']['email'] . '"' : '' ?>>
		</div>
		<div class="form-group">
			<label for="user[password]">Password</label>
			<input type="password" name="user[password]" class="form-control">
		</div>
		<div class="form-group">
			<input type="hidden" name="user[remember]" value="0">
			<label>
				<input type="checkbox" name="user[remember]" value="1" checked> Remember me
			</label>
		</div>
		<div class="form-group">
			<button class="btn btn-lg">Sign in</button>
		</div>
	</div>
</form>