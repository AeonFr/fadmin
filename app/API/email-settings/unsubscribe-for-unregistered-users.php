<?php 
/**
 * Loop through the "unsubscribes.json" database... If the user is not there, add it! We are simply comparing with md5 emails, no actual email is necessarily stored.
 */
require_module('unsubscribe-for-unregistered-users');

include "$config[root]/layout.php";

if (isset($_GET['resubscribe'])):
	$module['unsubscribe4unregistered']->resubscribe($_GET['ref']);
?>
<div class="jumbotron">
	<h3>You had resubscribed to the list.</h3>
	<p>Thanks! Remember, if you change your mind, you can <a href="<?php echo $config['base_url'] ?>/API/email-settings/?ref=<?php echo $_GET['ref'] ?>">unsubscribe</a> at any time.</p>
</div>
<?php
else:
?>
<div class="jumbotron">
<?php if ($module['unsubscribe4unregistered']->is_subscribed($_GET['ref'])): ?>
	<?php $module['unsubscribe4unregistered']->unsubscribe($_GET['ref']); ?>
	<h3>You were unsuccessfully unsubscribed from this list.</h3>
<?php else: ?>
	<h3>You are already unsubscribed.</h3>
<?php endif; ?>
<h4>Do you wish to re-subscribe to this list? follow this link:
<a href="<?php echo $config['base_url'] ?>/API/email-settings/?resubscribe=true&ref=<?php echo $_GET['ref'] ?>">Re-subscribe</a>
</h4>
<?php endif; ?>