<?php 
require_module('pulses');
switch ($vars['endpoint']) {
	/**
	 * PUBLIC ENDPOINTS:
	 */
	case 'getChanges.json':
		reply($module['pulses']->getChanges());
		break;
	case 'getCacheOBJ.json':
		if (isset($_POST['requestTemplates']))$rq=true; else $rq=false;
		reply($module['pulses']->getCacheOBJ($rq));
		break;
	case 'readAllNotifs.json':
		$module['pulses']->markAllAsRead(false);
		reply(1);
		break;
	case 'getNotifs.json':
		$list = $module['pulses']->getLast(["chat"=>false,"limit"=>$_POST['last']]);
		$arr = array();
		foreach($list as $indexOf => $row){
			array_push($arr,$module['pulses']->buildNotifRow($row));
		}
		$reply =[
				"list" => $arr
			];
		reply($reply);
		break;
	default:
		throw new Exception("Endpoint doesn't exist");
		break;
}