<?php
/**
 * This folder handles all kind of requests that do NOT require a response with a traditional template ("AdminLTE").
 * This means that the requests that are performed here are mainly through Ajax,
 * and the response can either be in xml format, to be injected in some part of the page,
 * or in json format, to be interpreted by a script
 *
 *
 * View more documentation in "DOCS/API"
 */
require_once "../config.php";
require_once "$config[root]/includes/general-functions.php";
/**
 * Before rendering ANYTHING, let's check whether we're logged in or not...
 */
require_once "$config[root]/includes/auth.php";
/**
 * And let's include some handy functions
 */
function reply_with_error($message){
	http_response_code(400);
	die(json_encode([
		"status" => -1,
		"error" => $message
	]));
}
// error handler function
function customError($errno, $errstr, $errfile, $errline) {
	$errorMsg = "502 Internal server Error. Contact support if the problem persists.";
	global $config;
	if (has_permission('verbose_exceptions') || $config['debug_mode'])
		$errorMsg = "[$errno] $errstr <br> $errfile -> [$errline]";
	echo reply_with_error($errorMsg);
}
// set error handler
set_error_handler("customError");
// check for required $_POST variables that are missing, reply with an error $message
function check_required($arr){
	foreach ($arr as $value) {
		if (!isset($_POST[$value])) reply_with_error("The request must have '$value'");
	}
}
//reply with success!
function reply($response){
	echo json_encode($response, JSON_PRETTY_PRINT);
	exit();
}

/**
 * Start Routing...
*/
require "$config[root]/vendor/fast-route/autoload.php";

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
	global $config;
	$baseurl = $config['base_url'].'/API';
	/**
	 * ----------------------------------------------------------------------------------------------
	 */
	$r->addRoute('POST', "$baseurl/pulse.json", "pulse");
	$r->addRoute('POST', "$baseurl/{module}/{endpoint:.+}", "REST-Modules-API");
	//$r->addRoute('GET', "$baseurl/{module}/{endpoint:.+}", "REST-Modules-API");
	/**
	 * ----------------------------------------------------------------------------------------------
	 */
	
});
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
if (false !== $pos = strpos($uri, '?')) {
	$uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
	case FastRoute\Dispatcher::NOT_FOUND:
		http_response_code(404);
		include "$config[root]/layout.php";
		include "$config[root]/pages/404.html";
		exit;
		break;
	case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
		$allowedMethods = $routeInfo[1];
		http_response_code(405);
		echo 'Error 405: Method Not Allowed';
		break;
	case FastRoute\Dispatcher::FOUND:
		$handler = $routeInfo[1];
		$vars = $routeInfo[2];
		global $config;
		/**
		 * ---------------------------------------------------------------------------------------------
		 */
		switch ($handler) {
			case "pulse":
				echo json_encode(array("pulse"=>get_pulse()));
				exit();
				break;
			case "REST-Modules-API":
				/**
				 * Enroute this requests to be interpreted by separate files
				 * The name of the file is "/API/REST-Modules-API/api-{module}.php"
				 * (Where {module} is the name of the module).
				 *
				 * NOTE:
				 * Every other method that should not be interpreted by this script must be interpreted before this one and possibly added as an exception here! Keep that in mind before developing other "endpoints" that don't need to be executed here.
				 */
				try{
					$file_addr = "REST-Modules/api-$vars[module].php";
					if(file_exists($file_addr))
						include $file_addr;
					else
						throw new Exception("The API $vars[module] is not a valid REST API.");
				}catch(Exception $e){
					reply_with_error($e->getMessage());
				}
				break;
		}
		/**
		 * ---------------------------------------------------------------------------------------------
		 */
		break;
}