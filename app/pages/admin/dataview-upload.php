<?php $template['header'] = "Update tables";
require_once "$config[root]/table_schema.php";
$template['breadcrumb'] = ["Admin" => "/admin"]; ?>

<link rel="stylesheet" href="<?php echo $config['base_url'] ?>/assets/css/misc/step_progress_bar.css">
<link rel="stylesheet" href="<?php echo $config['base_url'] ?>/assets/css/misc/card.css">
<!-- stepBar -->
<div class="p-relative" style="height:100px">
    <div class="progress-MISC">
      <div class="step">
        <div class="step-progress"></div>
        <div class="icon-wrapper">
          <svg class="icon icon-checkmark" viewBox="0 0 32 32"><path class="path1" d="M27 4l-15 15-7-7-5 5 12 12 20-20z"></path>  </svg>
          <div class="step-text">SELECT TABLE</div>
        </div>
      </div>
      <div class="step">
        <div class="step-progress"></div>
        <div class="icon-wrapper">
          <svg class="icon icon-checkmark" viewBox="0 0 32 32"><path class="path1" d="M27 4l-15 15-7-7-5 5 12 12 20-20z"></path>  </svg>
          <div class="step-text">UPLOAD FILE</div>
        </div>
      </div>
      <div class="step">
        <div class="step-progress"></div>
        <div class="icon-wrapper">
          <svg class="icon icon-checkmark" viewBox="0 0 32 32"><path class="path1" d="M27 4l-15 15-7-7-5 5 12 12 20-20z"></path>  </svg>
          <div class="step-text">CONFIGURE</div>
        </div>
      </div>
      <div class="step">
        <div class="icon-wrapper">
          <svg class="icon icon-checkmark" viewBox="0 0 32 32"><path class="path1" d="M27 4l-15 15-7-7-5 5 12 12 20-20z"></path>  </svg>
          <div class="step-text">DONE!</div>      
        </div>
      </div>
    </div>
</div>
<!-- /stepBar -->
<!-- {component:fieldset} -->
<div class="p-relative" style="height: calc(100vw - 533px);min-height:533px; overflow-y: auto;">
    <div    id="selectTable"   class="card p-absolute from-top top-0 left-0 right-0 ">
        <fieldset class="h3">
            Select a Table:
            <ul>
                <button onclick="fieldset.selectTable.select('holdings')"
                class="card btn card--btn   btn-block   pointer">Holdings       </button>
                <button onclick="fieldset.selectTable.select('ledgers')"
                class="card btn card--btn   btn-block   pointer">Ledgers        </button>
                <button onclick="fieldset.selectTable.select('openpositions')"
                class="card btn card--btn   btn-block   pointer">Open positions </button>
            </ul>
        </fieldset>
    </div>
    <div    id="uploadFile"   class="hidden card h3 p-absolute from-top top-0 left-0 right-0 ">
        <fieldset>
            <h3>Upload a File:</h3>
            <p>

            	Table headers in line: <input type="number" value="1" disabled>
				
				<i class="fa fa-info-circle" data-toggle="tooltip" title="<img src='<?php echo $config['base_url'] ?>/assets/img/dataview-upload-table-headers.png' class='block' style='max-width:100%'>This line must match the line from the .csv file that contains the <b>titles</b> of each column."></i>	

            </p>
            <form action="/file-upload">
                    <input name="file" type="file" multiple />
            </form>
        </fieldset>
    </div>
    <fieldset
    id="setTableSchema"        class="hidden card h3 p-absolute from-top top-0 left-0 right-0 overflow-container">
        <div class="text-center" style="width:640px">
            <h3>Configuration</h3>
            <p class="small">Control everything is O.K, then click
            <a class="btn btn-default btn-flat" onclick="fieldset.confirm.init()">CONTINUE</a> </p>
        </div>
        <div style="width:640px" id="tableSchemaForm">
            <!-- {Template:tableSchemaForm} -->
            <div class="loading"></div>
        </div>
    </fieldset>
    <fieldset id="confirm" class="hidden card h3 p-absolute from-top top-0 left-0 right-0 overflow-container">
        <div style="width:640px" id="ConfirmOperation">
            <!-- {Template:ConfirmOperation} -->
            <div class="loading"></div>
        </div>
    </fieldset>
    <fieldset id="done" class="hidden card h3 p-absolute from-top top-0 left-0 right-0 overflow-container">
        <div style="width:640px" id="DoneMessage">
            <!-- {Template:DoneMessage} -->
            <div class="loading"></div>
        </div>
    </fieldset>
</div>
<!-- /{component:fieldset} -->
<?php footer_section_start(); ?>
<style>.label>label{color:#0073b7 !important;white-space:normal!important;}</style>
<script template="tableSchemaForm" type="text/x-handlebars-template">
    <div class="flex">
        <div class="flex-grow px1 py1" style="max-width:320px">
            <p class="block label">
                <label class="block"
                for="">nicename</label>
                <h6 class="help-tooltip">This is what the name of the table is displayed to users.</h6>
            </p>
        </div>
        <div class="flex-grow px1 py1 bg-blue">
            <input type="text" class="form-control" name="nicename"
            value="{{nicename}}">
        </div>
    </div>
    <div class="flex">
        <div class="flex-grow px1 py1" style="max-width:320px">
            <p class="block label">
                <label class="block"
                for="">Icon</label>
                <h6 class="help-tooltip">Select a <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank">fontAwesome</a> icon's class, or leave at the default, "fa fa-database"</h6>
            </p>
        </div>
        <div class="flex-grow px1 py1 bg-blue">
            <input type="text" class="form-control" name="icon"
            value="{{icon}}">
        </div>
    </div>
    <div class="flex">
        <div class="flex-grow px1 py1" style="max-width:320px">
            <p class="block label">
                <label class="block"
                for="">permission required</label>
                <h6 class="help-tooltip">Users inherit permissions from their roles. The users with this permission will be able to see this table.</h6>
            </p>
        </div>
        <div class="flex-grow px1 py1 bg-blue">
            <input type="text" class="form-control" name="permission_required"
            value="{{permission_required}}">
        </div>
    </div>
    <div class="flex">
        <div class="flex-grow px1 py1" style="max-width:320px">
            <p class="block label">
                <label class="block"
                for="">Excluded Columns</label>
                <h6 class="help-tooltip">Separate column names in your table with commas (",").</h6>
            </p>
        </div>
        <div class="flex-grow px1 py1 bg-blue">
            <input type="text" class="form-control" name="exclude_columns"
            value="{{#each exclude_columns}}{{.}},{{/each}}">
        </div>
    </div>
    <div class="flex">
        <div class="flex-grow px1 py1" style="max-width:320px">
            <p class="block label">
                <label class="block"
                for="">Filter the content of the table?</label>
                <h6 class="help-tooltip"> If you choose yes, each user will receive a different version of the table.</h6>
            </p>
        </div>
        <div class="flex-grow px1 py1 bg-blue">
            <select class="form-control" name="filter_content">
            {{#xif ' this.filter_content == "true" || this.filter_content == true '}}
                <option value="true" selected>Yes</option>
                <option value="false">No</option>
            {{else}}
                <option value="true">Yes</option>
                <option value="false" selected>No</option>
            {{/xif}}
            </select>
        </div>
    </div>
    <div class="flex">
        <div class="flex-grow px1 py1" style="max-width:320px">
            <p class="block label">
                <label class="block"
                for="">Filter content rule</label>
                <h6 class="help-tooltip"></h6>
            </p>
        </div>
        <div class="flex-grow px1 py1 bg-blue h6">
            Filter the content where the user's
            <select class="form-control" name="filter_content_by_variable"
            rel="txtTooltip" title="The data from the user's account, as it appears in the database." data-toggle="tooltip" data-placement="top">
                <option value="client_code" {{#xif " this.filter_content_by_variable == 'client_code'"}}selected{{/xif}}>
                    client code</option>
                <option value="email" {{#xif " this.filter_content_by_variable == 'email'"}}selected{{/xif}}>
                    email</option>
                <option value="name" {{#xif " this.filter_content_by_variable == 'name'"}}selected{{/xif}}>
                    name</option>
                <option value="role" {{#xif " this.filter_content_by_variable == 'role'"}}selected{{/xif}}>
                    role</option>
                <option value="client_pan" {{#xif " this.filter_content_by_variable == 'client_pan'"}}selected{{/xif}}>
                    client pan</option>
            </select>
            matches with the column
            <div class="SelectFilterColumnContainer">
                <input name="filter_content_column" class="form-control" type="text" value="{{filter_content_column}}"
                rel="txtTooltip" title="The name of a table header, from the file you uploaded." 
                data-toggle="tooltip" data-placement="bottom">
                <div id="SelectFilterColumn">
            </div>
            </div>
        </div>
    </div>
    <div class="btn btn-default btn-flat" onclick="fieldset.confirm.init()">CONTINUE</div>
</script>

<script template="SelectFilterColumn" type="text/x">
    <select name="filter_content_column" class="form-control">
        <option disabled>Choose an option...</option>
        {{#each titles}}
        <option value="{{.}}">{{.}}</option>
        {{/each}}
    </select>
</script>

<script template="ConfirmOperation" type="text/x">
    <div class="alert alert-info">Click "CONFIRM" to apply this changes to <i class="{{table_schema.icon}}"></i> {{table_schema.nicename}}</div>
    {{#xif ' this.table_schema.filter_content == "true" || this.table_schema.filter_content == true '}}
        <div class="box box-solid box-success" style="font-size:2rem">
            <div class="box-header">Set content of tables</div>
            <div class="box-body"><i class="fa fa-info-circle"></i> Tables visible as "<i class="{{table_schema.icon}}"></i> {{table_schema.nicename}}" for users whose <code>{{table_schema.filter_content_by_variable}}</code> matches...</div>
            {{#each operationResume.generatedFiles}}
            <div class="box-body">
                 ...<code>{{.}}</code> <a data-userVar="{{.}}" href="" target="_blank">Preview this table <i class="ion-ios-eye"></i> <i class="fa fa-external-link" data-toggle="tooltip" title="Open a preview of this table"></i></a>
                <label class="block"><input type="checkbox" name="notify[{{.}}]" notify="{{.}}" value="true" checked> Send notification to affected user(s).</label>
            </div>
            {{/each}}
            <div class="box-footer small">This tables are only generated to users with permission <code>{{table_schema.permission_required}}</code></div>
            <div class="box-footer small">If a user is not yet in the database, add it <b>before</b> continuing and then repeat the update process from step 1.</div>
        </div>
    {{else}}
        <div class="box box-solid box-success" style="font-size:2rem">
            <div class="box-header">Set content of table <i class="{{table_schema.icon}}"></i> {{table_schema.nicename}}</div>
            {{#each operationResume.generatedFiles}}
            <div class="box-body">
                <a data-userVar="" href="" target="_blank">Preview this table <i class="ion-ios-eye"></i> <i class="fa fa-external-link" data-toggle="tooltip" title="Open a preview of this table"></i></a>
                <label class="block"><input type="checkbox" name="notify[{{.}}]" notify="{{.}}" value="true" checked> Send notification to affected user(s).</label>
            </div>
            <div class="box-footer small">
                This table will be visible to <strong>all users</strong> with the permission <code>{{table_schema.permission_required}}</code>.
            </div>
            {{/each}}
        </div>
    {{/xif}}
    {{#if operationResume.errors}}
        <div class="box box-solid box-warning" style="font-size:2rem">
            <div class="box-header">Warning(s)</div>
            {{#each operationResume.errors}}
            <div class="box-body small">
                <i class="ion-android-warning"></i> {{{.}}}
            </div>
            {{/each}}
        </div>
    {{/if}}
    <hr>
    <div class="warning small text-warning mx1">
        <div>
            <label data-toggle="tooltip" title="Check this to disable all notifications."><input type="checkbox" name="notify_users" value="false"> <i class="ion-android-notifications-off"></i> Don't send any notifications.</label>
        </div>
    </div>
    <div class="my1"></div>
    <div class="btn btn-default btn-flat" id="ConfirmOperationBtn" onclick="fieldset.done.init()">CONFIRM</div>
</script>

<script template="DoneMessage" type="text/x">
    <h1>That's it!</h1>

    <p><a href="<?php base_url() ?>/admin/notifications">If users affected are offline, notifications won't reach. You're recommended to notify them via email, just clicking here and use option "DELIVER VIA EMAIL"</a></p>
</script>

<script src="<?php echo $config['base_url'] ?>/assets/js/pages/admin_tableview.js"></script>


<?php footer_section_end(); ?>