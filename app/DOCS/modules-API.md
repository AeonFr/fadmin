

The modules API is a simple API to include one or many classes into the global scope, in a simple and readable way.

Each of this classes is dependent of the modules API to function properly.

It's initialized by default on the front-end and can be manually initialized with the following code:

```
require_once 'config.php';
require_once "$config[root]/includes/general-functions.php";
```

All modules are located in the `includes/classes/modules` folder and can be included simply by using the following function:

```
require_module($module_name);
```

Where `$module_name` is the name of the file in that folder. E.g.: `require_module('users')` will require `includes/modules/users.php` and `require_module('sub/file')` will require the file `includes/modules/sub/file.php`

The files are included with the `require_once` syntax, to make sure they're not included twice.

> After including a module, the expected behavior is to have a new index under the `$module` object. For example, after `require_module('users')`, the `$module['users']` var contains a class where all the users functions are available.


## users

This module is developed to retrieve costumer's information.
Please notice that if you are looking a way to retrieve information for the current user (you), you can check this very same values in the `$_SESSION['user']` variable. This variable will be `false` if no user is logged in. It's initialized on authentication.

```
if ($_SESSION['user']) echo "Hello, logged in user " . $_SESSION['user']'[name'] . ".";
```

> md5_hash is not returned by default with this function.

## permissions
---

This modules edit the "user_roles" table, which contains rules to determine access to the site's modules and other stuff.

Each row in this table contains a *role* and a *permission_list* values.  
The *role* column is a unique identifier that matches the value of the *role* row in the "users" table. The *permission_list* row is a serialized array containing the actual permissions.

> Multiple users can have the same "role" but a same user can't have two roles.

### ALL THE PERMISSIONS & DESCRIPTION.

They are listed in a file called `all-permissions.html` inside the `DOCS` folder (inside this documentation). The table is also loaded in `/admin/permissions` (or "Manage User Roles", since it was renamed)

## my-account

A way to access *some* of the functions of the **users** module without having the `manage_other_users` permission. So this is not a module by itself, but it *is* an endpoint of the API. So far the only method is to change the password. You can check the code at "/API/REST-Modules/api-my-account.php"

## pulse

An interface of "notifications" and "chats", to send "pulsations" to the user and let him be notified correctly of new chats or events. Notifications and chats are stored in the same table but differ on only the "sender" value. In chats, this value is set as the client code of the sender, while in notifications the value is empty.

## notifications

Every function that only the admin needs to fetch and act upon notifications is separated in this module. Keeping them separate is both security-wise and performance-wise.

## email

Send emails and retrieve the email logs.

All sent emails are stored in the logs.
The logs contain a "transaction_name" and "transaction_group" fields.
This identify the name of the particular transaction and the type/group of transactions it was sent as, respectively.

The `$module['email']->fetchLogs()` method can group logs in structures that make them easier to read.

This is an example structure: (written down in javascript/json syntax because it's easier to read)

```
fetchLogs = {
	"group_name": {
		"group_name": "group_name",
		"count": 20, //emails sent in this group
		"last_sends":{
			"date": "2016-04-21 00:09:00", // last date where emails where sent
			"count": 5 // number of emails sent that date
		}
		"template_used": "template_name", // if more than one template is used, this is the string ">1"
		/**
		 * this is the complete list of emails sent with this template. It's optional. It can be disabled by calling $module['email']->fetchLogs(["list"=>false]);
		 Additionally, you can *only* request this data. Calling $module['email']->fetchLogs(["list"=> "group_name"]);
		 */
		"list": [
			{
				"transaction_id": 150, // unique identifier for this transaction
				"date": "2016-04-21 00:09:00",
				"transaction_name": "sent_name",
				"to": { "name": "name", "email": "example1@example.com" }, //name can be missing
				"template": "template name", // can be missing
				"subject": "subject of the email",
				"status": "No error.", // either "No error" or an error string (as outputted by PHPMailer). If emails are disabled by the $config file, this will say something like "emails disabled by $config"
				"html": "<html></html>", // content of the email that was sent. This is sometimes ommited, for example when the email contains a password.

			},
			#...
		]
	}
};
```

Some notes on this:

- A "No error." email is an email that left the server. **This does mean it was _sent_, but not necessarily that it was _received_.** Because emails clients can filter through spam the mail in the process, or the email can be bounced (e.g. when the user's account doesn't exist), PHP isn't smart enough to tell by itself if the email was received.
- To avoid receiving a lot of data, the "html" field of each email can be ommited: `$module['email']->fetchLogs(["html"=>false])` (needless to say this is unnecessary if you set earlier`"list"` to `false`).
- You can also receive _only_ the "html" of a transaction, by transaction id: -supposing the transaction_id is 150: `$module['email']->fetchLogs(["html"=>150])` 

<style>
:root {
	--blockquote: #05CFB5;
}
#preview{ line-height: 2 }
pre{ word-wrap: break-word; white-space: pre-wrap; line-height: 1.25 }
blockquote{ border-color: var(--blockquote); font-size: 1.1em; }
</style>