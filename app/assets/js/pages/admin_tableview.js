/**
 * stepBar component.
 * in pages/tableview.php
 * @type {Object}
 */
var stepBar = {
    e: _('.progress-MISC'),
    step: 0,
    setStep: function (n){
        stepBar.step = n;
        // remove ".done" from all steps
        __('.step', stepBar.e).forEach(function (step){
            step.classList.remove('done');
        })
        //add ".done" to relevant ones
        //♣ start counting the first .step as .done when the current step is 2 ♣
        var  i = 2;
        __('.step', stepBar.e).forEach(function (step){
            if (i<=n) step.classList.add('done');
            i++;
        });
        return true;
    }
};
/**
 * fieldSet component
 * @type {Object}
 */
var fieldset = {
    setActive: function (fsetid){
        fieldset.isRegistered(fsetid);
        for(var currentValue in fieldset){
            if (!fieldset.hasOwnProperty(currentValue)) continue;
            if (typeof fieldset[currentValue].selector != "undefined"){
                __(fieldset[currentValue].selector).forEach(function(fieldset) { fieldset.classList.add('hidden'); });
            }
        };
        _(fieldset[fsetid].selector).classList.remove('hidden');
    },
    /**
     * Check if a fieldset is registered and warn (for internal debugging)
     */
    isRegistered: function (fsetid, name){
        if (typeof fieldset[fsetid] !== 'object')
        {
            console.log('Error: fieldset with id', fsetid, 'is not registered.');
            return false;        }
        if (name) if (typeof fieldset[fsetid][name] !== 'function')
        {
            console.log("Error: non existent fieldset's action", name);
            return false;        }
        return true;
    },
    /**
     * store "data" between fieldsets
     */
    data: {},
    getData: function(dataIndex){
        return this.data[dataIndex];
    },
    setData: function(dataIndex, data){
        return this.data[dataIndex] = data;
    },
    /**
     * @ select a table
     */
    "selectTable": {
        "selector": "#selectTable",
        // Function "select an option"
        "select": function (name){
            fieldset.setActive('uploadFile');
            fieldset.uploadFile.init(name);
        }
    },
    /**
     * @ upload a file
     */
    "uploadFile": {
        "selector": "#uploadFile",
        "init": function(table_name){
            stepBar.setStep(2);
            $('[data-toggle="tooltip"]').tooltip({html:true});
            $('[name="file"]').change(function(){
                $('[name="file"]').addClass('loading');
                uploadCSV(table_name, $('[name="table_headers_line"]').val(), function(data){
                    fieldset.setData('table_name', table_name);
                    fieldset.setActive('setTableSchema');
                    fieldset.setTableSchema.init();
                    show_message(JSON.parse(data).message, {duration:400});
                });
            })
        }
    },
    "setTableSchema": {
        "selector": "#setTableSchema",
        "init": function(){
            stepBar.setStep(3);
            ajaxNvars.requestNrender({endpoint:'dataview/tableSchemaForm',template:'tableSchemaForm','selector':'#tableSchemaForm', data:{table:fieldset.getData('table_name')}}, function(response){
                fieldset.setData('table_schema', response);
                $('[rel="txtTooltip"]').tooltip();
                $('#tableSchemaForm').addClass('loading');
                ajaxNvars.request({endpoint: 'dataview/get.json', data: {table:'/temp/'+fieldset.getData('table_name')}}, function(data){
                    $('#tableSchemaForm').removeClass('loading');
                    fieldset.setTableSchema.selectFilterColumnInit(data);
                    // inputSpy
                    fieldset.setTableSchema.startInputSpy();
                });
                window.setTimeout(function(){
                    // remove loading thingry and use "basic" version
                    $('#tableSchemaForm').removeClass('loading');
                },5000);
            });
        },
        "selectFilterColumnInit": function(data){
            var json =  JSON.parse(data);
            fieldset.setData('table_data', json);
            $('input[name="filter_content_column"]').click(function(){
                ajaxNvars.renderTemplate({template:'SelectFilterColumn', selector:'#SelectFilterColumn', data:json});
                $('select[name="filter_content_column"] [value="'+ $('input[name="filter_content_column"]').val() +'"]').prop('selected', true);
                $('input[name="filter_content_column"]').remove();
                input_spy = (fieldset.setTableSchema.input_spy || false);
                _('select[name="filter_content_column"]').onchange = function(){
                    input_spy = true;};
            });
        },
        "input_spy": false,
        "startInputSpy": function(){
            input_spy = (fieldset.setTableSchema.input_spy || false);
            $('#setTableSchema input').on('keydown', wr);
            $('#setTableSchema select').on('change', wr);
            
            function wr(){
                input_spy = true;
                return;
            }
        }
    },
    "confirm": {
        "selector": "#confirm",
        "init": function(){
            fieldset.setActive('confirm');
            // Upload the data from the form, if it's any different!
            request = {};
            if (input_spy){
                function valOf(index){
                    return _('[name="'+index+'"]').value;
                }
                request.table_schema = {
                    "nicename": valOf('nicename'),
                    "icon": valOf('icon'),
                    "permission_required": valOf('permission_required'),
                    "exclude_columns": valOf('exclude_columns').split(','),
                    "filter_content": valOf('filter_content') == 'true' ? true : false
                }
                if (request.table_schema.filter_content){
                    request.table_schema['filter_content_column'] = valOf('filter_content_column');
                    request.table_schema['filter_content_by_variable'] = valOf('filter_content_by_variable');
                }
                fieldset.setData('table_schema', request.table_schema);
            }
            request.table_name = fieldset.getData('table_name');
            ajaxNvars.request({
                endpoint: 'dataview/setTableSchema.json',
                data: request
            }, function(data){
                fieldset.setData('operationResume', data);
                var renderdata = {
                        table_schema: fieldset.getData('table_schema'),
                        operationResume: fieldset.getData('operationResume')
                    };
                ajaxNvars.renderTemplate({
                    template: 'ConfirmOperation',
                    selector: '#ConfirmOperation',
                    data: renderdata
                });
                $('[data-userVar]').each(function(){
                    $(this).attr('href', base_url +'/dataview/' +  fieldset.getData('table_name') + '?userVar=' + $(this).attr('data-userVar') + '&preview=true' );
                });
                stepBar.setStep(4);
            });
        }
    },
    "done":{
        "selector": "#done",
        "init": function(){
            stepBar.setStep(5);
            fieldset.setActive('done');
            $('#DoneMessage').addClass('loading');
            var data = {table_name: fieldset.getData('table_name')};
            if ($('[name="notify_users"][value="false"]').prop('checked')){
                //the user explicitely asked for no notifications to be sent
            }else{
                $('#DoneMessage').removeClass('loading').text('Sending Notifications...');
                data.sendNotifications = true;
            }
            ajaxNvars.request({
                endpoint: 'dataview/confirm.json',
                data:data
            }, function(response){
                console.log(response);
                ajaxNvars.renderTemplate({template:"DoneMessage", selector: "#DoneMessage"});
                $('#DoneMessage').removeClass('loading');
            });
        }
    }
}


function uploadCSV( table_name, table_headers_line, callback ){
    if (!$('[name="file"]')[0].files[0]) return show_message("Choose a file", {class:'alert-error'});
    var formData = new FormData();
    formData.append('file', $('[name="file"]')[0].files[0]);
    formData.append("table", table_name);
    formData.append("table_headers_line", table_headers_line);
    $.ajax({
            url : base_url + '/API/dataview/upload.json',
            type : 'POST',
            data : formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success : function(data) {
                callback(data);
            }
    }); 
}

/*

step[1] = function (){
    $('[data-toggle="tooltip"]').tooltip({html:true});
    $('#uploadCSV').click(function(){
        if (!$('[name="table[name]"]:checked').val()) return show_message("Choose a table first.", {class:'alert-error'});
        if (!$('#file')[0].files[0]) return show_message("Choose a file", {class:'alert-error'});
        var formData = new FormData();
        formData.append('file', $('#file')[0].files[0]);
        formData.append("table", $('[name="table[name]"]:checked').val());
        formData.append("table_headers_line", $('[name="table_headers_line"]').val());
        $.ajax({
                url : '<?php echo $config['base_url'] ?>/API/dataview/upload.json',
                type : 'POST',
                data : formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) {
                    step.data = JSON.parse(data);
                    step.data.table_name = $('[name="table[name]"]:checked').val();
                    step.data.table_headers_line = $('[name="table_headers_line"]').val();
                    console.log(step.data.message);
                    show_step(2);
                }
        }); 
    });
}

function preview_data_of(table_name){
    var value = $('#preview_data_of input').val();
    if (!value) return show_message('Specify a value in the input!', {duration:5000});
    var url = '<?php echo $config['base_url'] ?>/dataview/'+table_name+'?userVar='+value;
    show_message('View data for this user in the following link: <a href="'+url+'" class="btn btn-link" target="_blank">'+url+' <i class="fa fa-external-link"></i></a>')
}
step[2] = function(){
    virtualForm.create('#applyForm', function(data){
        step.data = data;
        show_step(3);
    });
}

$('[name="table[name]"]').on('change', function(){
    var choosed_table = $('[name="table[name]"]:checked').val();
    if (!choosed_table) return;
    if (step.current!=1 && !confirm('Are you sure you want to start from step one with table '+choosed_table+'?')){
        return false;
    }
    $('#choose-table .box-title').text(choosed_table);
    $('#choose-table .box-tools>button').click();
    $('#choose-table').addClass('box-success').removeClass('box-default');
    show_step(1);
});*/