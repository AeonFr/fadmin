<?php $template['header'] = "Edit Email Templates";
$template['breadcrumb'] = ["Admin" => "/admin", "Mails"=>"/admin/bulkmail"];
if (!has_permission('send_bulkmail')) die("You don't seem to have permission to access this particular page.");
?>
<style>body{margin:0}</style>
<script template="list" type="text/x-handlebars-template">
<p class="text-right">
	<span class="my2 mx1">ACTIONS:</span>
	<span onclick="state_new()" class="btn-group mx1 my1">
		<a  class="btn btn-lg btn-flat btn-success"><i class="fa fa-pencil-square"></i> new template</a>
	</span>
</p>
	{{#each .}}
		<div class="flex my1 border black-25">
			<div class="block flex-grow black-75 py1">
				<table>
					<tr>
						<td>
							{{#xif "this.startsWith('__')"}}
								<i class="fa fa-code text-warning px1"></i>
							{{else xif "this.startsWith('_')"}}
								<i class="fa fa-file-code-o text-warning px1"></i>
							{{else}}
								<i class="fa fa-file text-success px1"></i>
							{{/xif}}
						</td>
						<td class="TEMPLATE-NAME">{{#x "this.substr(0,this.length - 4)"}}{{/x}}</td>
					</tr>
				</table>
			</div>
			{{#xif "!this.startsWith('_')"}}
			<a class="pointer block border alert-warning py1 px2" onclick="state_delete('{{.}}')">
				<i class="fa fa-trash"></i> delete
			</a>
			{{/xif}}
			<a class="pointer block border alert-info py1 px2" edit="{{.}}">
				<i class="fa fa-pencil"></i> edit
			</a>
			<a class="pointer block border alert-info py1 px2" onclick="state_preview('{{.}}')" data-toggle="modal" data-target="#previewModal">
				<i class="fa fa-eye"></i> preview <sup>(beta)</sup>
			</a>
		</div>
	{{/each}}
</script>
<script template="edit" type="text/x-handlebars-template">
	<div class="box box-default box-solid from-bottom p-relative bottom-0">
		<div class="box-header">
			<h3 class="box-title">Edit template <code>{{name}}</code></h3>
			<div class="pull-right fa fa-close" onclick="state_list()"></div>
		</div>
		<textarea id="editor1" cols="30" rows="30" class="form-control box-body">{{content}}</textarea>
		<div class="box-footer">
			<div save="{{name}}" class="btn btn-flat btn-success">SAVE</div>
			<span class="px1 py1 text-muted">Last updated
				<span id="saved-ago" class="text-success">{{last_updated}}</span>
			</span>
			<div class="my1">
				<div class="btn btn-flat btn-default" data-toggle="modal" data-target="#previewModal" preview>PREVIEW (you will preview the last saved version)</div>
			</div>
		</div>
	</div>
</script>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-default box-solid STATE STATE-LIST">
			<div class="box-header"><h3 class="box-title">EMAIL TEMPLATES LIST:</h3></div>
			<div class="box-body">
				<div id="list">
					<div class="p-relative" style="min-height:300px">
						<div class="loading p-absolute"></div>
					</div>
				</div>
				<p>REFERENCE:</p>
				<div class="table-responsive">
					<div class="flex">
						<div class="flex-grow" style="flex-basis:33%">
							<table class="table"><tr><td><i class="fa fa-code text-warning px1"></i></td>
							<td>Files included in other templates.
							<i class="block text-muted">Prepended by "__". Include them in a template following this example:</i>
							<pre class="text-sm"><code>&lt;?php include "<b class="text-info">__header</b>.php"; ?></code></pre>
							<i class="block text-muted">Where the text marked in blue is the name of the template.</i></td>
							</tr></table>
					
							 </div>
						<div class="flex-grow" style="flex-basis:33%">
							<table class="table"><tr><td><i class="fa fa-file-code-o text-warning px1"></i></td>
							<td>System templates. They are used internally to handle things like account creation. Edit with caution, <em>important info is sent through this templates.</em>
							<i class="block text-muted">Prepended by "_"</i></td>
							</tr></table>
					
							 </div>
						<div class="flex-grow" style="flex-basis:33%">
							<table class="table"><tr><td><i class="fa fa-file text-success px1"></i></td>
							<td>User created templates. Can be created and deleted at any time.</td>
							</tr></table>
					
							 </div>
					</div>
				</div>
			</div>
		</div>
		<div class="STATE STATE-EDITOR"></div>
	</div>
</div>


<!-- Modal -->
<div id="previewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Preview Template</h4>
      </div>
      <div class="modal-body">
        <iframe class="block" style="width:100%;" frameborder="0"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php footer_section_start(); ?>
<style>
	textarea{ font-family:'Monaco',monospace; line-height: 1.618; }
</style>
<script>
	function showState(state_name){
		$('.STATE').addClass('hidden');
		$('.'+state_name).removeClass('hidden');
	}
	//
	function state_list(){
		showState('STATE-LIST');
		ajaxNvars.requestNrender({
			endpoint: 'bulkmail/emailTemplates',
			data: {"endpoint":"list"},
			template: 'list',
			selector: "#list"
		},
		function(data){			
			$('[edit]').click(function(){
				var name = $(this).attr('edit');
				state_edit(name);
			});
		});
	}
	function state_edit(name){
		showState('STATE-EDITOR');
		ajaxNvars.request({
			endpoint: 'bulkmail/emailTemplates',
			data: {"endpoint": "get", "name": name}
		},
		function(data){
			ajaxNvars.renderTemplate("edit", data, ".STATE-EDITOR");
			$('[preview]').on('click',function(){
				state_preview(name);
			})
			$('[save]').click(function(){
				try{
					var the_content = $('#editor1').val();
				}catch(e){
					show_message("Error obtaining the content of the document before sending to the server... try refreshing the page if the problem persists.",{class:'alert-error'});
				}
				if (typeof the_content !== 'string' || !the_content || the_content=='')
					show_message("Error obtaining the content of the document before sending to the server... try refreshing the page if the problem persists.",{class:'alert-error'});
				else{
					ajaxNvars.request({
						endpoint: 'bulkmail/emailTemplates',
						data: {endpoint: 'set', name: name, content: the_content }
					}, function(){
						show_message('Updated', {duration: 1000, class:'bg-black-75'});
						$('#saved-ago').text(new Date().toLocaleString());
					});
				}
			});
		});
	}
	function state_preview(name){
		console.log('preview', name);
		var html = "Woops! You shouldn't be viewing this";
		$.post('<?php echo $config['base_url'] ?>/API/bulkmail/previewTemplate',{"template": name.substr(0,name.length -4)})
		.done(function(data){
			html = data;
			try{
				var json = JSON.parse(data);
				html = json.html;
			}catch (e){
				console.log('This seems like an error.', e);
				show_message('It seems like the php script from the file '+name+' contains invalid characters. This might potentially be a security error: please correct it before sending any email with this template!', {class:'alert-error'})
			}
			loadModal(html);
		})
		.fail(function(err,b,c){
			console.log(err, b, c)
			html = err.responseText;
			loadModal(html);
		});
		function loadModal(html){
			$('#previewModal .modal-body>iframe').contents().find('html').html(html);
			$('#previewModal .modal-body>iframe').height( $('#previewModal .modal-body>iframe').contents().find('html').height() );
		}
	}
	function state_delete(name){
		if (name.startsWith('_')){
			show_message("You can't delete a template with that name from within this GUI. Please delete from the server directly."); return;
		}
		show_message('Are you sure you want to delete <code>'+name+'</code>? <a class="btn btn-default" id="confirm">YES</a>');
		$('#Message #confirm.btn').click(function(){
			ajaxNvars.request({
				endpoint: 'bulkmail/emailTemplates',
				data: {"endpoint": "delete", "name": name}
			}, function(data){
				show_message('Template deleted', 5000);
				state_list();
			});
		})
	}
	function state_new(){
		show_message('Please insert the name of the new template: <input type="text" style="color:black" id="newTemplateName"/> <a class="btn btn-success" id="newTemplateButton">Create</a>');
		$('#Message #newTemplateButton').click(function(){
			var value = $('#newTemplateName').val();
			if (value=='') return show_message('Name is invalid');
			if (value.startsWith('_')) return show_message('This web interface is unable to create a template with that name (starts with "_")');
			show_message('Processing...');
			if ( $('#list .TEMPLATE-NAME:contains("'+value+'")').length != 0 ){
				return show_message('That template already exists!');
			}
			var name = value + '.php';
			ajaxNvars.request({
				endpoint: 'bulkmail/emailTemplates',
				data: {"endpoint": "set", "name": name, "content": '...'}
			}, function(data){
				show_message('Template Created', 5000);
				state_edit(name);
			})
		})
	}
	state_list();
</script>
<?php footer_section_end(); ?>