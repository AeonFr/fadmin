<?php 
if (!has_permission("upload_data"))
	throw new Exception("It seems like you don't have permission to be here.");

require_module(['dataview']);

function require_permission($perm_name){
	if (!has_permission($perm_name)) return reply_with_error("Sorry! You don't have permission to access this place. Check back with the admin if you think this is a mistake.");
}

switch ($vars['endpoint']) {
	case 'upload.json':
		require_permission('upload_data');
		$table_name = $_POST['table'];
		$table_headers_line = $_POST['table_headers_line'];
		reply($module['dataview']->uploadTempMasterFile($table_name, $table_headers_line));
		break;
	case 'apply.json':
		require_permission('upload_data');
		$table_name = $_POST['table'];
		$table_headers_line = $_POST['table_headers_line'];
		/**
		 * Apply a temporary master file as the absolute file.
		 */
		reply($module['dataview']->applyTempMasterFile($table_name, $table_headers_line));
		break;
	case 'tableSchemaForm':
		 reply( $module['dataview']->table_schema[$_POST['table']] );
	break;
	case 'get.json':
		if (isset($_POST['filter_variable']) && has_permission('upload_data')){
			if (!isset($_POST['table'])) $_POST['table'] = $_POST['url_without_csv_extension'];
			reply($module['dataview']->getData($_POST['table'],$_POST['filter_variable']));
		}else reply($module['dataview']->getData($_POST['table']));
		break;
	case 'setTableSchema.json':
		if (isset($_POST['table_name']) && isset($_POST['table_schema']))
			$module['dataview']->setTableSchema($_POST['table_name'], $_POST['table_schema']);

		reply($module['dataview']->applyTempMasterFile($_POST['table_name'], 1));
		break;
	case 'confirm.json':
		if (!isset($_POST['sendNotifications']))
		reply($module['dataview']->confirmTempMasterFile($_POST['table_name'], 1, false));
		else reply($module['dataview']->confirmTempMasterFile($_POST['table_name'], 1, true));
		break;
	default:
		throw new Exception("Endpoint $vars[endpoint] doesn't exist");
		break;
}