<?php 
$template['header'] = "My Account"; ?>
<div class="row">
	<div class="col-md-6">
		<div class="box box-solid box-primary">
			<div class="box-header">Change your password</div>
			<form class="box-body" id="change-password" endpoint="my-account/change-password.json">
				<div class="form-group">
					Old Password:
					<input type="password" name="old_password" id="old_password" class="form-control" required>
				</div>
				<div class="form-group">
					New Password:
					<input type="password" name="new_password" id="new_password" class="form-control" required>
				</div>
				<div class="form-group">
					<label><input type="checkbox" id="show_new_password"> <span class="fa fa-eye"></span> Show password</label>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-flat btn-default"><i class="fa fa-lock"></i> Continue</button>
				</div>
			</form>
		</div>
		<div class="box box-solid box-primary">
			<div class="box-header">
				Email settings
			</div>
			<div class="box-body">
				<p>Control the type of emails you receive in your email inbox.</p>
				<a href="<?php echo "$config[base_url]/API/email-settings/?source=my-account" ?>" class="btn btn-block btn-flat btn-primary">MANAGE EMAIL SETTINGS</a>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box box-solid box-default">
			<div class="box-header">Your data</div>
			<div class="box-body">
				<table class="table">
					<tr>
						<th>Email</th><th>Name</th><th>Registered at</th>
					</tr>
					<tr>
						<td><?php echo $_SESSION['user']['email'] ?></td><td><?php echo $_SESSION['user']['name'] ?></td><td><?php echo $_SESSION['user']['registered_at'] ?></td>
					</tr>
				</table>
			</div>
		</div>
		
		<p>Want to change your email? Please contact <em><?php echo $config['admin_email'] ?></em></p>
	</div>
</div>

<?php footer_section_start(); ?>
<script>$('#show_new_password').change(function(){
	if ($(this).is(':checked'))
		$('#new_password').attr('type', 'text');
	else
		$('#new_password').attr('type', 'password');
	});
	virtualForm.create('#change-password', function(response){
		show_message("<b class='block'>Sweet! </b>"+response, {class: "alert-success", duration: 5000});
		$('#new_password').val('');
		$('#old_password').val('');
	});
	</script>
<?php footer_section_end(); ?>