<?php 
if (!has_permission('send_notifications'))
	throw new Exception("It seems like you don't have permission to access this data.");
require_module(['pulses', 'notifications']);
switch ($vars['endpoint']) {
	case 'new.json':
		if (!has_permission('send_notifications')){
			throw new Exception("You don't have permission to access this endpoint.");
		}else{
			$module['pulses']->send([
				"receiver" => explode(";", str_replace(" ", "", preg_replace("#\R+#", "", $_POST['to_users']))),
				"body" => $_POST['body'],
				"meta" => isset($_POST['meta']) ? $_POST['meta'] : []
			]);
			reply("Pulse sent correctly");
		}
		break;
	case 'list-unreceived.json':
		reply($module['notifications']->listUnreceived());
		break;
	case 'delete.json':
		check_required(['pulse_id']);
		reply($module['notifications']->delete($_POST['pulse_id']));
		break;
	case 'sendUnreceivedAsMail.json':
		reply($module['notifications']->sendUnReceivedAsMail());
		break;
	default:
		throw new Exception("Endpoint doesn't exist");
		break;
}