<?php
class database extends PDO{
	private function throwException(Exception $e, $query, $params=array()){
		if (has_permission('verbose_exceptions')){
			$trace = $e->getTrace();
			$trace_arr="";
			foreach($trace as $tr){
				$trace_arr .="
".$tr['file']." [".$tr['line']."]";
			}
			throw new Exception(
							'<br> <b>Error message:</b> ' . $e->getMessage() .
							" (Full error on the javascript Console)
<!-- FULL ERROR: " . '
'. __METHOD__ . ' <SQL exception:> ' . var_export($query, true) . '
<Parameters:> ' . var_export($params, true) . '
<Error information:> ' . var_export($this->errorInfo(), true) . "
<Error in file [line]:> ". $trace_arr . " [" . $e->getLine() . "]
-->" ,
							0,
							$e);
		}
		else
		{
			throw new Exception ("Error querying the database. Please contact support if the problem persists");
		}
	}
	public function __construct($dsn,
															 $username=null,
															 $password=null,
															 $driver_options=null)
	 {
			try {
				parent::__construct($dsn, $username, $password, $driver_options);
				parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch(PDOException $e) {
				echo 'Conección fallida: ' . $e->getMessage();
			}
	 }
	 public function getAssoc($sql, $params=array())
	{
		 try
		 {
				$stmt = parent::prepare($sql);
				$params = is_array($params) ? $params : array($params);
				$stmt->execute($params);

				return $stmt->fetchAll(PDO::FETCH_ASSOC);
				
		 }
		 catch (Exception $e)
		 {
			$this->throwException($e, $sql, $params);
		 }
	}
	public function query_escaped($query, $params=array()){
		try
			{
				$stmt = parent::prepare($query);
				$params = is_array($params) ? $params : array($params);
				$stmt->execute($params);
				return $stmt;
			}
		catch(Exception $e)
			{
				$this->throwException($e, $query, $params);
			}
	}
	 #get the number of rows in a result
	public function num_rows($query, $params=array())
		{
			try
			{
						$stmt =$this->query_escaped($query, $params);

						return $stmt->rowCount();
			}
			catch(Exception $e)
			{
				$this->throwException($e, $query, $params);
			}
		}
}