<?php 
/**
 * ADMIN MANAGEMENT OF NOTIFICATIONS
 * for the "public" API, view pulses.php
 */
class Notifications{
	function __construct(){
		global $config;
		$this->config = $config;
	}
	function listUnreceived(){
		global $db; global $module;
		$getAssoc = $db->getAssoc("SELECT * FROM pulses WHERE sender='' AND was_received=0");
		foreach($getAssoc as $key=>$row){
			$getAssoc[$key]["for"] = $module['pulses']->get_name_of($row['receiver']);
			$getAssoc[$key]["meta"] = array();
			$query_meta = $db->getAssoc("SELECT meta_key, meta_value FROM pulse_meta WHERE pulse_id=?", $row['pulse_id']);
			foreach ($query_meta as $value) {
				$getAssoc[$key]["meta"][$value['meta_key']] = $value['meta_value'];
			}
		}
		return $getAssoc;
	}
	function delete($pulse_id){
		global $db;
		$db->query_escaped("DELETE FROM pulses WHERE sender='' AND pulse_id=?", $pulse_id);
		$db->query_escaped("DELETE FROM pulse_meta WHERE pulse_id=?", $pulse_id);
		return true;
	}
	function sendUnreceivedAsMail(){
		global $db; global $module;
		require_module(['users', 'pulses', 'email']);
		$getListOfUnreceived = $this->listUnreceived();
		$groupByCode = array();
		foreach($getListOfUnreceived as $key=>$row){
			try{
				if (!isset($groupByCode[$row['receiver']])){
					$groupByCode[$row['receiver']] = array(
					"userData"=>$module['users']->getUserData($row['receiver']),
					"unreceivedNotifs"=>array());
				}
				array_push($groupByCode[$row['receiver']]["unreceivedNotifs"], $module['pulses']->buildNotifRow($row));
			}catch(Exception $e){}
		}
		/**
		 * - Check if the user is subscribed to the email notifications
		 * - If he is, send him an email and mark all notifications as "received" for each user.
		 * - Else, do nothing... (not much to be done)
		 */
		$transactions = array();
		foreach($groupByCode as $client_code => $cli_obj){
			if ($this->userIsSubscribed2Notifs($client_code)){
				$transaction = $module['email']->send(
				"Send Unreceived Notifications to $client_code - ".$cli_obj['userData']['name'],
				"Send Unreceived Notifications",
				[
					"template" => "_sendUnreceivedNotifications",
					"template_vars" => array( "unreceivedNotifs" => $cli_obj["unreceivedNotifs"]),
					"to_name" => $cli_obj['userData']['name'],
					"to_email" => $cli_obj['userData']['email'],
					"subject" => "Unreceived Notifications"
				]);
				$db->query_escaped("UPDATE pulses SET was_received=1 WHERE sender='' AND receiver=?", $client_code);
				array_push($transactions, $transaction);
			}
		}
		return $transactions;
	}
	function userIsSubscribed2Notifs($client_code){
		global $db; global $module;
		require_module('users');
		$check = $module['users']->get_meta($client_code, "disable_notifs_by_mail");
		// if this meta value is SET, then the user has unsubscribed
		if (!$check) return true;
		//else, if it's NOT SET, user is subscribed
		else return false;

	}
}
$module['notifications'] = new Notifications();