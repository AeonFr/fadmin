<?php 
/**
 * Bootstrap layout,
 * for error pages & login pages
 * This layout works as a header - The content must be outputed before including it
 */
require_once "config.php";
?><!DOCTYPE html>
<html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<title><?php echo (isset($layout['title'])) ? $layout['title'] : $config['page_title'] ?></title>
 	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 	<link rel="stylesheet" href="<?php echo $config['base_url'] ?>/AdminLTE/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $config['base_url'] ?>/AdminLTE/dist/css/AdminLTE.min.css">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
 </head>
 <body>

<div class="wrapper">
<header class="main-header">
	<!-- Logo -->
	<a href="<?php echo $config['base_url'] ?>" class="logo">
	  <!-- logo for regular state and mobile devices -->
	  <span class="logo-lg"><?php echo (isset($layout['title'])) ? $layout['title'] : $config['page_title'] ?></span>
	</a>
</header>
</div>
<div class="container">
