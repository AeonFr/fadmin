<?php $template['header'] = $config['page_title'] ?>
<div class="row">
	<div class="col-lg-8">
		<div class="box box-default">
		  <div class="box-header with-border">
		    <h3 class="box-title">Hello, <?php echo $_SESSION['user']['name'] ?></h3>
		    <div class="box-tools pull-right">
		      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		    </div><!-- /.box-tools -->
		  </div><!-- /.box-header -->
		  <div class="box-body">
			<h4>You have signed-in as <?php echo $_SESSION['user']['email'] ?>
			<hr>
			<a class="btn btn-xs btn-flat btn-default" href="<?php echo $config['base_url'] ?>/sign-out">Sign out</a></h4>
		  </div><!-- /.box-body -->
		</div><!-- /.box -->
		<!-- WIDGETS -->
		<?php include "notifications.php" ?>
	</div>
	<div class="col-lg-4">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">What to do...</h3>
				<div class="box-tools pull-right">
			      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			    </div><!-- /.box-tools -->
			</div>
			<div class="box-body sidebar-menu sidebar-menu-inverse" id="menu">
			</div>
		</div>
	</div>
</div>
<?php footer_section_start(); ?>
<style>
.sidebar-menu-inverse>li.active>a {    background: white!important;    color: #1e282c!important;    border: none;}
.sidebar-menu-inverse .treeview-menu{    background-color: white!important;}
.sidebar-menu-inverse .treeview-menu>li.active>a {    color: #2c3b41;}
</style>
<script>
	_('#menu').innerHTML  = _('ul.sidebar-menu').innerHTML;
	__('#menu li').forEach(function(current){
		current.classList.add('active');
	})
</script>
<?php footer_section_end(); ?>