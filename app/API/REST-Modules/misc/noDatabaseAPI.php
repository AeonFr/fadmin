<?php 
/**
 * Adapted from:
 * https://github.com/AeonFr/noDatabaseAPI
 */

$noDatabaseAPIConfig = array(
	"API_KEY" => "dGhpc0lzLVZlcnlSYW5kb20",
	"READ_ONLY_KEY" => false,
	// folder in wich all the HTML files will be saved.
	"files_folder" => "$config[root]/_noSQL/email_templates/",
	"allowed_extensions" => '/(.html|.php)/'
	);
/**
 * No Database API
 The simplest, buggiest RESTFUL API to write data inside HTML files (TM).
 A.K.A.: A no-database database. Get it? Because it's a base of data. But they're just files. Lol. BTW you should probably keep using sublime text and your FTP client. This software is crap.
 * Licenced Creative Uncommons or at Least Very Rare.
*/
// THE RULES OF THE GAME:
// // // // // // // //
// #1 - Every transaction is requested via POST and contains AT LEAST the following data:
/*
{
	"key": "your api key"
	"endpoint": "set"
}
*/
// 
// #2 - There are only 4 "ENDPOINTS" or "METHODS:
/*
 set -> call this method with name, and content. Name represents a filename, content represent a string to write inside the file. The script doesn't give a flying fuck on wich name you choose, so it's going to change it in order to make it a valid file name (your asscii art won't work, srry) and it will return it in the form of {"name": "..."}
 get -> gets the content of a file. Just name it.
 list -> lists all files, returns an array with their names.
 delete -> you guessd it, soldier.
 */
// #3 - Every response is sent as a JSON object.
// #4 - Thoug shall use an API KEY, and Thoug shall declare it inside "config.php"
// (there's also other things to set there. Check it out...)
class noDatabaseAPI {
	function __construct(){
		//require_once "config.php";
		global $noDatabaseAPIConfig;
		$this->API_KEY = $noDatabaseAPIConfig['API_KEY'];
		$this->READ_ONLY_KEY = $noDatabaseAPIConfig['READ_ONLY_KEY'];
		$this->files_folder = $noDatabaseAPIConfig['files_folder'];
		$this->allowed_extensions = $noDatabaseAPIConfig['allowed_extensions'];
		if (!empty($_POST) && isset($_POST['key']) && isset($_POST['endpoint'])):
			header('Content-Type: application/json');
			//for organized access, compile all $_POST variables inside $this->request
			$this->request = [
				"key" => $_POST['key'],
				"endpoint" => $_POST['endpoint']
			];
			// "name" is not allways required. "list" endpoint does not need it.
			if (isset($_POST['name'])) $this->request['name'] = $_POST['name'];
			// "content" is currently only used for "set" endpoint.
			if (isset($_POST['content'])) $this->request['content'] = $_POST['content'];
			switch ($this->request['endpoint']) {
				case 'set':
					$this->set();
					break;
				case 'get':
					$this->get();
					break;
				case 'list':
					$this->list_is_a_fucking_protected_word();
					break;
				case 'delete':
					$this->delete();
					break;
			}
		endif;
	}
	// #5 - All errors throw 400 status code and the following json:
	/*
		{"error":"error_message"}
	*/
	private function error($message){
		http_response_code(400);
		die(json_encode(["error"=>$message]));
	}
	private function validate($read_only=false){
		if (!isset($this->request)) $this->error("You must send your requests as a JSON encoded string, containing at least the following data: {'key':'your_api_key','request':'request_type'}");
		if (!isset($this->request['key'])) $this->error("Your api keys weren't send at all. If you think you may have lost them, refer to the 'Lost and Found' department. Thanks.");
		if (!$read_only){
		}
		if (!$read_only && $this->API_KEY !== $this->request['key']){
			$this->error("Invalid API key.");
		}else if ($read_only){
			//in read_only mode, both API_KEY and READ_ONLY_KEY are valid.
			if($this->API_KEY !== $this->request['key'] || $this->READ_ONLY_KEY !== $this->request['key']){
				$this->error("Invalid API key.");
			}
		} 
		else return true;
	}
	private function checkData($string_or_array){
		if (!is_array($string_or_array)) $array = array($string_or_array);
		else $array = $string_or_array;
		foreach ($array as $key => $value) {
			if (!isset($this->request[$value])) $this->error("The value for $value is required, yet, it was not sent with the request, or maybe it was sent in the wrong format? I'm sure I've checked correctly, but I shall check again. Do you wan't a cup of tea while you wait?");
		}
	}
	/**
	 * Escapes the name to be "file friendly". Only A to Z and numbers, space & dashes. Yep. Merry Xmas.
	 */
	private function escape_name(){
		$r1 = str_replace('.'.pathinfo($this->request['name'], PATHINFO_EXTENSION), '', $this->request['name']);
		$r2 = preg_replace('/[^A-Za-z0-9_\-\ ]/', '_', $r1);
		return $r2.'.'.pathinfo($this->request['name'], PATHINFO_EXTENSION);
	}
	/**
	 * Checks if file exists, dies with error if false
	 */
	private function file_exists($file_addr){
		if (!file_exists($file_addr)) $this->error("The file ".$this->request['name']." does not exist.");
	}
	/**
	 * Checks if the extension is valid, before writting the file
	 */
	private function valid_file_extension($path){
		if (!preg_match($this->allowed_extensions,pathinfo($path, PATHINFO_EXTENSION))) return true; else $this->error("Invalid Extension!");
	}
	public function set(){
		$this->validate();
		$this->checkData(['name', 'content']);
		$escaped_name = $this->escape_name();
		$this->valid_file_extension($escaped_name);
		//does the file exists?
		//or we should create it?
		//nevermind. PHP's function for opening/creating is the same
		$file = fopen($this->files_folder.$escaped_name, "w") or die($this->error("You don't have access to the folder, your permissions are not setted correctly or something like that."));
		fwrite($file, $this->request['content']);
		fclose($file);
		die(json_encode(["name"=>$escaped_name]));
	}
	public function get(){
		$this->validate();
		$this->checkData('name');
		$escaped_name = $this->escape_name();
		$file_addr = $this->files_folder.$escaped_name;
		$this->file_exists($file_addr);
		$content = file_get_contents($file_addr);
		$last_updated = date("Y-m-d H:i:s", filemtime($file_addr));
		die(json_encode(["name"=>$escaped_name,"content"=>$content,"last_updated"=>$last_updated]));
	}
	public function list_is_a_fucking_protected_word(){
		$this->validate();
		$directory = $this->files_folder;
		$scanned_directory = array_diff(scandir($directory), array('..', '.', 'fragments'));
		die(json_encode($scanned_directory));
	}
	public function delete(){
		$this->validate();
		$escaped_name = $this->escape_name();
		$file_addr = $this->files_folder.$escaped_name;
		$this->file_exists($file_addr);
		unlink($file_addr);
		die(json_encode(["name"=>$escaped_name]));
	}
}
$noDatabaseAPI = new noDatabaseAPI();