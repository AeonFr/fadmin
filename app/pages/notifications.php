<style>
	.menu{
		list-style: none; padding-left: 0;
		font-size: 1.2em;
	}
</style>
<script template="notifs-fetch" type="text/x-handlebars-template">
	<div class="pull-right">
		<span>
			<span showUnreadNotifs>0</span> Unread Notifications
		</span>
		<div onclick="ajaxNvars.request('pulses/readAllNotifs.json')" class="btn btn-default btn-flat">Mark All as Read</div>
	</div>
	<div class="clearfix"></div>
	<div class="my1"></div>
	<ul class="menu">
		{{#each notifs.list}}
		<li class="py2 px2">
			<a {{#if link}} href="{{link}}" {{/if}}><div class="text-muted text-sm px1"><i class="fa fa-clock-o"></i> {{sent_at}} ago</div>
			<span class="pre-line block py1 {{#if read}}text-muted{{/if}}" style="padding-top:0">{{#if icon}}<i class="{{icon}}"></i>{{/if}} {{body}}</span></a>{{{custom_html}}}
		</li>
		{{/each}}
		{{#unless load_all}}{{#xif "this.notifs.list.length == 10"}}
			<li class="px2 py2 btn-block btn-default btn-flat btn" onclick="requestNotifs(9999, true)">Displaying last 10 notifications. Click to load all!</li>
		{{/xif}}
		{{/unless}}
	</ul>
	{{#unless this.notifs.list}} <h3 class="px1 text-center text-muted py2">
	<big class="fa fa-bell-o"></big><br>
	You don't have notifications
	</h3>
	{{else}}
	<small class="block px1 text-muted">* Time is estimated and calculated only when the page is loaded.</small>
	{{/unless}}
</script>
<div class="box box-solid box-default">
	<div class="box-body">
		<div id="target-notifs-fetch">
			<div class="px2 py2 mx2 my2 text-muted text-center">Loading...</div>
		</div>
	</div>
</div>

<?php footer_section_start(); ?>
<script>
var tm = 0,
localCacheOBJ = {};
function requestNotifs(number, load_all){
	ajaxNvars.request('pulses/getNotifs.json', {last: number}, function(data){
		localCacheOBJ = {"chats": {}, "notifs": data, "load_all": load_all};
		ajaxNvars.renderTemplate('notifs-fetch', localCacheOBJ, '#target-notifs-fetch');
		pulse.onRender = function(data){
			if (data.notifs.list){
				jQuery.merge(data.notifs.list, localCacheOBJ.notifs.list);
				localCacheOBJ.notifs.list = data.notifs.list;
				ajaxNvars.renderTemplate('notifs-fetch', localCacheOBJ, '#target-notifs-fetch');
			}
			pulse.render();
		}
	})
}
requestNotifs(10, false);
</script>
<?php footer_section_end(); ?>