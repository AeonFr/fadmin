<?php 
if (!has_permission("manage_other_users"))
	throw new Exception("You don't have permission to manage other users.");
require_module('users');
switch ($vars['endpoint']) {
	case 'list.json':
		reply($module['users']->listAll());
		break;
	case 'single.json':
		check_required(['client_code']);
		reply($module['users']->getUserData($_POST['client_code']));
		break;
	case 'add.json':
	case 'edit.json':
		$data = array(
			"client_code" => $_POST['client_code'],
			"email" => $_POST['email'],
			"name" => $_POST['name'],
			"role" => $_POST['role'],
			"client_pan" => $_POST['client_pan'],
			"phone_num" => $_POST['phone_num']
		);
		if (isset($_POST['password']) && $_POST['password'] == "") unset($_POST['password']);
		if (isset($_POST['password']) && $_POST['password'] != "") $data['password'] = $_POST['password'];
		if (isset($_POST['send_password_by_email'])) $send_password_by_email = true;
		else $send_password_by_email = false;
		if ($vars['endpoint'] == 'add.json'){
			$userVars = $module['users']->add($data, $send_password_by_email);
			if (is_array($userVars) && $userVars != NULL && $userVars != false){
				reply ([ "message" => "User '$_POST[name]' was added successfully."]);
			}else{
				$errorMsg = "There was an error processing the request.";
				if (has_permission('verbose_exceptions'))
					$errorMsg .= "The script should have returned user data, instead it returned the following value: ". var_export($userVars, true);
				reply_with_error($errorMsg);
			}
		}else{
			$userVars = $module['users']->edit($data, $send_password_by_email);
			if (is_array($userVars) && $userVars != NULL && $userVars != false){
				reply ([ "message" => "User '$_POST[name]' was edited successfully."]);
			}else{
				$errorMsg = "There was an error processing the request.";
				if (has_permission('verbose_exceptions'))
					$errorMsg .= "The script should have returned user data, instead it returned the following value: ". var_export($userVars, true);
				reply_with_error($errorMsg);
			}
		}
		break;
	case 'remove.json':
		check_required(['client_code']);
		if ($module['users']->remove($_POST['client_code'])){
			reply(["message"=>"User with code '$_POST[client_code]' was removed successfully."]);
		}
		break;
	default:
		throw new Exception("Endpoint doesn't exist");
		break;
}