<?php
$template['header']= "Data Management";

if (!has_permission('filebase')) die('502');

if (!isset($_GET['folder'])){
    $folder = "/";
}
 else   $folder = $_GET['folder'];
?>
<div class="filebox-title">

    <a class="btn btn-lg" href="/admin/filebase"><span class="fa fa-home"></span></a>

    <a class="btn btn-lg"> > <?php echo $folder ?></a>
   
</div>
<?php
$database_root = $config['root']"/_noSQL/";

function filter_syst($var){
    switch ($var) {
        case '.':
        case '..':
        case 'fragments':
        return false;
            break;
        default:
        return true;
            break;
    }
}


$content = array_filter(scandir($database_root), "filter_syst");
$organized_content = [];

foreach ($content as $i=>$item){
//    if ($item)
}


?>
<div class="filebox-container flex flex-wrap">
    <a class="filebox" href="<?php base_url() ?>/admin/filebase/data/openpositions" data-id="">
        <div class="file-icon folder">
            <svg class="js-geomicon geomicon" data-icon="folder" viewBox="0 0 32 32" style="fill:currentcolor"><title>folder icon</title><path d="M0 4 L0 28 L32 28 L32 8 L16 8 L12 4 z"></path></svg>
        </div>
        <time>2016-05-10 16:50</time>
        <h1>openpositions</h1>
    </a>
    <a class="filebox" href="<?php base_url() ?>/admin/filebase/data/ledgers" data-id="">
        <div class="file-icon folder">
            <svg class="js-geomicon geomicon" data-icon="folder" viewBox="0 0 32 32" style="fill:currentcolor"><title>folder icon</title><path d="M0 4 L0 28 L32 28 L32 8 L16 8 L12 4 z"></path></svg>
        </div>
        <time>2016-05-10 16:50</time>
        <h1>ledgers</h1>
    </a>
    <a class="filebox" href="" data-id="">
        <div class="file-icon folder">
            <svg class="js-geomicon geomicon" data-icon="folder" viewBox="0 0 32 32" style="fill:currentcolor"><title>folder icon</title><path d="M0 4 L0 28 L32 28 L32 8 L16 8 L12 4 z"></path></svg>
        </div>
        <time>2016-05-10 16:50</time>
        <h1>holdings</h1>
    </a>
    <a class="filebox" href="" data-id="">
        <div class="file-icon csv">
            <svg class="js-geomicon geomicon" data-icon="file" viewBox="0 0 32 32" style="fill:currentcolor"><title>file icon</title><path d="M4 2 L4 30 L28 30 L28 10 L20 2 z"></path></svg>
        </div>
        <time>2016-05-10 16:50</time>
        <h1>table_schema.json</h1>
    </a>
</div>


<link rel="stylesheet" href="<?php base_url(); ?>/plug-ins/data-management/style.css">