<?php 

$module = array();

/**
 * Refer to DOCS/modules-API.md
 * for the documentation/explanation on this
 * @param [string or array] $required_modules Must match exactly the name of a file inside "includes/classes/modules"
 */
function require_module($required_modules){
	global $config;
	global $db;
	global $module;
	if (!is_array($required_modules)) $required_modules = array($required_modules);
	foreach($required_modules as $module_name){
		require_once "$config[root]/includes/modules/$module_name.php";
	}
};

function base_url(){ global $config; echo $config['base_url']; }

/**
 * Verify if the current user has a permission.
 * Check the DOCS/modules-API.md file under "permissions"(name of the module)
 * to understand how they work
 */
function has_permission($perm_name){
	if (!isset($_SESSION['user'])) return false;
	if (in_array($perm_name, $_SESSION['user']['permission_list'])) return true;
	return false;
}

// to use inside /pages/....
// Sends a section to the footer.
function footer_section_start(){
	ob_start();
}
function footer_section_end(){
	global $template;
	if (isset($template['footer']))
		$template['footer'] .= ob_get_clean();
	else
		$template['footer'] = ob_get_clean();
}
//helpers
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}