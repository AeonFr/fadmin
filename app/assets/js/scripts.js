var base_url = (base_url || "/app");

// Page visibility API polyfill
// https://www.w3.org/TR/page-visibility/
// This will add the "status" attribute to "visible" to the document body if the page is currently active,
// or "hidden" if the tab is not focused
(function() {
  var hidden = "hidden";

  // Standards:
  if (hidden in document)
    document.addEventListener("visibilitychange", onchange);
  else if ((hidden = "mozHidden") in document)
    document.addEventListener("mozvisibilitychange", onchange);
  else if ((hidden = "webkitHidden") in document)
    document.addEventListener("webkitvisibilitychange", onchange);
  else if ((hidden = "msHidden") in document)
    document.addEventListener("msvisibilitychange", onchange);
  // IE 9 and lower:
  else if ("onfocusin" in document)
    document.onfocusin = document.onfocusout = onchange;
  // All others:
  else
    window.onpageshow = window.onpagehide
    = window.onfocus = window.onblur = onchange;

  function onchange (evt) {
    var v = "visible", h = "hidden",
        evtMap = {
          focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h
        };

    evt = evt || window.event;
    if (evt.type in evtMap)
      document.body.setAttribute('status', evtMap[evt.type]);
    else
      document.body.setAttribute('status', this[hidden] ? "hidden" : "visible");
  }

  // set the initial state (but only if browser supports the Page Visibility API)
  if( document[hidden] !== undefined )
    onchange({type: document[hidden] ? "blur" : "focus"});
})();


/*************************************************************************************************/
/********************                         HELPERS                         ********************/

/*You must include handlebars.js before this file.
 * This little handlebars helper will allow the use of more
 * complete if conditions.
 * Source: https://gist.github.com/akhoury/9118682
 * a helper to execute an IF statement with any expression
    USAGE:
   -- Yes you NEED to properly escape the string literals, or just alternate single and double quotes 
   -- to access any global function or property you should use window.functionName() instead of just functionName()
   -- this example assumes you passed this context to your handlebars template( {name: 'Sam', age: '20' } ), notice age is a string, just for so I can demo parseInt later
   <p>
     {{#xif " this.name == 'Sam' && this.age === '12' " }}
       BOOM
     {{else}}
       BAMM
     {{/xif}}
   </p>
   */
Handlebars.registerHelper("xif", function (expression, options) {
	return Handlebars.helpers["x"].apply(this, [expression, options]) ? options.fn(this) : options.inverse(this);
});
Handlebars.registerHelper("x", function (expression, options) {
  var fn = function(){}, result;

  // in a try block in case the expression have invalid javascript
  try {
    // create a new function using Function.apply, notice the capital F in Function
    fn = Function.apply(
      this,
      [
        'window', // or add more '_this, window, a, b' you can add more params if you have references for them when you call fn(window, a, b, c);
        'return ' + expression + ';' // edit that if you know what you're doing
      ]
    );
  } catch (e) {
    console.warn('[warning] {{x ' + expression + '}} is invalid javascript', e);
  }

  // then let's execute this new function, and pass it window, like we promised
  // so you can actually use window in your expression
  // i.e expression ==> 'window.config.userLimit + 10 - 5 + 2 - user.count' //
  // or whatever
  try {
    // if you have created the function with more params
    // that would like fn(window, a, b, c)
    result = fn.call(this, window);
  } catch (e) {
    console.warn('[warning] {{x ' + expression + '}} runtime error', e);
  }
  // return the output of that result, or undefined if some error occured
  return result;
});

/**
 * General functions I use to make vanilla JS simple as hell
 */
var _    =  function (q, ctx){ return (ctx||document).querySelector(q); };
/**
 * Return a nodeList as an array
 * e.g.: loop through all "p" selectors
 * __('p').forEach(function(p) { #... }
 */
function __(q, c) {for(var n = (c||document).querySelectorAll(q), a = [], i=n.length;i;a[--i]=n[i]);return a;}
/**
 * create an element by passing it as text "<div></div>"
 */
var elt  =  function(txt) {
	var div = document.createElement('div');
	div.innerHTML = txt;
	return div.removeChild(div.firstChild);
}



function show_message(message, options){
	console.log(message);
	$('#Message').remove(); //remove previous messages
	if (!options) options = {};
	if (!options.class) { options.class = '' };
	var element = elt('<div id="Message" class="'+ options.class +' p-fixed top-auto left-0 right-0 bottom-0 px2 py1 bg-black white z-1000"><span id="closeMessage" class="pull-right fa fa-close pointer px1 py1"></span>'+message+'</div>');
	document.body.appendChild(element);
	_('#closeMessage').onclick = close_message;
	if (options.duration){
		window.setTimeout(close_message, options.duration);
	}
}
function close_message(){
	$('#Message').remove();
}

var ajaxNvars = {};

var virtualForm = {};

var pulse = {};

(function(){
	/**
	 * [ajaxNvars description]
	 * Class to load data via handlebars and render the result using a template
	 * @type {Object}
	 * [ajaxNvars.renderTemplate description]
	 * Proxy function for handlebar's render template...
	 * The templates are stored in ajaxNvars.cache object,
	 * so they are not executed by handlebars next time. 
	 * The template name matchs the selector [template="template_name"]
	 * Is a good idea to make this selector a hidden DOM element,
	 * or even better: a script tag.

	<script template="template_name" type="text/x-handlebars-template">
	<!-- Template content -->
	</script>

	 */
	ajaxNvars.cache = {}
	/**
	 * 			
	 * @param {string} template_name        Name of the [template] attribute
	 *                                      of the handlebars template HTML.
	 * @param {object} data                 Data object, to be injected in the template.
	 * @param {string} renderSelector       Selector in wich to inject the template into.
	 *
	 * Alternative use:
	 * ajaxNvars.renderTemplate({
	 * "template": template_name,
	 * "data": data,
	 * "selector": renderSelector
	 * });
	 *
	 * If renderSelector is omitted, last used will be used.
	 */
	ajaxNvars.renderTemplate = function(template_name, data, renderSelector){
		if ( typeof template_name == "object" ){
			var config = template_name;
			template_name = config.template;
			data = (config.data || {});
			renderSelector = config.selector;
		}
		if (!renderSelector){
			renderSelector = ajaxNvars.lastRenderSelector;
		}else{
			ajaxNvars.lastRenderSelector = renderSelector;
		}
		if (!ajaxNvars.cache[template_name]){
			var source   = _("[template='"+template_name+"']").innerHTML;
			try{
				ajaxNvars.cache[template_name] = Handlebars.compile(source);
			}catch(e){
				console.log("Error compiling template "+template_name, e);
				return false;
			}
		}
		var html = ajaxNvars.cache[template_name](data);
		_(renderSelector).innerHTML = html;
		return true;
	}
	/**
	 * jQuery standard request, with error handling.
	 * Alternative function call:
	 * ajaxNvars.request({
	 * "endpoint": "example/method.json",
	 * "data": { #... }, // Data to be send alongside the request
	 * "onFail": function(xhr, textResponse, error){} //This function executes on failure, after the usual error message
	 * }, function(response){
	 * 	//success function. Everything went OK
	 * });
	 */
	ajaxNvars.request = function(endpoint, request_data, callback){
		if (typeof endpoint=="object"){
			var data = endpoint;
			callback = request_data;
			endpoint = data.endpoint;
			request_data = (data.data || {});
		}else if (!callback && typeof request_data == "function"){
			callback = request_data;
			request_data = {};
		}
		$.post(base_url+"/API/"+endpoint, request_data)
		.done(function(response){
			/** Everything went O.K. */
			try{
				if (typeof response !== 'object')
					var json_response = JSON.parse(response);
				else
					var json_response = response;
			}catch(e){
				show_message("Error in last request. If the error persists, try refreshing the page. <!--Endpoint:"+endpoint+" -->", {class: "alert-danger"});
				return console.log(response);
			}
			if (callback) callback(json_response);
		})
		.fail(function(xhr, textResponse, error){
			var json_response = (JSON.parse(xhr.responseText) || false),
				errorMessage = (json_response) ? json_response.error : xhr.responseText;
			show_message('<strong>Error:</strong><div id="textResponse">'+errorMessage+'</div>', {class: "alert-danger"});
			if (data && data.onFail) data.onFail(xhr, textResponse, error);
		});
	}
	/**
	 * Send a POST request, render a template using the response.
	 * config = {
	 * 	"endpoint": "list.json",
	 * 	"template": "template_name",
	 * 	"selector": "renderSelector",
	 * 	"data": {}
	 * }
	 */
	ajaxNvars.requestNrender = function(config, callback){
		var endpoint = config.endpoint,
		template_name = config.template,
		renderSelector = config.selector,
		request_data = config.data || {};
		_(renderSelector).classList.add('o-25');
		ajaxNvars.request(endpoint, request_data, function(data){
			if (ajaxNvars.renderTemplate(template_name, data, renderSelector)){
				_(renderSelector).classList.remove('o-25');
				if (callback) callback(data);
			}else{
				console.log('There was an error rendering the template with the following data.', data);
			}
		});
	}
/**
 * [virtualForm]
 */
 	virtualForm.parseForm = function(selector){
		//# parseForm = parse the contents of a form into an object, ready to send to server.
		// the data is appended to the contents of virtualForm.data object (its null by default)
		// that way you can append data to a request by modifying this variable
		var data = (virtualForm.data || {});
		$(selector + ' [name]:not([disabled])').each(function(index){
			var input_type = $(this).attr('type');
			if ( (input_type !== 'checkbox' && input_type !== 'radio') 
			|| ( (input_type=='checkbox' || input_type=='radio') && $(this).is(':checked') ) ){
				var name = $(this).attr('name'),
					value = $(this).val();
				data[name] = value;
			}
		});
		return data;
 	}
	virtualForm.create = function(selector, callback){
		$(selector).on('submit', function(e){
			e.preventDefault();
			var endpoint = $(this).attr('endpoint'),
			data = virtualForm.parseForm(selector);
			$(selector + ' [type="submit"]').addClass('disabled').attr('type', 'onProgress-submit');
			ajaxNvars.request({
				"endpoint": endpoint,
				"data": data,
				"onFail": function(a,b,c){
					console.log()
					$(selector + ' [type="onProgress-submit"]').removeClass('disabled').attr('type', 'submit');
				}
			},
			function(responseText){
				// on success...
				$(selector + ' [type="onProgress-submit"]').removeClass('disabled').attr('type', 'submit');
				if (callback) callback(responseText);
			})
		})
	}

	/**
	 * pulse
	 */
	
pulse.cacheOBJ = {};

pulse.templates = {"chats": false, "notifs": false};
pulse.createTemplates = function(){
	var source_chats = sessionStorage.getItem('template_chats'),
		source_notifs = sessionStorage.getItem('template_notifs');
	pulse.templates.chats = Handlebars.compile(source_chats);
	pulse.templates.notifs = Handlebars.compile(source_notifs);
}
pulse.render = function(){
	var new_chatRenderContent = pulse.templates.chats(pulse.cacheOBJ.chats);
	_('#chatRenderTemplate').innerHTML = new_chatRenderContent;

	var new_notifRenderContent =  pulse.templates.notifs(pulse.cacheOBJ.notifs);
	_('#notifsRenderTemplate').innerHTML = new_notifRenderContent;
	

	__('[showUnreadChats]').forEach(function(currentValue){
		currentValue.innerHTML = pulse.cacheOBJ.chats.count;
	});
	__('[showUnreadNotifs]').forEach(function(currentValue){
		currentValue.innerHTML = pulse.cacheOBJ.notifs.count;
	});

}
pulse.requestCacheOBJ = function(template_content){
	var request = {},
		template_content = true;
	if (!sessionStorage.getItem("template_version") || !sessionStorage.getItem("template_chats") || !sessionStorage.getItem("template_notifs")){
		request = { "requestTemplates": true };
		template_content = false;
	}else if (sessionStorage.getItem("template_version") !== pulseTemplateVersion){
		request = { "requestTemplates": true };
		template_content = false;
	}
	else request = {};
	ajaxNvars.request('pulses/getCacheOBJ.json', request, function(data){
		//save a lot of errors:
		if (typeof data.chats == "undefined") data.chats = { count:0 };
		if (typeof data.notifs == "undefined") data.notifs = { count: 0 };

		if (!template_content){
			sessionStorage.setItem('template_version', pulseTemplateVersion);
			sessionStorage.setItem('template_chats', data.templates.chats);
			sessionStorage.setItem('template_notifs', data.templates.notifs);
			delete data.templates;
		}

		pulse.createTemplates();
		pulse.cacheOBJ = data;
		if (pulse.onRender) pulse.onRender(data);
		else pulse.render();
		var PS = pulseFrequency;
		if (document.body.getAttribute('status') !== 'visible') PS = pulseFrequency*4;
		if (pulseIsActive) pulse.timeout = window.setTimeout(pulse.requestChanges, PS);
	});
}
pulse.requestChanges = function(){
	ajaxNvars.request('pulses/getChanges.json', {}, function(data){
		//save a lot of errors:
		if (typeof data.chats == "undefined"){
			data.chats = { count:0 };
			document.title = windowTitle;
		} 
		if (typeof data.notifs == "undefined"){
			data.notifs = { count: 0 };
			document.title = windowTitle;
		} 
		// if count is = 0, deny cache for labels
		if (data.chats.count == 0){
            for (var key in pulse.cacheOBJ.chats.label) {
              if (pulse.cacheOBJ.chats.label.hasOwnProperty(key)) {
                delete pulse.cacheOBJ.chats.label[key].unread_count;
              }
            }
        }
        pulse.cacheOBJ.notifs.count = data.notifs.count;
        if (data.notifs.list){
            for (var i = 0; i < data.notifs.list.length; i++) {
                pulse.cacheOBJ.notifs.list.unshift(data.notifs.list[i]);
            }
            _('#notification_sound').play();
            document.title = '('+data.notifs.count+') '+ windowTitle;
        }
        pulse.cacheOBJ.chats.count = data.chats.count;
        if (data.chats.list)
            pulse.cacheOBJ.chats.list.push(data.chats.list);
        if (data.chats.label){
            jQuery.extend(data.chats.label, pulse.cacheOBJ.chats.label);
            pulse.cacheOBJ.chats.label = data.chats.label;
            _('#notification_sound').play();
            document.title = '('+data.chats.count+') '+ windowTitle;
        }
        if (pulse.onRender) pulse.onRender(data);
        else pulse.render();

        var PS = pulseFrequency;
        if (document.body.getAttribute('status') !== 'visible') PS = pulseFrequency*4;
        if (pulse.timeout) clearTimeout(pulse.timeout);
        if (pulseIsActive) pulse.timeout = window.setTimeout(pulse.requestChanges, PS);
    });
}
pulse.markAllNotifsAsRead = function(){
    ajaxNvars.request('pulses/readAllNotifs.json');
    pulse.cacheOBJ.notifs.count = 0;
    pulse.render();
}
$(window).focus(function(){
  if(pulseIsActive) pulse.requestChanges();
});


})()
var windowTitle = document.querySelector('title').innerHTML;
pulse.requestCacheOBJ();

