<li class="header">You have {{count}} unread messages</li>
<li>
	<ul class="menu">
		{{#each label}}
		<li>
			<a href="<?php echo $config['base_url'] ?>/messages#/{{ClC}}" open-chat="{{ClC}}">
				<div class="pull-left">
					<img src="<?php echo $config['base_url'] ?>/assets/svg/person.svg" class="img-circle" alt="User Image">
				</div>
				<h4>
					{{name}}{{#if unread_count}} <i class="label label-success">{{unread_count}}</i>{{/if}}
				</h4>
				<span class="text-muted nowrap"><b>{{last_msg_of}}:</b> {{last_msg_fragment}}</span>
				<small class="nowrap pull-right" style="margin-top:1px"><i class="fa fa-clock-o"></i> {{sent_at}} ago</small>
				<?php if (has_permission('chat_with_all')): ?>
				<p>{{#if is_online}}<i class="fa fa-circle text-success"></i> Online{{else}}<i class="fa fa-circle"></i> Offline{{/if}} </p>
				<?php endif; ?>
			</a>
		</li>
		{{/each}}
	</ul>
</li>
<li class="footer"><a href="<?php echo $config['base_url'] ?>/messages?source=navbar">See All Messages</a></li>
