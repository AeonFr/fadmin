<?php 
if (!has_permission('send_bulkmail'))
	throw new Exception("It seems like you don't have permission to access this data.");
require_module('email');

switch ($vars['endpoint']) {
	case 'fetch-logs.json':
		$options = array();
		if (isset($_POST['list'])){ 
			if ($_POST['list']=='false') $_POST['list']= false;
			$options['list'] = $_POST['list'];
		}
		if (isset($_POST['html'])){ 
			if ($_POST['html']=='false') $_POST['html']= false;
			$options['html'] = $_POST['html'];
		}
		reply($module['email']->fetchLogs($options));
		break;
	case 'emailTemplates':
		$_POST['key'] = "dGhpc0lzLVZlcnlSYW5kb20";
		include "misc/noDatabaseAPI.php";
		break;
	case 'previewTemplate':
		require_module('email');
		try{
			$template = interpret_template($_REQUEST['template'],
			array(
			"config" => $config,
			"data" => array(
				"to_name" => "Example User",
				"to_email" => "example@example.com",
				"template_vars" => array(
					"password" => "123456",
					"unreceivedNotifs" => [
						array(
							"link" => $config['full_url'],
							"body" => "Content of a notification"
						),
						array(
							"body" => "Notification #2 content.",
							"custom_html" => "<p><a href='#'>Link 1</a> <a href='#'>Link 2</a>.</p>"
						)
					],
					"from" => "admin",
					"body" => "Hello"
				)
			)));
			reply($template);
		}catch(Exception $e){
			die("There's an error in the template. ".var_export($e, true));
		}
		exit();
		break;
	case 'massiveSend.json':
		require_module('email');
		reply($module['email']->bulkMail($_POST['to'], $_POST['subject'], $_POST['template'], $_POST['transaction_group']));
		break;
	default:
		throw new Exception("Endpoint doesn't exist");
		break;
}