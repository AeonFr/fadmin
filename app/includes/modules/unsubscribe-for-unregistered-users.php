<?php
class unsubscribeForUnregisteredUsers{
	function __construct(){
		global $config;
		$this->dbLocation = $config['root']."/_noSQL/unsubscribes.json";
		$this->dbContent = json_decode(file_get_contents($this->dbLocation), true); 
	}
	private function update_db($newDbContent){
		$file = fopen($this->dbLocation, 'w');
		fwrite($file, json_encode($newDbContent));
		$this->dbContent = $newDbContent;
		fclose($file);
	}
	/**
	 * check if a user is subscribed
	 * @param  string  $md5 hash of a user's email
	 * @return boolean      true if is subscribed, false otherwise.
	 */
	function is_subscribed($md5){
		if (isset($this->dbContent[$md5]))
			return false;
		else if (!isset($this->dbContent[$md5])) return true;
	}
	function unsubscribe($md5){
		$newDbContent = $this->dbContent;
		$newDbContent[$md5] = 1;
		$this->update_db($newDbContent);
	}
	function resubscribe($md5){
		if ($this->is_subscribed($md5)) return true;
		$newDbContent = $this->dbContent;
		if (isset($newDbContent[$md5])) unset($newDbContent[$md5]);
		$this->update_db($newDbContent);
		return true;
	}
}

$module['unsubscribe4unregistered'] = new unsubscribeForUnregisteredUsers();