<?php 
//selected by query variable
$table = $vars['table'];
// get the table schema
require_module('dataview');
$table_schema = $module['dataview']->table_schema;

$template['header'] = $table_schema[$table]["nicename"];
/**
 * If the parameter "userVars" is set into this file, the version of the table that matches the one generated for that user, or group of users, that share that value in their "filter_content_by_variable" will be fetched. This is harder to explain than what it's actually it.
 */

$dataFolder = 'cache';
if (isset($_GET['preview']) && has_permission('upload_data')){
	$dataFolder  = 'temp';
?><div class="alert alert-warning"><strong>Preview version</strong></div><?php
}
 ?>

<div id="loading" class="box box-primary px1 py1">
	<p class="text-center my2">
		<i class="fa fa-coffee"></i> Loading data...
	</p>
	<p>This is taking more than what it should. Would you want to <a href="">refresh the page?</a> If the problem persists, please contact support.</p>
</div>

<div id="nodata" class="hidden box px1 py1 p-relative bottom-0 from-bottom">
	<div class="text-muted text-center my2 py2 table-responsive">
		<h1 class="ion-eye-disabled"></h1>
		<h4>
			There's no data to display here.
			<span class="nowrap">Come back other time!</span>
		</h4>
	</div>
</div>

<script template="<?php echo $table ?>" type="text/x-handlebars-template">
	<div class="box-header">
		<div data-toggle="tooltip" title="Date where this data was uploaded." class="pull-right label label-info">{{info.last_updated}}</div>
		<h3 class="box-title"><i class="<?php echo $table_schema[$table]['icon'] ?>"></i> <?php echo $table ?>
		</h3>
	</div>
	<div class="table-responsive box-body">
		<table class="table table-striped">
			<tr>
				{{#each table.titles}}
					<th data-title="{{.}}">{{.}}</th>
				{{/each}}
			</tr>
			{{#each table.data}}
				<tr>
					{{#each .}}
						<td data-title="{{@key}}">{{.}}</td>
					{{/each}}
				</tr>
			{{/each}}
		</table>
	</div>
</script>
<div id="<?php echo $table ?>" class="hidden box box-solid box-default"></div>

<?php footer_section_start(); ?>
<textarea id="data" class="hidden"><?php 

require_module('dataview');



if (!$table_schema[$table]["filter_content"] || $table_schema[$table]["filter_content"] == 'false'){
	$var = $module['dataview']->get_data_of($table, $dataFolder);
	$fileAddr = $module['dataview']->dbFolder."/".$dataFolder."/".$table."/_universal.json";
	if (file_exists($fileAddr)) $last_updated = filemtime($fileAddr);
}else{
	if (isset($_GET['userVar']) && $_GET['userVar']!=='' && has_permission('upload_data')){
		$userVar  = $_GET['userVar'];
		$var = $module['dataview']->get_data_of($table, $dataFolder, $userVar);
		$fileAddr = $module['dataview']->dbFolder.$dataFolder."/".$table."/".$userVar.".json";
		if (file_exists($fileAddr)) $last_updated = filemtime($fileAddr);
	}else{
		$userVar  = $_SESSION['user'][ $table_schema[$table]["filter_content_by_variable"] ];
		$var = $module['dataview']->get_data_of($table, $dataFolder, $userVar);
		$fileAddr = $module['dataview']->dbFolder.$dataFolder."/".$table."/".$userVar.".json";
		if (file_exists($fileAddr)) $last_updated = filemtime($fileAddr);
	}
}

if ($var != 'false'){
	$parse =  json_decode($var);
	echo json_encode(array(
	"table"=>$parse,
	"info"=> [ "last_updated" => isset($last_updated) ? date('d/m/Y', $last_updated) : '' ] 
	));
}

?></textarea>
<script>
	if (_('#data').value){
		var data = JSON.parse( _('#data').value );
	}else data = false;
	$('#loading').remove();
	if (data==false || data.table == null){
		_('#nodata').classList.remove('hidden');
	}else{
		ajaxNvars.renderTemplate('<?php echo $table ?>', data, '#<?php echo $table ?>');
		_('#<?php echo $table ?>').classList.remove('hidden');
		$('[data-toggle="tooltip"]').tooltip();
	}
</script>
<?php footer_section_end(); ?>