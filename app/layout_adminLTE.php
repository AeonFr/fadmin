<?php 
/**
 * Bootstrap layout, using AdminLTE
 * Configured via the $layout variable
 */


// echoes "active" is the current page is being visited
// Ej: is_active("/admin") will echo "active" under all pages that starts with "/app/admin"
function is_active($page){
	global $config;
	if (startsWith($_SERVER['REQUEST_URI'], $config["base_url"].$page)) echo "active";
	return;
}

// Detect the ?source GET parameter, and write the data in a file under the noSQL folder. This is just for testing and to collect data, it's not important for the site functionality. This is intended to provide feedback on how users reach the pages they reach.
if (isset($_GET['source'])){
	$fileAddr = "$config[root]/_noSQL/click_sources.json";
	$json = json_decode(file_get_contents($fileAddr), true);
	$host = $_SERVER['HTTP_HOST'];
	$request = str_replace($_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);
	if (!isset($json[$host])) $json[$host] = array();
	if (!isset($json[$host][$request])) $json[$host][$request] = array();
	if (!isset( $json[$host][$request][$_GET['source']] )) $json[$host][$request][$_GET['source']] = 1;
	else $json[$host][$request][$_GET['source']] += 1;

	$file = fopen($fileAddr, 'w');
	fwrite($file, json_encode($json, JSON_PRETTY_PRINT));
	fclose($file);
}

include "config.php";

?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo (isset($template['header'])) ? $template['header'] ." · ". $config['page_title'] : $config['page_title'] ?></title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="<?php echo $config['base_url'] ?>/AdminLTE/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="<?php echo $config['base_url'] ?>/AdminLTE/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="<?php echo $config['base_url'] ?>/AdminLTE/dist/css/skins/<?php echo $config['AdminLTE_skin'] ?>.min.css">
		<link rel="stylesheet" href="<?php echo $config['base_url'] ?>/assets/css/generals.css">
		<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition <?php echo $config['AdminLTE_skin']; if ($config['AdminLTE_fixed_navbar']) echo ' fixed'; if (isset($template['sidebar-collapse'])) echo ' sidebar-collapse'; ?>">
		<div class="wrapper">

			<!-- Main Header -->
			<header class="main-header">

				<!-- Logo -->
				<a href="<?php echo $config['base_url'] ?>/" class="logo">
					<span class="logo-mini"><?php echo $config['navbar_short_title'] ?></span>
					<span class="logo-lg"><?php echo $config['navbar_title'] ?></span>
				</a>

				<!-- Header Navbar -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<!-- Navbar Right Menu -->
					<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
							<!-- Messages -->
							<li class="dropdown messages-menu">
								<!-- Menu toggle button -->
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-envelope-o"></i>
									<span class="label label-success" id="chatUnreadCount" showUnreadChats>..</span>
								</a>
								<ul class="dropdown-menu" id="chatRenderTemplate">
									<div class="loading" style="height: 200px"></div>
								</ul>
							</li><!-- /.messages-menu -->
							<!-- Notifications Menu -->
							<li class="dropdown notifications-menu" template="pulse-navbar-notifications">
								<!-- Menu toggle button -->
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-bell-o"></i>
									<span class="label label-warning" id="notifsUnreadCount" showUnreadNotifs>..</span>
								</a>
								<ul class="dropdown-menu" id="notifsRenderTemplate">
									<div class="loading" style="height: 200px"></div>
								</ul>
							</li>
							<!-- User Account Menu -->
							<li class="dropdown user user-menu">
								<!-- Menu Toggle Button -->
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo $config['base_url'] ?>/assets/svg/person.svg" class="user-image" alt="User Image">
									<span class="hidden-xs"><?php echo $_SESSION['user']['name'] ?></span>
								</a>
								<ul class="dropdown-menu">
									<!-- The user image in the menu -->
									<li class="user-header">
										<img src="<?php echo $config['base_url'] ?>/assets/svg/person.svg" class="img-circle" alt="User Image">
										<p>
											<?php echo $_SESSION['user']['name'] ?>
											<!-- <small></small> -->
										</p>
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="<?php echo $config['base_url'] ?>/my-account?source=navbar" class="btn btn-default btn-flat"><i class="ion-android-person" style="color:#00a65a"></i> My Account</a>
										</div>
										<div class="pull-right">
											<a href="<?php echo $config['base_url'] ?>/sign-out" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
										</div>
									</li>
								</ul>
							</li>
							<!-- Control Sidebar Toggle Button -->
							<!-- <li>
								<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
							</li> -->
						</ul>
					</div>
				</nav>
			</header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">

				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">

					<!-- Sidebar user panel (optional) -->
					<div class="user-panel">
						<a href="<?php echo $config['base_url'] ?>/my-account?source=sidebar-profilepic" class="pull-left image">
							<img src="<?php echo $config['base_url'] ?>/assets/svg/person.svg" class="img-circle" alt="User Image">
						</a>
						<div class="pull-left info">
							<p><?php echo $_SESSION['user']['name'] ?></p>
						</div>
					</div>

					<!-- Sidebar Menu -->
					<ul class="sidebar-menu">
						<li class="<?php is_active("/#") ?>">
							<a href="<?php echo $config['base_url'] ?>/"><i class="fa ion-home"></i> Home</a>
						</li>
						<li class="<?php is_active("/my-account") ?>">
							<a href="<?php echo $config['base_url'] ?>/my-account?source=sidebar"><i class="fa ion-android-person"></i> My Account</a>
						</li>
						<li class="<?php is_active("/messages") ?>">
							<a href="<?php echo $config['base_url'] ?>/messages?source=sidebar"><i class="fa fa-envelope-o"></i> Messages <span class="label pull-right label-default" showUnreadChats></span></a>
						</li>
						<li class="<?php is_active("/notifications") ?>">
							<a href="<?php echo $config['base_url'] ?>/notifications?source=sidebar"><i class="fa fa-bell-o"></i> Notifications <span class="label pull-right label-default" showUnreadNotifs></span></a>
						</li>
						<?php require_once("$config[root]/table_schema.php");
							foreach($table_schema as $url=>$table){
								if (has_permission($table['permission_required'])): ?>
						<li class="<?php is_active("/dataview/".$url) ?>">
							<a href="<?php echo $config['base_url'] ?>/dataview/<?php echo $url ?>"><i class="<?php echo $table['icon'] ?>"></i> <?php echo $table['nicename'] ?></a>
						</li>
								<?php
								endif;
							}
						 ?>
						<?php if (has_permission("view_admin_menu")): ?>
						<li class="treeview <?php is_active("/admin") ?>">
							<a href="<?php echo $config['base_url'] ?>/admin"><i class="fa fa-th-large"></i> <span>Admin</span> <i class="fa fa-angle-left pull-right"></i></a>
							<ul class="treeview-menu">
								<?php if (has_permission("manage_other_users")): ?>
								<li class="<?php echo is_active("/admin/users") ?>"><a href="<?php echo $config['base_url'] ?>/admin/users"><i class="fa fa-users"></i> Manage users</a></li>
								<?php endif; ?>
								<?php if (has_permission("manage_user_permissions")): ?>
								<li class="<?php echo is_active("/admin/permissions") ?>"><a href="<?php echo $config['base_url'] ?>/admin/permissions"><i class="fa fa-lock"></i> Manage user roles</a></li>
								<?php endif; ?>
								<?php if (has_permission("send_notifications")): ?>
								<li class="<?php echo is_active("/admin/notifications") ?>"><a href="<?php echo $config['base_url'] ?>/admin/notifications"><i class="fa fa-bell-o"></i> Notifications</a></li>
								<?php endif; ?>
								<?php if (has_permission("send_bulkmail")): ?>
								<li class="<?php echo is_active("/admin/bulkmail") ?>"><a href="<?php echo $config['base_url'] ?>/admin/bulkmail"><i class="fa fa-envelope-square"></i> Mail
									<i class="fa fa-angle-left pull-right"></i>
								</a>
									<ul class="treeview-menu">
										<li class="<?php echo is_active("/admin/bulkmail?email-logs") ?>"><a href="<?php echo $config['base_url'] ?>/admin/bulkmail?email-logs"><i class="fa fa-file-text-o"></i> Email Logs</a></li>
										<li class="<?php echo is_active("/admin/bulkmail/edit-templates") ?>"><a href="<?php echo $config['base_url'] ?>/admin/bulkmail/edit-templates"><i class="fa fa-file-code-o"></i> Edit Templates</a></li>
										<li class="<?php echo is_active("/admin/bulkmail/send") ?>"><a href="<?php echo $config['base_url'] ?>/admin/bulkmail/send"><i class="fa fa-envelope-o"></i> Send from Template</a></li>
										<li><a href="<?php echo $config['base_url'] ?>/vendor/phpList/admin"><i class="fa fa-asterisk"></i> phpList</a></li>
									</ul>
								</li>
								<?php endif; ?>
								<?php if (has_permission("upload_data")): ?>
								<li class="<?php echo is_active("/admin/dataview-upload") ?>"><a href="<?php echo $config['base_url'] ?>/admin/dataview-upload"><i class="fa fa-table"></i> Update tables</a></li>
								<?php endif; ?>
								<?php if (has_permission("view_documentation")): ?>
								<li class="<?php echo is_active("/admin/documentation") ?>"><a href="<?php echo $config['base_url'] ?>/admin/documentation"><i class="fa fa-book"></i> Documentation</a></li>
								<?php endif; ?>
							</ul>
						</li>
						<?php endif; ?>
					</ul>
				</section>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>
						<?php echo (isset($template['header'])) ? $template['header'] : $config['page_title'] ?>
						<?php if (isset($template['description'])): ?>
							<small><?php echo $template['description'] ?></small>
						<?php endif; ?>
					</h1>
					<?php if (isset($template['breadcrumb'])): ?>
						<ol class="breadcrumb">
							<li><a href="<?php echo $config['base_url'] ?>"><i class="fa fa-dashboard"></i> Home</a></li>
							<?php foreach($template['breadcrumb'] as $key=>$value): ?>
								<li>
									<a href="<?php echo $config['base_url'] . $value ?>"><?php echo $key ?></a>
								</li>
							<?php endforeach; ?>
							<li class="active">Here</li>
						</ol>
					<?php endif; ?>
				</section>
				<section class="content">
					<?php if (isset($layout['content'])) echo $layout['content']; ?>
				</section>
			</div>
		</div><!-- ./wrapper -->
		<audio id="notification_sound" src="<?php echo $config['base_url'] ?>/assets/mp3/notification.mp3" preload="auto"></audio>
		<!-- jQuery 2.1.4 -->
		<script src="<?php echo $config['base_url'] ?>/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="<?php echo $config['base_url'] ?>/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
		<!-- Handlebars -->
		<script src="<?php echo $config['base_url'] ?>/assets/js/handlebars-v4.0.5.js"></script>
		<!-- Custom script -->
		<script>var base_url = '<?php echo $config['base_url'] ?>',pulseIsActive=true, pulseFrequency = <?php echo $config['pulse_frequency'] ?>, pulseTemplateVersion='<?php echo $config["pTemplVersion"] ?>';</script>
		<script src="<?php echo $config['base_url'] ?>/assets/js/scripts.min.js"></script>
		<?php if($config['AdminLTE_fixed_navbar']): ?>
			<!-- Slimscroll - Required for fixed navbar -->
			<script src="<?php echo $config['base_url'] ?>/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
			<script>
				var AdminLTEOptions = {
						navbarMenuSlimscroll: true,
						sidebarExpandOnHover: false
				}
			</script>
			<?php if (!endsWith("-dark", $config['AdminLTE_skin'])): ?>
				<style>.slimScrollBar{background: white!important;}</style>
			<?php endif; ?>
		<?php endif; ?>
		<!-- AdminLTE App -->
		<script src="<?php echo $config['base_url'] ?>/AdminLTE/dist/js/app.min.js"></script>
		<?php if (isset($template['footer'])) echo $template['footer']; ?>
	</body>
</html>
