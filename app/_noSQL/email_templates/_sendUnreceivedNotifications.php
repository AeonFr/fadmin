<?php include "__header.php"; ?>
<p><?php echo $vars['data']['to_name'] ?>,
<?php if (count($vars['data']['template_vars']['unreceivedNotifs']) > 1): ?>
you have new notifications for 
<?php else: ?>ç
you have a new notification for
<?php endif; ?>
<a href="<?php echo $vars['config']["full_url"] ?>"><?php echo $vars['config']["page_title"] ?>.</a></p>
<div class="list-style:none;padding:20px">
<?php foreach($vars['data']['template_vars']['unreceivedNotifs'] as $notif_data): ?>
<div style="padding:12px;background-color:#eeeeee;margin-bottom:12px">
	<a <?php if(isset($notif_data['link'])) echo 'href="'.$notif_data['link'].'"'; ?> style="display:block;color:#000000!important;text-decoration:none!important;">
		<?php echo nl2br($notif_data['body']); ?>
	</a>
	<?php if (isset($notif_data['custom_html'])) echo $notif_data['custom_html']; ?>
</div>
<?php endforeach; ?>
</div>
<p>You can view all your notifications by entering <a href="<?php echo $vars['config']["full_url"] ?>/notifications">your notification's dashboard</a>.</p>
<p>You can also unsubscribe from notification alerts simply by following <a href="<?php echo $vars['config']["full_url"] ?>/API/email-settings/?rel=<?php echo md5($vars['data']['to_email']) ?>&disable_notifs_by_mail=true">this link</a>.</p>

<?php include "__footer.php"; ?>
<?php $subject = "New Notifications"; ?>