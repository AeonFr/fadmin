<?php
/**
 * FAdmin * 2016
 * by Francisco Cano
 * frankno.94@gmail.com
 * https://franciscocano.tk
 */
require_once 'config.php';
/**
 * Require some handy functions...
 */
require_once "$config[root]/includes/general-functions.php";
/**
 * Before rendering ANYTHING, let's check whether we're loged in or not...
 */
require_once "$config[root]/includes/auth.php";

/**
 * If we are logged in,
 * Interpret the URL and render a page
 */
require_once "$config[root]/routing.php";