<?php
if (!has_permission("manage_user_permissions"))
	throw new Exception("You don't have permission to access this page");
require_module('permissions');
switch ($vars['endpoint']) {
	case 'list.json':
		reply( $module['permissions']->listAll() );
		break;
	case 'update-permission-list.json':
		check_required(["role", "permission_list"]);
		reply ($module['permissions']->setList($_POST['role'], $_POST['permission_list']));
		break;
	case 'rename-user-role.txt':
		check_required(['role', 'new_name']);
		reply ($module['permissions']->rename($_POST['role'], $_POST['new_name']));
		break;
	case 'add-user-role.txt':
		check_required(["role", "permission_list"]);
		reply ($module['permissions']->add($_POST["role"], $_POST["permission_list"]));
		break;
	case 'remove-user-role.txt':
		check_required(["role_id"]);
		if ($module['permissions']->remove($_POST['role_id']))
			reply ("Role by id $_POST[role_id] was deleted.");
		break;
	default:
		throw new Exception("Endpoint doesn't exist");
		break;
}