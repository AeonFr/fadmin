<?php 
$template['header'] = "Notifications";
$template['breadcrumb'] = ["Admin" => "/admin", "Notifications" => "/admin/notifications"];
?>
<div class="alert alert-warning">
	You won't receive chat messages or notifications on this page.
</div>
<form id="newNotif" endpoint="admin-notifications/new.json">
	<div class="jumbotron">
		<h1><i class="fa fa-bell"></i> Create new notification.</h1>
		<p>Start by choosing a body and an icon for the notification.</p>
		<div class="row">
			<div class="col-sm-10 col-md-8 col-lg-6">
				<div class="form-group">
					<p>
						<!-- ICONS -->
						<label class="btn btn-xs btn-default fa fa-bell"><input type="radio" name="meta[icon]" value="fa fa-bell"></label>
						<label class="btn btn-xs btn-default fa fa-asterisk"><input type="radio" name="meta[icon]" value="fa fa-asterisk"></label>
						<label class="btn btn-xs btn-default fa fa-book"><input type="radio" name="meta[icon]" value="fa fa-book"></label>
						<label class="btn btn-xs btn-default fa fa-check-circle-o"><input type="radio" name="meta[icon]" value="fa fa-check-circle-o"></label>
						<label class="btn btn-xs btn-default fa fa-calendar"><input type="radio" name="meta[icon]" value="fa fa-calendar"></label>
						<label class="btn btn-xs btn-default fa fa-circle"><input type="radio" name="meta[icon]" value="fa fa-circle"></label>
						<label class="btn btn-xs btn-default fa fa-clock-o"><input type="radio" name="meta[icon]" value="fa fa-clock-o"></label>
						<label class="btn btn-xs btn-default fa fa-comment"><input type="radio" name="meta[icon]" value="fa fa-comment"></label>
						<label class="btn btn-xs btn-default fa fa-diamond"><input type="radio" name="meta[icon]" value="fa fa-diamond"></label>
						<label class="btn btn-xs btn-default fa fa-rss"><input type="radio" name="meta[icon]" value="fa fa-rss"></label>
						<label class="btn btn-xs btn-default fa fa-gear"><input type="radio" name="meta[icon]" value="fa fa-gear"></label>
						<label class="btn btn-xs btn-default fa fa-info"><input type="radio" name="meta[icon]" value="fa fa-info"></label>
						<label class="btn btn-xs btn-default fa fa-line-chart"><input type="radio" name="meta[icon]" value="fa fa-line-chart"></label>
						<label class="btn btn-xs btn-default fa fa-paper-plane-o"><input type="radio" name="meta[icon]" value="fa fa-paper-plane-o"></label>
						<label class="btn btn-xs btn-default fa fa-print"><input type="radio" name="meta[icon]" value="fa fa-print"></label>
						<label class="btn btn-xs btn-default fa fa-share-alt"><input type="radio" name="meta[icon]" value="fa fa-share-alt"></label>
						<label class="btn btn-xs btn-default fa fa-smile-o"><input type="radio" name="meta[icon]" value="fa fa-smile-o"></label>
						<label class="btn btn-xs btn-default fa fa-sticky-note-o"><input type="radio" name="meta[icon]" value="fa fa-sticky-note-o"></label>
						<label class="btn btn-xs btn-default fa fa-sitemap"><input type="radio" name="meta[icon]" value="fa fa-sitemap"></label>
						<label class="btn btn-xs btn-default fa fa-star"><input type="radio" name="meta[icon]" value="fa fa-star"></label>
						<label class="btn btn-xs btn-default fa fa-tachometer"><input type="radio" name="meta[icon]" value="fa fa-tachometer"></label>
						<label class="btn btn-xs btn-default fa fa-search-plus"><input type="radio" name="meta[icon]" value="fa fa-search-plus"></label>
						<label class="btn btn-xs btn-default fa fa-thumb-tack"><input type="radio" name="meta[icon]" value="fa fa-thumb-tack"></label>
						<label class="btn btn-xs btn-default fa fa-lock"><input type="radio" name="meta[icon]" value="fa fa-lock"></label>
						<label class="btn btn-xs btn-default fa fa-unlock"><input type="radio" name="meta[icon]" value="fa fa-unlock"></label>
						<label class="btn btn-xs btn-default ion-android-person"><input type="radio" name="meta[icon]" value="ion-android-person"></label>
						<label class="btn btn-xs btn-default ion-android-people"><input type="radio" name="meta[icon]" value="ion-android-people"></label>
						<label class="btn btn-xs btn-default ion-android-person-add"><input type="radio" name="meta[icon]" value="ion-android-person-add"></label>
						<label class="btn btn-xs btn-default ion-android-alert"><input type="radio" name="meta[icon]" value="ion-android-alert"></label>
					</p>
					<textarea required name="body" class="form-control" placeholder="This is the content for the notification"></textarea>
				</div>
				<p>You want the user to be able to click on the notification to go somewhere?</p>
				<label class="block">
					<input type="checkbox" name="has_link" value="1"> Make this notification redirect user to another URL
				</label>
				<div class="form-group">
					URL: <input disabled type="text" name="meta[link]" class="form-control" value="<?php echo $config['full_url'] ?>/">
				</div>
				<p>Finally, you can inject any HTML code at the end of your notification. (Be cautious not to break anything)</p>
				<label class="block">
					<input type="checkbox" name="has_custom_html" value="1"> Use custom HTML in this notification
				</label>
				<textarea disabled name="meta[custom_html]" class="form-control bg-black-75 white-75" placeholder="Custom <html> code"></textarea>
			</div>


			<!-- Section 2 -->
			
			<div class="col-sm-10 col-md-8 col-lg-6 text-center">
				<p>Click the button above to load a preview:</p>
				<p id="PREVIEW" class="btn btn-info btn-flat">PREVIEW YOUR NOTIFICATION</p>
				<p class="alert alert-info">The preview will load where notifications are regularly displayed. This is completely offline, <em>real</em> notifications are disabled in this page.</p>
				<p>Once you're all set, choose a group of users. Enter their Client Codes separated by <code>;</code>. I recommend you to store this codes somewhere else in case you need to send the notification again if something goes wrong.</p>
				<textarea required name="to_users" class="form-control" placeholder="Enter Client Codes separated by ;"></textarea>
				<p>Ready? Double-check everything. When done, click the following button:</p>
				<button type="submit" class="btn btn-warning btn-flat">
					<p class="btn btn-info btn-flat">SEND NOTIFICATION</p>
					<div><i class="fa fa-warning"></i> This cannot be undone.</div>
				</button>
				
			</div>
		</div>
	</div>
</form>

<?php footer_section_start(); ?>
<script>
// window.onbeforeunload = function(e) {
//     return 'Dialog text here.';
// };
pulseIsActive = false;
$('#PREVIEW').on('click', function(){
	var pulseData = {
		chats: { count: -1, list: []},
		notifs: {
			count: 1,
			list: [
				{
					icon: ($('[name="meta[icon]"]:checked').val() || "") ,
					sent_at: "1 second",
					body: $('[name="body"]').val()
				}
			]
		}
	};
	if ($('[name="has_link"]').is(':checked')){
		pulseData.notifs.list[0].link = $('[name="meta[link]"]').val();
	}
	if ($('[name="has_custom_html"]').is(':checked')){
		pulseData.notifs.list[0].custom_html = $('[name="meta[custom_html]"]').val();
	}
	console.log(pulseData);
	pulse.cacheOBJ.chats = {};
	pulse.cacheOBJ = pulseData;
	pulse.render();
});

$('[name="has_link"], [name="has_custom_html"]').on('change', function(){
	var selector = '[name="meta['+($(this).attr('name')).substring(4)+']"]';
	if ($(this).is(':checked')){
		$(selector).prop('disabled', false);
	}else{
		$(selector).prop('disabled', true);
	}
})

virtualForm.create('#newNotif', function(response){
	show_message('Pulse created!');
	_('#newNotif').reset(); // reset all form fields
});
</script>
<?php footer_section_end(); ?>