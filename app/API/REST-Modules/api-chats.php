<?php 
// if (!has_permission("manage_self"))
// 	throw new Exception("It seems like you don't have permission to do this. Please contact the administrator.");

require_module(['chat', 'pulses']);
switch ($vars['endpoint']) {
	case 'list-contacts.json':
		reply($module['chat']->listContacts());
		break;
	case 'chat-window.json':
		$userData = $module['users']->getUserData($_POST['ClC']);
		$module['chat']->markConversationAsRead($_POST['ClC']);
		/** The data that is "public" */
		$data = [
			"display_name" => $userData['name'],
			"ClC" => $userData['client_code']
		];
		if (isset($_POST['limit'])) $limit = $_POST['limit'];
		else $limit = 10;
		$last = $module['chat']->lastChatsWith($_POST['ClC'], $limit);
		reply(array(
			"chats" => $last,
			"data" => $data
		));
		break;
	case 'newMessage.json':
		$module['chat']->markConversationAsRead($_POST['ClC']);
		reply($module['chat']->newMessage($_POST['ClC'], $_POST['body']));
		break;
	default:
		throw new Exception("Endpoint doesn't exist");
		break;
}
