<?php 
$template['header'] = "Notifications";
$template['breadcrumb'] = ["Admin" => "/admin"];
if (!has_permission('send_notifications')) die("You don't have permission to view this page!");
?>
<script template="unreceived-notifications" type="text/x-handlebars-template">
	{{#unless .}}
		<h4 class="text-muted mx1 my1 px1 py1">THERE ARE NO UNRECEIVED NOTIFICATIONS <i class="fa fa-smile-o"></i></h4>
	{{else}}
	<table class="table">
		<tr>
			<th>Sent at</th>
			<th>Body</th>
			<th>Custom HTML?</th>
			<th>Link?</th>
			<th>For user <i data-toggle="tooltip" title="If this column is 'false', the user doesn't exist, or was deleted." class="fa fa-info-circle"></i></th>
			<th>User Code</th>
			<th>(...)</th>
		</tr>
		{{#each .}}
		<tr>
			<td>{{sent_at}}</td>
			<td>{{body}}</td>
			<td class="nowrap" style="overflow: hidden;text-overflow: ellipsis;max-width: 100px;">{{meta.custom_html}}</td>
			<td class="nowrap" style="overflow: hidden;text-overflow: ellipsis;max-width: 100px;">{{meta.link}}</td>
			<td>{{for}}</td>
			<td>{{receiver}}</td>
			<td><div data-delete="{{pulse_id}}" class="fa fa-trash"></div></td>
		</tr>
		{{/each}}
	</table>
	{{/unless}}
</script>
<div class="jumbotron">
	<p><a href="notifications/new"><i class="fa fa-plus"></i> New notification</a></p>
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="btn btn-flat" onclick="initial_state()" data-toggle="tooltip" title="Click to refresh">Unreceived notifications</div>
			<div class="btn btn-flat btn-primary" id="sendUnreceivedAsMail"data-toggle="tooltip" title="Deliver all the unreceived notifications via email (only to subscribed users) (this might take a while)"><i class="fa fa-envelope"></i> DELIVER VIA EMAIL</div>
		</div>
		<div class="box-body">
			<div id="target-unreceived-notifications">
				<div class="px2 py2 mx2 my2 text-center muted"><i class="fa fa-sandclock"></i> Loading data...</div>
			</div>
		</div>
	</div>
</div>
<?php footer_section_start(); ?>
<script>
	function initial_state(){
		$('#target-unreceived-notifications').addClass('o-50');
		ajaxNvars.requestNrender({
			'endpoint': 'admin-notifications/list-unreceived.json',
			'template': 'unreceived-notifications',
			'selector': '#target-unreceived-notifications'
		}, function(data){
			console.log(data);
			$('#target-unreceived-notifications').removeClass('o-50');
			$('[data-toggle="tooltip"]').tooltip();
			$('[data-delete]').click(function(){
			 	var pulse_id = $(this).attr('data-delete');
			 	confirm(function(){
			 		ajaxNvars.request({'endpoint':'admin-notifications/delete.json', 'data': {pulse_id:pulse_id} }, function(){
			 			show_message('Notification Deleted');
			 			initial_state();
			 		});
			 	});
			})
		});
		$('#sendUnreceivedAsMail').click(function(){
			confirm(function(){
				ajaxNvars.request('admin-notifications/sendUnreceivedAsMail.json', {}, function(data){
					console.log(data);
					show_message(data.length + ' notifications sent.', {class:'alert-success'})
				})
			});
		});
	}
	initial_state();

	function confirm(callback){
		show_message('Are you sure? <div confirm class="btn btn-warning">YES</div>');
		$('[confirm]').click(callback);
	}
</script>
<?php footer_section_end(); ?>