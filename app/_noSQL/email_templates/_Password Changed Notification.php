<?php include "__header.php"; ?>
<p>Hi <?php echo $vars['data']['to_name'] ?>,
<br> This is an automatic email to notify that your password was recently changed for your account at the site <b><?php echo $vars['config']['full_url'] ?></b>.
<br> If you haven't done this, please contact <?php echo $vars['config']['admin_email'] ?>.
</p>

<?php include "__footer.php"; ?>
<?php $subject = "Password Changed"; ?>