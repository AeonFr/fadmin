<?php 

/**
 * Edit the "user_roles" table,
 * set different permissions to users of different groups of "permissions"
 */

class Permissions {
	function __construct(){}
	public function add($role, $permission_list){
		global $db;
		$perm_exists = $db->num_rows("SELECT * FROM user_roles WHERE role='?'", array($role));
		if ($perm_exists) throw new Exception('A permission with that name already exists');
		else{
			$db->query_escaped("INSERT INTO user_roles (role, permission_list) VALUES (?, ?)",
			array($role, $permission_list));
			return "$role role added successfully.";
		}
	}
	public function rename($role_old_name, $role_new_name){
		global $db;
		$perm_exists = $db->num_rows("SELECT * FROM user_roles WHERE role='?'",	array($role_new_name));
		if ($perm_exists > 1) return ('A permission with that name already exists');
		else {
			$db->query_escaped("UPDATE user_roles SET role=? WHERE role=?",
			array($role_new_name, $role_old_name));
			$db->query_escaped("UPDATE users SET role=? WHERE role=?",
			array($role_new_name, $role_old_name));
			// if the current user has this permission name, update the session variable.
			// In fact, just update ALL of that variable.
			require_module('users');
			global $module;	$_SESSION['user'] = $module['users']->getUserData($_SESSION['user']['client_code']);
			return "Role renamed successfully.";
		}
	}
	public function remove($role_id){
		global $db;
		//does the role exists?
		$query_role = $db->getAssoc("SELECT * FROM user_roles WHERE user_role_id=?",	array($role_id));
		if (count($query_role) !== 0){
			if ($query_role[0]['role'] === $_SESSION['user']['role']){
				throw new Exception("You can't delete your actual role.");
				return;
			}
			$db->query_escaped("DELETE FROM user_roles WHERE user_role_id=?", array($role_id));
			return true;
		}else{
			throw new Exception("The role with id $role_id doesn't exists.");
		}
	}
	public function setList($role, $imploded_list){
		global $db;
		if ($db->query_escaped("UPDATE user_roles SET permission_list=? WHERE role=? LIMIT 1", array($imploded_list, $role))){
			// update the session['user'] variable to match with the new permissions,
			require_module('users');
			global $module;	$_SESSION['user'] = $module['users']->getUserData($_SESSION['user']['client_code']);
			return true;
		}else{
			throw new Exception("Error updating $role");
		}
	}
	public function getList($role){
		global $db;
		$query_list = $db->getAssoc("SELECT permission_list FROM user_roles WHERE role=? LIMIT 1", array($role));
		if (isset($query_list[0]))
			return explode(";", str_replace(" ", "", preg_replace("#\R+#", "", $query_list[0]['permission_list'])));
		else
			throw new Exception("Role doesn't exist!");
	}
	public function listAll(){
		global $db;
		return $db->getAssoc("SELECT * FROM user_roles");
	}
	public function listWhere($permissions_array){
		global $db;
		$return = [];
		foreach($permissions_array as $permission){
			$return = array_merge($return, $db->getAssoc("SELECT * FROM user_roles WHERE permission_list LIKE '%$permission%'"));
		}
		return $return;
	}
}

$module['permissions'] = new Permissions();