<?php $template['header']="Mails";
if (!has_permission('send_bulkmail')) die("You don't seem to have permission to access this particular page."); ?>
<style>
.info-box.pointer{
	transition: box-shadow 1s;
}
.info-box.pointer:hover{
	box-shadow: 0px 1px 3px 2px rgba(0,0,0,.25);
}
.info-box.smaller .info-box-icon {
    width: 60px;
    height: 60px;
    font-size: 36px;
    line-height: 60px;
}

.info-box.smaller .info-box-content {
    margin-left: 60px;
}

.info-box.smaller {
    min-height: 60px;
}
.info-box-text{
	text-transform: none;
}
</style>
<script template="logs" type="text/x-handlebars-template">
	<div class="info-box-container p-relative">
		{{#each .}}
			<div class="info-box pointer" data-transaction-group="{{group_name}}">
				<span class="info-box-icon"><i class="fa fa-envelope"></i></span>

				<div class="info-box-content">
				  <span class="info-box-number">
				  <i class="text-muted">GROUP:</i> {{group_name}}
				  <span class="pull-right label label-default">CLICK FOR + DETAILS</span>
				  
				  </span>
				  <span class="info-box-text">{{count}} emails sent.</span>

				  <span class="progress-description">
					Last transaction: {{last_sends.count}} emails ({{#x " Math.round(this.last_sends.count / this.count *100) "}} {{/x}}% of total) on date {{last_sends.date}}.
				  </span>
				  <div class="progress">
					<div class="progress-bar bg-black" style="width: {{#x " Math.round(this.last_sends.count / this.count *100) "}} {{/x}}%"></div>
				  </div>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>
		{{/each}}
	</div>
</script>
<script template="single-log-item" type="text/x-handlebars-template">
	<div class="info-box-container p-relative">
		{{#with group}}
		<div class="my1 text-center">
			<div class="btn btn-flat btn-info" onclick="list_transaction_groups()">
				<i class="fa fa-chevron-left"></i>
				Back
			</div>
		</div>
		<div class="my2 text-center">
		  <span class="info-box-number">
		  <i class="text-muted">DETAILS OF GROUP:</i> {{group_name}}
		  
		  </span>
		  <span class="block info-box-text">Total: {{count}} email.</span>
		  <span class="block info-box-text"><i class="fa fa-info-circle" data-toggle="tooltip" title="If you here see something like '>1', it means the emails in this group were sent from two or more different templates."></i> Template: <code>{{template_used}}</code></span>
		</div>
		<div class="my2"></div>
		{{/with}}
		<section class="list-transactions">
			{{#each list}}
			<div class="single-transaction">
				<div class="info-box smaller">
					{{#xif "this.status == 'No error.'"}}
		            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>
					{{else}}
					<span class="info-box-icon bg-red"><i class="fa fa-close"></i></span>
					{{/xif}}
		            <div class="info-box-content">
		              <span class="info-box-text">
							<span class="label pull-right label-warning">{{date}}</span>
		              	&lt;{{to.name}}> {{to.email}}
		              </span>
		              <span class="info-box-number">{{transaction_name}} <a href="#preview-html" data-load-html-of="{{transaction_id}}" class="btn btn-xs btn-flat btn-default"><i >LOAD PREVIEW</i></a></span>
		            </div>
		            <!-- /.info-box-content -->
		        </div>
				<pre>subject: <code>{{subject}}</code>{{#if template}}
template: <code>{{template}}</code>{{/if}}
<b>status: <code>{{status}}</code></b></pre>
			</div>
			{{/each}}
		</section>
	</div>
</script>
<script template="preview-html" type="text/x-handlebars-template">
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title">Preview a sent email</h3>
		</div>
		<div class="box-body">
			<iframe allowfullscreen frameborder="0" class="block" style="min-height: 500px;width:100%" id="preview-iframe"></iframe>
		</div>
	</div>
</script>
<div class="row">
	<div class="col-xs-12 my2 btn-group">
		<a href="<?php echo $config['base_url'] ?>/admin/bulkmail/send" class="btn btn-flat btn-info">SEND EMAILS NOW</a>
		<a href="<?php echo $config['base_url'] ?>/admin/bulkmail/edit-templates" class="btn btn-flat btn-default">EDIT EMAIL TEMPLATES</a>
	</div>
	<div class="col-xs-12 col-lg-6">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Outbound <i class="fa fa-info-circle" data-toggle="tooltip" title="Here is the log of all the outbound email of the system, even the ones that are sent automatically."></i></h3>
			</div>
			<div class="box-body" id="logs"></div>
		</div>
	</div>
	<div class="col-xs-12 col-lg-6" id="preview-html"></div>
</div>

<?php footer_section_start(); ?>
<script>
	$('[data-toggle="tooltip"').tooltip();
	list_transaction_groups();
	function list_transaction_groups(){
		$('#preview-html').html('');
		ajaxNvars.requestNrender({
			"endpoint": "bulkmail/fetch-logs.json",
			"data": {list: false},
			"template": "logs",
			"selector": "#logs"
		}, function(data){
			$('[data-transaction-group]').click(function(){
				var transaction_group = $(this).attr('data-transaction-group');
				var transaction_group_data = data[transaction_group];
				$('#logs>.info-box-container').addClass('loading');
				single_transaction(transaction_group_data);
			})
		});
	}
	function single_transaction(transaction_group_data){
		ajaxNvars.request({
			'endpoint': 'bulkmail/fetch-logs.json',
			'data': {'list': transaction_group_data.group_name, 'html':false}
		}, function(data){
			ajaxNvars.renderTemplate('single-log-item', {"group": transaction_group_data, "list": data}, '#logs');
			$('[data-toggle="tooltip"').tooltip();
			$('[data-load-html-of]').click(function(){
				load_html_of($(this).attr('data-load-html-of'));
			})
		})
	}
	function load_html_of(transaction_id){
		ajaxNvars.requestNrender({
			'endpoint': 'bulkmail/fetch-logs.json',
			'data': {'html': transaction_id},
			'template': 'preview-html',
			'selector': '#preview-html'
		}, function(data){
			if (data!='')
				$('#preview-iframe').contents().find('html').html(data);
			else
				$('#preview-iframe').contents().find('html').html("Preview Not available (it may be missing because it contained sensitive user data).");
		})
	}
</script>
<?php footer_section_end(); ?>