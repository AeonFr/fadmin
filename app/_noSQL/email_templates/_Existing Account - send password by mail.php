<?php include "__header.php" ?>
<p>Hi <?php echo $vars['data']['to_name'] ?>,
<br> I'm sending this email to remind you of your credentials at
<a href="<?php echo $vars['config']["full_url"] ?>"><?php echo $vars['config']["page_title"] ?>.</a></p>
<p>Your username is: <b><?php echo $vars['data']['to_email'] ?></b>
</p>
<p>
Your password is: <b><?php echo $vars['data']['template_vars']['password'] ?></b>
</p>
<p>
This information is private and can be only viewed by you.
<br>
You can change your password anytime by logging in to the site: <?php echo $vars['config']["full_url"] ?>
</p>

<?php include "__footer.php"; ?>
<?php 
$subject = "Hi ". $vars['data']['to_name'] .", this is your new password at ". $vars['config']['page_title'];
 ?>