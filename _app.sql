-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-04-2016 a las 20:05:24
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `_fiverr_adminfa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_transactions`
--

CREATE TABLE `email_transactions` (
  `transaction_id` int(11) NOT NULL,
  `transaction_name` varchar(50) NOT NULL,
  `transaction_group` varchar(50) NOT NULL,
  `transaction_data` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `sent_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `email_transactions`
--

INSERT INTO `email_transactions` (`transaction_id`, `transaction_name`, `transaction_group`, `transaction_data`, `status`, `sent_at`) VALUES
(1, 'Email to user admin (id:sysAdm)', 'Your Password at Financial App', '{"template":"_Existing Account - send password by mail","to_email":"admin@admin.com","to_name":"admin","subject":"Hi admin, this is your new password"}', 'Email disabled via $config.', '2016-04-21 04:25:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pulses`
--

CREATE TABLE `pulses` (
  `pulse_id` int(100) NOT NULL,
  `sender` varchar(25) DEFAULT NULL,
  `receiver` varchar(25) NOT NULL,
  `body` text NOT NULL,
  `sent_at` timestamp NULL DEFAULT NULL,
  `was_received` tinyint(1) NOT NULL,
  `was_read` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pulse_meta`
--

CREATE TABLE `pulse_meta` (
  `pulse_meta_id` int(11) NOT NULL,
  `pulse_id` int(11) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `meta_value` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `client_code` varchar(25) CHARACTER SET latin1 NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `client_pan` varchar(25) CHARACTER SET latin1 NOT NULL,
  `role` varchar(100) CHARACTER SET latin1 NOT NULL,
  `phone_num` varchar(25) CHARACTER SET latin1 NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registered_at` timestamp NULL DEFAULT NULL,
  `md5_hash` varchar(32) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`client_code`, `name`, `email`, `client_pan`, `role`, `phone_num`, `updated_at`, `registered_at`, `md5_hash`) VALUES
('sysAdm', 'admin', 'admin@admin.com', '', 'ADMIN', '', '2016-04-23 18:02:55', NULL, 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_auth_data`
--

CREATE TABLE `user_auth_data` (
  `auth_data_id` int(11) NOT NULL,
  `client_code` varchar(25) NOT NULL,
  `auth_key` varchar(50) NOT NULL,
  `last_request` timestamp NULL DEFAULT NULL,
  `user_agent` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_meta`
--

CREATE TABLE `user_meta` (
  `meta_id` int(11) NOT NULL,
  `client_code` varchar(25) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_roles`
--

CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL,
  `permission_list` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `role`, `permission_list`) VALUES
(1, 'ADMIN', 'view_admin_menu;\nmanage_other_users;\nmanage_user_permissions;\nsend_notifications;\nview_documentation;\nverbose_exceptions;\nchat_with_all;\nreceive_chats_from_all;\nsend_bulkmail;\nupload_data;\nview_table_holdings;\nview_table_ledgers;\nview_table_openpositions'),
(2, 'basic (only chat with admin & receive notifs)', ''),
(3, 'access to holdings ONLY', 'view_table_holdings'),
(4, 'access to ledgers ONLY', 'view_table_ledgers'),
(5, 'access to openpositions ONLY', 'view_table_holdings'),
(6, 'access to: holdings + ledgers', 'view_table_holdings;\nview_table_ledgers'),
(7, 'access to: holdings + openpositions', 'view_table_holdings;\nview_table_openpositions'),
(8, 'access to: ledgers + openpositions', 'view_table_ledgers;\nview_table_openpositions;'),
(9, 'access to holdings, ledgers & openpositions', 'view_table_holdings;\nview_table_ledgers;\nview_table_openpositions;');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `email_transactions`
--
ALTER TABLE `email_transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indices de la tabla `pulses`
--
ALTER TABLE `pulses`
  ADD PRIMARY KEY (`pulse_id`);

--
-- Indices de la tabla `pulse_meta`
--
ALTER TABLE `pulse_meta`
  ADD PRIMARY KEY (`pulse_meta_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`client_code`);

--
-- Indices de la tabla `user_auth_data`
--
ALTER TABLE `user_auth_data`
  ADD PRIMARY KEY (`auth_data_id`);

--
-- Indices de la tabla `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indices de la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `email_transactions`
--
ALTER TABLE `email_transactions`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `pulses`
--
ALTER TABLE `pulses`
  MODIFY `pulse_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `pulse_meta`
--
ALTER TABLE `pulse_meta`
  MODIFY `pulse_meta_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_auth_data`
--
ALTER TABLE `user_auth_data`
  MODIFY `auth_data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
