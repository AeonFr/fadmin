<?php 
/**
 * Authentificate the user...
 * This file will make the script stop excecuting & show a sign-in message if:
 * 		- The user is not logged in
 * 		AND the user did not submit any data in the last request
 * Here, in this very same file, we also handle the connection with MySQL.
 * **Therefore, by including this file in any other, you are successfully connecting to a database.**
 */
/*
---
Regarding authentication, each user is provided with a unique random string when logged in. This string is stored as the name of a cookie; it's value is determined by the md5 hash of the user id followed by his email.

```
$_COOKIE[ $user['auth_key'] ] === md5($user['id'].$user['email'])) ? true : false;
```

The auth_keys are stored in a different table, related to user's table by client_code. This keys are deleted when the user logs out, else they're stored.

Each request with this method will update a value in the user_auth_data table, called "last_request", which is exactly what it's named after.

Considering each request to the API will also update this value, we not only use this value to check if an entry should be deleted or not (table maintained), we also store this to check if a user is logged in or not.

---
*/
if (!isset($config))require_once "./config.php";
// PDO connection
require_once "$config[root]/includes/classes/database.php";
$db = new database("mysql:host=$config[db_host];dbname=$config[db_table]", $config["db_user"], $config["db_password"]);
$db->query("SET time_zone = '$config[sql_time_zone]'");


require_once "$config[root]/includes/classes/auth.php";

$auth = new Auth();

if (isset($_POST['user'])){
	// the sign-in form has been submitted, interpret it
	try{
		$sign_in = $auth->sign_in();
		require_module('users');
		$_SESSION['user'] = $module['users']->getUserData($sign_in);
		//this "refresh" will make the browser not to send an annoying message asking whether it's cool or not to "re-send" the form data if the page is refreshed or the {back} button is pressed.
		header("Location: $config[base_url]");
		exit();
	}catch(Exception $e){
		$log_in_error = $sign_in;
	}

}
$auth->authentificateOrRequestSignInForm();
// if the code below this line is executed, we have signed in!
// The user data can be retrieved in the variable
// $_SESSION['user']
// Ej: $_SESSION['user']['name'] -> name of signed-in user.