<?php 

class DataView {
	function __construct(){
		global $config;
		$this->config = $config;
		global $table_schema;
		
		$this->dbFolder = "$config[root]/_noSQL/dataview/";
		require_once "$config[root]/vendor/parsecsv-for-php/parsecsv.lib.php";
		$this->parseCSV = new parseCSV();
		$this->parseCSV->delimiter = ";";
		$this->parseCSV->output_delimiter = ";";
		$this->parseCSV->enclosure = '"';

		//backward compatibility - back up mode
		//MIGRATION CODE - SHOULD BE ERASED IN NEXT VERSION.
		require_once "$config[root]/table_schema.php";
		$this->table_schema = $table_schema;

		if (!file_exists($this->dbFolder."table_schema.json")){
			//create the file
			$table_schema_json = fopen($this->dbFolder."table_schema.json", "w");
			//put contents
			fwrite($table_schema_json, json_encode($this->table_schema));
			fclose($table_schema_json);
		}

		$table_schema_json = file_get_contents($this->dbFolder."table_schema.json");
		$this->table_schema = json_decode($table_schema_json, true);

	}
	function parseFile($filePath, $table_headers_line=1, $auto=true, $sort_by=null, $conditions=null){
		$this->parseCSV->offset = ($table_headers_line>1)? $table_headers_line - 1 : null;
		if ($sort_by != null && $sort_by != '') $this->parseCSV->sort_by = $sort_by;
		else $this->parseCSV->sort_by = null;
		//$this->parseCSV->output_filename = $filePath;
		if ($conditions!=null){
			$this->parseCSV->conditions = $conditions;
		} else {
			$this->parseCSV->conditions = null;
		}
		// parse while auto-detecting delimiter characters.
		if ($auto) $this->parseCSV->auto($filePath);
		else $this->parseCSV->parse($filePath);
		return  array(
					"titles" => $this->parseCSV->titles,
					"data" => $this->parseCSV->data
				);
	}
	private function empty_folder($dir){
		$files = glob($dir."/*"); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
		    unlink($file); // delete file
		}
	}
	private function empty_temp_folder(){
		$this->empty_folder($this->dbFolder."temp");
	}
	function uploadTempMasterFile($table, $table_headers_line=1){
		$table_schema = $this->table_schema;
		if (!isset($this->table_schema[$table])){
			throw new Exception("The table name is invalid"); return false;
		}
		if (empty($_FILES) || !isset($_FILES['file'])){ throw new Exception("No 'file' detected."); return false; }
		$type = explode(".",$_FILES['file']['name']);
		if(strtolower(end($type)) !== 'csv'){
			throw new Exception("File must have .csv or .CSV extension."); return false;
		}
		$this->empty_temp_folder();
		$filePath = $this->dbFolder."temp/".$table.".csv"; // upload to temp folder
		//create file
		fopen($filePath, "w");
		move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
		return ["message" => "File uploaded."];
	}
	function setTableSchema($table_name, $table_data){
		$table_schema_mergeable = [];
		$table_schema_mergeable[$table_name] = $table_data;
		$this->table_schema = array_merge($this->table_schema, $table_schema_mergeable);
		$file = fopen($this->dbFolder."table_schema.json", "w");
		fwrite($file, json_encode($this->table_schema));
		fclose($file);
		return ["message" => "Table schema changed"];
	}
	function unparseToFile($parsedArr, $filePath){
		$this->parseCSV->delimiter = ";";
		$this->parseCSV->output_delimiter = ";";
		$this->parseCSV->enclosure = "";
		$content = $this->parseCSV->unparse($parsedArr['data'], $parsedArr['titles']);

		/*create directry of $filePath, if it doesn't exist.*/
		//1. get the directory:
		$fExpl = explode('/', $filePath);
		array_pop($fExpl); //delete the file name
		$fDir = join("/", $fExpl); //get the directory
		if (!is_dir($fDir)){
			mkdir($fDir);
		}
		$file = fopen($filePath, 'w');
		fwrite($file, $content);
		fclose($file);
	}
	function unparseToJSON($parsedArr, $filePath){
		/*create directry of $filePath, if it doesn't exist.*/
		//1. get the directory:
		$fExpl = explode('/', $filePath);
		array_pop($fExpl); //delete the file name
		$fDir = join("/", $fExpl); //get the directory
		if (!is_dir($fDir)){
			mkdir($fDir);
		}
		$file = fopen($filePath, 'w');
		fwrite($file, json_encode($parsedArr));
		fclose($file);
	}
	function exclude_columns($tableName, $parsedArr){
		$table_schema = $this->table_schema;
		foreach($table_schema[$tableName]['exclude_columns'] as $i=>$excluded_col){
			$table_schema[$tableName]['exclude_columns'][$i] = trim($excluded_col);
		}
		array_push($table_schema[$tableName]['exclude_columns'], [' ', '', null]);
		$newData = [ 'titles'=>[], 'data' => [] ];
		foreach($parsedArr['titles'] as $indexOf=>$title){
			if (!in_array(trim($title), $table_schema[$tableName]["exclude_columns"])){
				array_push($newData['titles'], trim($title));
			}
		}
		foreach($parsedArr['data'] as $i=>$dataRow){
			$empty_data = 0;
			$dataPush = [];
			foreach($dataRow as $indexOf=>$dataContent){
				if (trim($dataContent) == '') $empty_data++;

				if (in_array(trim($indexOf), $newData['titles'])){
					$dataPush[$indexOf] = $dataContent;
				}
			}
			if ($empty_data !== count($parsedArr['titles'])){
				//row is not empty
				array_push($newData['data'], $dataPush);
			}
		}
		return $newData;
	}
	/**
	 * Generate the filtered parsed array from a table in the "temp" folder, that will then be cached.
	 */
	function generateFilteredArr($tableName, $table_headers_line, $user){
		$table_schema = $this->table_schema;
		$filePath = $this->dbFolder."temp/".$tableName.".csv";
		// proxy variables
		$filterColumn = $table_schema[$tableName]["filter_content_column"];
		$filterByUserVar = $table_schema[$tableName]["filter_content_by_variable"];
		//conditions
		$conditions_general = $filterColumn . " contains ";
		$conditions = $conditions_general . $user[$filterByUserVar];
		$parsedArr = $this->parseFile($filePath, $table_headers_line, true, null, $conditions);
		$parsedArr = $this->exclude_columns($tableName, $parsedArr);
		return $parsedArr;
	}
	function applyTempMasterFile($tableName, $table_headers_line, $folder = 'temp'){
		/**
		 * Turn a masterfile into the "non-temporary" version, parsed properly.
		 * Up to this point,
		 * Still not accessible for front-end users: still missing "confirmTempMasterFile()"
		 */
		$table_schema = $this->table_schema;
		// Files are ALLWAYS uploaded to temp folder...
		$filePath = $this->dbFolder."temp/".$tableName.".csv";
		if ($table_schema[$tableName]["filter_content"] == 'true'){
			//set the directory
			$newFolderPath = $this->dbFolder.$folder."/".$tableName;
			if (!is_dir($newFolderPath)) mkdir($newFolderPath);
			//delete any already existing data.
			$this->empty_folder($newFolderPath);
			//recursively create files, only if there's any data at all to display.
			global $module;
			$allUsers = $module['users']->listAll();
			$generatedFiles = array();
			$errors = array();
			if (!isset($table_schema[$tableName])){
				throw new Exception('Unknown table $tableName');
			}
			if (!isset($table_schema[$tableName]['filter_content_by_variable']) || $table_schema[$tableName]['filter_content_by_variable']==''){
				throw new Exception('Atomized or missing data. Check the table configuration.');
			}
			foreach($allUsers as $user){
				$UserVar = $user[$table_schema[$tableName]["filter_content_by_variable"]];
				$parsedArr = $this->generateFilteredArr($tableName, $table_headers_line, $user);
				if (count($parsedArr['data'])>0){
					$newFilePath = $newFolderPath . "/" . $UserVar . ".json";
					$this->unparseToJSON($parsedArr, $newFilePath);
					array_push($generatedFiles, $UserVar);
				}else{
					array_push($errors, "Table for user <strong>$user[name] ($user[client_code])</strong> wasn't created because there wasn't any data to display for him/her.");
				}
			}
			return array("message"=>'done!', "generatedFiles" => $generatedFiles, "errors"=> $errors);
		}else{ // filter_content is not active. Serve the same file to all users
			$parsedArr = $this->parseFile($filePath, $table_headers_line);
			$parsedArr = $this->exclude_columns($tableName, $parsedArr);
			$newFilePath = $this->dbFolder.$folder."/".$tableName."/_universal.json";
			$this->unparseToJSON($parsedArr, $newFilePath);
			return array("message"=>'done!', "generatedFiles" => [""]);
		}
	}
	function confirmTempMasterFile($table_name, $table_headers_line, $sendNotifications=true){
		$apply = $this->applyTempMasterFile($table_name, $table_headers_line, 'cache');
		$this->empty_folder($this->dbFolder."temp");
		if ($sendNotifications){
			global $module;
			require_module(['permissions', 'users', 'pulses']);
			$usersToBeNotified = [];
			$rolesWithAccess = [];
			$rolesWithAccessRows = $module['permissions']->listWhere([ $this->table_schema[$table_name]['permission_required'] ]);
			foreach($rolesWithAccessRows as $row){
				array_push($rolesWithAccess, $row['role']);
			}
			if ($this->table_schema[$table_name]['filter_content'] == 'true'){
				$filter_content_variable = $this->table_schema[$table_name]['filter_content_by_variable'];
				foreach($apply['generatedFiles'] as $userVar){
					$users = ($module['users']->getUsersBy($filter_content_variable, $userVar));
					foreach ($users as $row){
						if (in_array($row['role'], $rolesWithAccess))
						array_push($usersToBeNotified, $row['client_code']);
					}
				}
			}else{
				$users = $module['users']->listAll();
				foreach($users as $row){
					if (in_array($row['role'], $rolesWithAccess))
						array_push($usersToBeNotified, $row['client_code']);
				}
			}
			// emmit notification to the $usersToBeNotified
			$nicename = $this->table_schema[$table_name]['nicename'];
			$icon = $this->table_schema[$table_name]['icon'];
			$notification_data = [
				"body" => "The data for the table $nicename has been updated. Click here to explore this table.",
				"meta" => [
					"icon" => $icon,
					"link" => $this->config['base_url'].'/dataview/'.$table_name
				]
			];
			foreach($usersToBeNotified as $client_code){
				$pulse_data = $notification_data;
				$pulse_data['receiver'] = $client_code;
				$module['pulses']->send($pulse_data);
			}
		}
		return ["message"=>"Success"];
	}
	function getData($table_name, $userVar=null){
		$fileAddr = $this->dbFolder.$table_name.".csv";
		if (file_exists($fileAddr) && file_get_contents($fileAddr) != ''){
			return json_encode($this->parseFile($fileAddr, 1));
		}else{
			return false;
		}
	}
	function get_data_of($table_name, $folder, $userVar=null){
		$fileAddr = $this->dbFolder.$folder."/".$table_name."/";
		if ($this->table_schema[$table_name]["filter_content"] == 'true'){
			$fileAddr .= $userVar.".json";
		}else{
			$fileAddr .= "_universal.json";
		}
		if (file_exists($fileAddr) && file_get_contents($fileAddr) != ''){
			return file_get_contents($fileAddr);
		}else{
			return false;
		}
	}
}
$module['dataview'] = new DataView();