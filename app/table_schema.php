<?php 
/*
check DOCS/table_schema.md for reference on how to configure this.
 */
$table_schema = [
	"holdings" => [
		"nicename" => "Holdings",
		"icon" => "fa fa-database",
		"permission_required" => "view_table_holdings",
		"exclude_columns" => ["Branch", "SB", "Client", "Client_Name", "ISIN", "Script Code", ""],
		"filter_content" => true,
		"filter_content_by_variable" => "client_code",
		"filter_content_column" => "Client"
	],
	"ledgers" => [
		"nicename" => "Ledgers",
		"icon" => "fa fa-database",
		"permission_required" => "view_table_ledgers",
		"exclude_columns" => ["Client", "Client Name", "Client Type", "Priority", "Report type", ""],
		"filter_content" => true,
		"filter_content_by_variable" => "client_code",
		"filter_content_column" => "Client"
	],
	"openpositions" => [
		"nicename" => "Open Positions",
		"icon" => "fa fa-database",
		"permission_required" => "view_table_openpositions",
		"exclude_columns" => ["Party Code", "Inst Type", ""],
		"filter_content" => false
	]
];