<?php 
$template['header'] = isset($template['header']) ? $template['header'] : "Messages";
 ?>
<style>
@media screen and (min-width: 760px){
 /*** Keep the chat contacts always visible in this screen size */
 .direct-chat-contacts {
     width: 360px!important;
     transform: translate(0%, 0)!important;
 }
 [data-widget="chat-pane-toggle"]{
     display: none;
 }
 .direct-chat-messages{
     width: calc(100% - 360px);
     transform: translate(360px, 0);
 }
 	/*also, add a little height*/
  .direct-chat-contacts,.direct-chat-messages{
 	 height: 325px;
  }
}
</style>
<div class="box box-primary direct-chat direct-chat-primary" style="max-width:1080px">
	<div class="box-header with-border">
		<h3 class="box-title">Direct Chat</h3>
		<div class="box-tools pull-right">
			<span data-toggle="tooltip" title="New Messages" class="badge bg-green" showUnreadChats></span>
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div><!-- /.box-header -->
	<div class="box-body">
		<!-- Conversations are loaded here -->
		<div class="direct-chat-messages">
			<!-- Messages Default to the left -->
			<div id="target-chat">
			</div>
			<!-- The input to submit chats -->
		</div><!--/.direct-chat-messages-->
			

		<!-- Contacts are loaded here -->
		<div class="direct-chat-contacts">
			<div id="target-new-chat">
				
			</div>
		</div><!-- /.direct-chat-pane -->
	</div><!-- /.box-body -->
	<div class="box-footer">
		<div id="target-chat-box">
				<div class="px1 py1 text-center">Click on a contact to start a conversation</div>
		</div>
	</div><!-- /.box-footer-->
</div><!--/.direct-chat -->

<script template="list-contacts" type="text/x-handlebars-template">
	<ul class="contacts-list">
		{{#each .}}
		<li>
			<a href="#/{{client_code}}" open-chat="{{client_code}}" class="block pointer" style="height: 40px;">
				<img class="contacts-list-img" src="<?php echo $config['base_url'] ?>/assets/svg/person.svg" alt="Contact Avatar">
				<div class="contacts-list-info">
					<span class="contacts-list-name">
						{{name}}
						<small class="contacts-list-date pull-right">2/28/2015</small>
					</span>
				</div><!-- /.contacts-list-info -->
			</a>
		</li><!-- End Contact Item -->
		{{/each}}
	</ul><!-- /.contacts-list -->
</script>

<script template="chat-window" type="text/x-handlebars-template">
		{{#xif " this.chats.length == 10 "}} <div class="direct-chat-msg  older btn btn-block btn-flat btn-default">All messages</div>{{/xif}}
		{{#each chats}}
			{{#xif "this.sender_code == '<?php echo $_SESSION['user']['client_code'] ?>'"}}
			<div class="direct-chat-msg right">
				<div class="direct-chat-info clearfix">
					<span class="direct-chat-name pull-right">{{sender}}</span>
					<span class="direct-chat-timestamp pull-left">{{sent_at}}</span>
				</div><!-- /.direct-chat-info -->
				<img class="direct-chat-img" src="<?php echo $config['base_url'] ?>/assets/svg/person.svg" alt="message user image"><!-- /.direct-chat-img -->
				<div class="direct-chat-text">{{body}}</div><!-- /.direct-chat-text -->
			</div><!-- /.direct-chat-msg -->
			{{else}}
			<div class="direct-chat-msg ">
				<div class="direct-chat-info clearfix">
					<span class="direct-chat-name pull-left">{{sender}}</span>
					<span class="direct-chat-timestamp pull-right">{{sent_at}}</span>
				</div><!-- /.direct-chat-info -->
				<img class="direct-chat-img" src="<?php echo $config['base_url'] ?>/assets/svg/person.svg" alt="message user image"><!-- /.direct-chat-img -->
				<div class="direct-chat-text">{{body}}</div><!-- /.direct-chat-text -->
			</div><!-- /.direct-chat-msg -->
			{{/xif}}
			
		
		{{/each}}
</script>
<script template="chat-box" type="text/x-handlebars-template">
	<form endpoint="chats/newMessage.json" id="send-chat" class="input-group">
			<input type="hidden" name="ClC" value="{{data.ClC}}">
			<textarea name="body" placeholder="Type Message ..." rows="1" class="form-control"></textarea>
			<span class="input-group-btn" style="vertical-align:top">
				<button type="submit" class="btn btn-primary btn-flat">Send</button>
			</span>
	</div>
</script>


<?php footer_section_start(); ?>
<script src="<?php echo $config['base_url'] ?>/assets/js/pages/messages.js"></script>
<?php footer_section_end(); ?>