	function generate_list(){
		ajaxNvars.requestNrender({
			endpoint: 'bulkmail/emailTemplates',
			data: {"endpoint":"list"},
			template: 'template-list',
			selector: "#template-list"
		},
		function(data){			
			virtualForm.create('#sendForm', function(data){
				console.log(data);
				ajaxNvars.renderTemplate('onSend', data, '#onSend');
				$('[CLOSETHIS]').click(function(){
					$('#onSend').html('');
				})
			})
		});
	}
	generate_list();
	$('#saveSettings').click(function(){
		var email_list = $('#email_list').val(),
		template = ($('[name="template"]:checked').val() || ""),
		subject = $('[name="subject"]').val();
		var json  ={
			email_list: email_list,
			template: template,
			subject: subject
		}
		var data = JSON.stringify(json);
		var url = 'data:text/json;charset=utf8,' + encodeURIComponent(data);
		window.open(url, '_blank');
		window.focus();
	});

	$('#retrieveSettings').change(function(){
		var file = document.getElementById("retrieveSettings").files[0];
		if (file) {
		    var reader = new FileReader();
		    reader.readAsText(file, "UTF-8");
		    reader.onload = function (evt) {
		    	try{
		    		var data = JSON.parse(evt.target.result);
		    	}catch(e){
		    		show_message("Invalid file format");
		    	}
		    	$('#email_list').val(data.email_list).blur();
		    	$('[name="template"][value="'+data.template+'"]').click();
		    	$('[name="subject"]').val(data.subject);
		    	show_message("data imported succesfully", {duration:5000})
		    }
		    reader.onerror = function (evt) {
		        show_message("error reading file");
		    }
		}
	});

	$('#email_list').blur(function(){
		try{
			var emailListData = parseEmailList( $('#email_list').val() );
			virtualForm.data = {"to": emailListData };
			$('#Message').remove();
			$('#email_list').attr('parsed', 1);
		}catch(e){
			show_message("could not parse recipient's data from textarea: <br>" + e.toString());
			$('#email_list').attr('parsed', 0);
		}

	})

var parseEmailList = (function() {
	function validateEmail(email) {
		// http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
	function wrongEmailError(wrongEmail, text) {
		var index = text.indexOf(wrongEmail);
		var lineCount = (text.substring(0, index).match(/\n/g) || []).length + 1;
		return new SyntaxError('"'+wrongEmail+'" could not be validated as a proper email. [line '+lineCount+'].');
	}
	return function parseEmailList(text) {
		var emailList = new Array();
		text.split(';').forEach(function(entry, i) {
			entry  = entry.trim().replace(/\n/g, '');
			if(entry == '') return;
			if(entry.indexOf(':')==-1)
				entry = ['', entry];
			else
				entry = entry.split(':').map(function(val) {return val.trim();});
			if( !validateEmail(entry[1]) )
				throw wrongEmailError(entry[1], text);
			emailList.push({toname: entry[0], toemail: entry[1]});
		});	
		return emailList;
	}
})();