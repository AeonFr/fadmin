# Email templating

Templates are simply .php files located in `includes/email_templates`.

Templates located in that folder that start with an underscore (_) should not be deleted, but can be modified.

> All templates are written in plain HTML. Yet, since sending HTML emails with proper format is an art (options like CSS styles, webfonts, layout, backgrounds are a pain in some email clients), it's recommended to either create simple, almost "rough text" templates, or to get a proper **HTML email template**.

## Variables inside templates

Inside a template, there is a variable available known as `$var`.  
This variable includes all of the configuration variables in `$var['config']`.  

So, if you want to echo the web page's title inside a template, you could do so by calling this chunk of code:

> PLEASE, View this file directly from the "/DOCS" folder if you want to copy the code. Code is outputting badly on the online documentation

```
<?php echo $vars['config']['page_title'] ?>
```

You can create any variable you want inside the file `config.php`, in the `$config` object, to make it instantly available in any email template.  

Yet the `$vars` object also contains the information relative to that unique transaction.  
This data is located in an object that looks like this:

```
$vars['data'] = [
	"to_email" => "email1@email.com",
	"to_name" => "Sample user",
	"subject" => "subject of the message", // This can be over-written inside the template!
	"template_vars" => [ #... ], // This can contain any custom variables or can be empty. Variables set here are "wiped out" and DOESN'T DISPLAY in the registry of transactions, so this variable is ideal to pass sensitive data to a template (like passwords).
	"custom_key" => "custom_value"
];
```

## Email subject

To overwrite the subject of an email inside a template, declare the `$subject` variable anywhere in the template.

```
<?php
// This subject will affect all emails send with this template. 
$subject = "Hi ". $vars['data']['to_name'] . ", this is your weekly update.";

/*
PHP random inconsistency/bug of the day:
"To output variables inside PHP strings, you can use " . $vars['data']['this_syntax'] . "!!! USING $vars[data][this_syntax] will throw an Error and email won't be send";
*/
?>
``` 

## Unsuscribe Links

The following code will output unsuscribe links. Please consider this:
- Is a very good practice to include this unsuscribe links. People will choose to mark emails as SPAM if you don't, and that will cause a bad reputation for the email account and further emails won't reach properly to future users.
- Following this links will ALLWAYS modify this settings for YOUR account if you have signed in.
If you happen to want to unsuscribe another user, follow the link after signing out.
- This code may output "bugged" in the online documentation viewer. Please open this file directly to see the code (sorry)
- The page that the links follows also allows the user to make other changes in the email notifications. In fact, it's the same page you can access by clicking "my account" > "email settings"

<blockquote>Following unsubscribe links from the "preview" box in the logs of outbound email will <b>only unsubscribe yourself</b>, since you are the logged in user and that takes preference over any variable sent with the url.</blockquote>

```
LINK TO UNSUSCRIBE FROM THE "MAILING LIST" (bulkmails)
<?php echo $vars['config']['full_url']	?>/API/email-settings/?ref=<?php echo md5($vars['data']['to_email']) ?>&unsuscribe_from_mailing_list=true
LINK TO UNSUSCRIBE FROM (automatic) EMAILS THAT INFORM OF UNREAD NOTIFICATIONS
<?php echo $vars['config']['full_url']	?>/API/email-settings/?ref=<?php echo md5($vars['data']['to_email']) ?>&disable_notifs_by_mail=true
LINK TO UNSUSCRIBE FROM (automatic) EMAILS THAT INFORM OF UNREAD PRIVATE MESSAGES
<?php echo $vars['config']['full_url']	?>/API/email-settings/?ref=<?php echo md5($vars['data']['to_email']) ?>&disable_chats_by_mail=true
```

- You can also manually *unsuscribe* a user from this kind of emails by **adding** a new column in the `user_meta` table. You can also *re-suscribe* a user by deleting that entry from the table.

| client_code | meta_key | meta_value
| --- | --- | ---
| ... | unsuscribe_from_mailing_list | true
| ... | disable_notifs_by_mail | true
| ... | disable_chats_by_mail | true

## Unsubscribe links for unregistered users.

Since the "bulkmail" program allows you to send mails to unregistered users, a small layer was built on top of the existing system to make it compatible. It's architectured as follows:

1. Unsubscribe links that starts with <code><?php echo $vars['config']['full_url']	?>/API/email-settings/?ref=<?php echo md5($vars['data']['to_email']) ?></code> are still valid to unsubscribe any user (the rest of the link is not important in this case), by email. The difference is that if the user is not registered to the page, the data is stored in a no-SQL database located at `_noSQL/unsubscribes.json`. It's recommended to send the full unsubscribe url: `<?php echo $vars['config']['full_url'] ?>/API/email-settings/?ref=<?php echo md5($vars['data']['to_email']) ?>&unsuscribe_from_mailing_list=true&disable_notifs_by_mail=true&disable_chats_by_mail=true` - This will cover all cases for both registered and unregistered users. An abreviation of this url was created for simplicity: `<?php echo $vars['config']['full_url'] ?>/API/email-settings/?ref=<?php echo md5($vars['data']['to_email']) ?>&unsubscribeFromAll`
2. If an unsubscribed user is registered on the page (after he was unsubscribed), he will start appearing as subscribed again. This might potentially be annoying to some users, so keep it in mind.


## That's it!

Happy *templating*!