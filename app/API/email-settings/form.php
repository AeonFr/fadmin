<?php
function echo_setting_s_checkbox($setting_name){
	global $user_settings;
	?>
	<input type="hidden" <?php if ($user_settings[$setting_name]) echo 'checked' ?> name="<?php echo $setting_name ?>" value="true">
	<input type="checkbox" <?php if (!$user_settings[$setting_name]) echo 'checked' ?> name="<?php echo $setting_name ?>" value="">
	<?php
}

?>
<style>.block{display:block;}</style>
<?php if (isset($SUCCESS_MESSAGE)): ?>
<div class="alert alert-info">
	<?php echo $SUCCESS_MESSAGE ?>
</div>
<?php endif; ?>
<a href="<?php  echo $config["base_url"].'/my-account?source=email-settings' ?>">←Back to MY ACCOUNT</a> · <a href="<?php  echo $config["base_url"].'/?source=email-settings' ?>">HOME</a>
<h3>Manage email settings</h3>
<h4 class="text-primary">Change email preferences for <em><?php echo $userData['email'] ?></em></h4>
<form class="box-body" method="post" action="<?php echo "$config[base_url]/API/email-settings/" ?>">
	<input type="hidden" name="ref" value="<?php echo md5($userData['email']) ?>">
	<label class=" block form-group">
		<?php echo_setting_s_checkbox("disable_notifs_by_mail") ?>
		Send me an email when I receive a new <strong>notification</strong>.
	</label>
	<label class=" block form-group">
		<?php echo_setting_s_checkbox("disable_chats_by_mail") ?>
		Send me an email when I receive a new <strong>private message</strong>.
	</label>
	<div class="form-group">These emails are only sent if you're <strong>not</strong> online when receiving the notification/chat.</div>
	<label class="block form-group">
		<?php echo_setting_s_checkbox("unsuscribe_from_mailing_list") ?>
		Include me in the mailing list.
	</label>
	<div class="form-group">Check this to receive emails that the administrator of the site considers relevant for you. You can unsubscribe at any time.</div>
	<button type="submit" class="btn btn-flat btn-default">Update</button>
</form>