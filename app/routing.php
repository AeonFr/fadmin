<?php 
require_once 'config.php';
/**
 * This is a file that enroutes all the requests according to the url.
 * This means that a request to "/app/users/" can in fact render the page
 * "/app/pages/users/index.php" (e.g.)
 * A routing system is used for security (don't expose app folder structure)
 * and simplicity (nice urls)
 * Please, DO read the documentation of the used rooting system
 * https://github.com/nikic/FastRoute
 * NOTE: This file works in conjunction with the .htaccess file, only in Apache Servers!!!
 * (An implementation on nigix is possible, but not covered nor tested).
*/
require "$config[root]/vendor/fast-route/autoload.php";
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
    global $config;
    /**
     * *********************************************************************************************
     */
    $r->addRoute('GET', $config['base_url'].'/', 'index');
    $r->addRoute('GET', $config['base_url'].'/sign-out', 'sign-out');
    $r->addRoute('GET', $config['base_url'].'/privacy-policy', 'privacy-policy');
    $r->addRoute('GET', $config['base_url'].'/my-account', 'my-account');
    $r->addRoute('GET', $config['base_url'].'/messages', 'messages');
    $r->addRoute('GET', $config['base_url'].'/notifications', 'notifications');
    $r->addRoute('GET', $config['base_url'].'/dataview/{table}', 'dataview');
    // /admin && /admin/subpage
    $r->addRoute('GET', $config['base_url'].'/admin[/{subpage:.+}]', 'admin');
    // /filebase
    $r->addRoute('GET', $config['base_url'].'/filebase/[{filepath:.+}]', 'filebase');
    // /admin/documentation -> iframe
    $r->addRoute('GET', $config['base_url'].'/DOCS-ROUGH[/{document_name:.+}]', 'DOCS');
    /**
     * *********************************************************************************************
     */
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        http_response_code(404);
        include "$config[root]/layout.php";
        include "$config[root]/pages/404.html";
        exit;
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        http_response_code(405);
        echo 'Error 405: Method Not Allowed';
        // ... 405 Method Not Allowed
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        /**
         * *****************************************************************************************
         */
        global $config;
        $layout = []; //an object containing options to render the layout
        ob_start();
        switch ($handler) {
            case 'sign-out':
                // log user out
                global $db;
                global $auth;
                $auth->log_out();
                exit();
                break;
            case 'index':
                include "$config[root]/pages/index.php";
                break;
            case 'privacy-policy':
                $template['header'] = "Privacy Policy";
                include "$config[root]/pages/privacy-policy.html";
                break;
            case 'my-account':
                include "$config[root]/pages/my-account.php";
                break;
            case 'admin':
                $template['header'] = "Admin Panel";
                $file = (isset($vars['subpage'])) ? $vars['subpage'] : 'index';
                include "$config[root]/pages/admin/$file.php";
                break;
            case 'messages':
                include "$config[root]/pages/messages.php";
                break;
            case 'notifications':
                include "$config[root]/pages/notifications.php";
                break;
            case 'dataview': 
                include "$config[root]/pages/dataview.php";
                break;
            case 'DOCS':
                if (!has_permission('view_documentation')){
                    die("Sorry, You don't have permission to access this url. That's all I can tell you.");
                }
                ob_end_flush();
                include "$config[root]/DOCS/index.php";
                exit();
                break;
            default:
                # code...
                break;
        }
        $layout['content'] = ob_get_clean();
        include "layout_adminLTE.php";
        /**
         * *****************************************************************************************
         */
        break;
}