<?php include "./config.php"; ?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Single Page Documentation</title>
	<!-- BassCss -->
	<link rel="stylesheet" href="http://d2v52k3cl9vedd.cloudfront.net/basscss/7.0.4/basscss.min.css">
	<!-- Anonymous Pro - Google Fonts 
	<link href='https://fonts.googleapis.com/css?family=Anonymous+Pro' rel='stylesheet' type='text/css'> -->
	<!-- Estilos para bloques de código
	(en markdown, 
	`` => <code></code>
	y
	``` ``` => <pre><code> </code></pre>) -->
	<style>
		pre > code{
			font-size: 1rem;
		}
		pre{
			background-color: #212021;
			color: #e1e1e1;
		}
		pre, code{ border: 1px solid silver; }
		pre{
			font-family: 'Anonymous Pro', Monaco, PT mono, monospace;
			padding: 1em; overflow: auto;
			white-space: pre-wrap;
		}
		pre > code{  border: none; }
		h3{
			border-bottom: 1px solid silver;
			font-family: 'Anonymous Pro', Monaco, PT mono, monospace;
		}
		ul{
			padding-left: 1em;
		}
		@media screen and (min-width: 52em){
			html{
				font-size: 18px;
			}
			#navigation{
				position: fixed;
				max-width: 100vh;
				bottom: 0;
				top: 0;
				overflow-y: auto;
			}
			#navigation + .col{
				margin-left: 33.3333%;
			}
			#navigation .active>a{
				background-color: yellow;
			}
		}
		</style>
	<!--jQuery-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<!-- Marked -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.5/marked.min.js"></script>
	<!-- Highlight.js -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.0.0/styles/monokai.min.css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.0.0/highlight.min.js"></script>
	<!-- Auto anchor-link -->
	<script src="<?php echo $config['base_url'] ?>/assets/js/anchorific.js"></script>
	<script>
	var defaultHighlightLanguage = 'javascript'; // Leave NULL ('') to let Highlight.js auto-detect the languaje
	// Let marked do its normal token generation.
	window.onload = function() {

		marked.setOptions({
			gfm: true,
			highlight: function (code) {
				return hljs.highlightAuto(code).value;
			},
			sanitize: true
		});
		$('#markdown').html( marked( $('#markdown').html() ) );
		
		// auto-crear anchor links
		$('body').anchorific({
		    navigation: '.anchorific', // position of navigation
		    speed: 200, // speed of sliding back to top
		    anchorClass: 'anchor', // class of anchor links
		    anchorText: '#', // prepended or appended to anchor headings
		    top: '.top', // back to top button or link class
		    spy: true, // scroll spy
		    position: 'prepend', // position of anchor text
		    spyOffset: 250 // specify heading offset for spy scrolling
		});
		$('select#files').change(function(){
			var value = $(this).val();
			window.location.href=value;
		})
		window.setTimeout(function(){
			var escapeUglyChars = replaceAll($('#markdown').html(), '&amp;<span class="hljs-keyword">gt</span>;', '>');
			$('#markdown').html(escapeUglyChars);
		},100)
	}
	function replaceAll(str, find, replace) {
	  return str.replace(new RegExp(find, 'g'), replace);
	}
	</script>
</head>
<body class="clearfix">
<div class="col md-col-4 px2" id="navigation">
	<div class="mxn2 border-bottom p2">
		<select name="iterator" id="files">
		<?php $dir = new DirectoryIterator(dirname(__FILE__));
		foreach ($dir as $fileinfo):
		    if (!$fileinfo->isDot() && $fileinfo->getFilename() !== 'index.php') {
		        ?><option value="<?php echo $fileinfo->getFilename() ?>"
				<?php if ($fileinfo->getFilename() == $vars['document_name']) echo 'selected' ?>
		        ><?php echo $fileinfo->getFilename() ?></option><?php
		    }
		endforeach; ?>
		</select>
	</div>
	<div class="mxn2 border-bottom p2" id="nav-title">
		Contents: <a href="#Top" class="top">Back to top</a>
	</div>
	<div class="anchorific"></div>
</div>
<div class="col md-col-8 p2" style="max-width:100%" id="markdown">
<?php $f = file_get_contents("$config[root]/DOCS/".$vars['document_name']);
$f_e = str_replace("?>", '?&gt;', str_replace("<?php", "&lt;?php", $f));
echo $f_e;
 ?>

</div>
