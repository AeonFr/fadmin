<?php footer_section_start(); ?>
<script template="modal-body-explore-table-schema" type="text/x-handlebars-template">
    <i class="block">Icon+Nicename: (the table as it will be displayed to users):</i>
    <h3><i class="{{icon}}"></i> {{nicename}}</h3>
    <hr>
    <i class="block">Permission required to view the data from this table:</i>
    <h4>{{permission_required}}</h4>
    <hr>
    {{#if filter_content}}
    <i class="block"><i class="fa fa-check-circle"></i> Filter the content is active.</i>

    <h4>The content from the uploaded .csv file will be filtered by the data from the column <code>{{filter_content_column}}</code>, when it matches the column <code>{{filter_content_by_variable}}</code> from the user's table.</h4>
    {{else}}
    <i class="block">Filter content is not active.</i>
    {{/if}}
    <hr>
    Excluded columns:
    <ul>
        {{#each exclude_columns}}
        <li>{{.}}</li>
        {{/each}}
    </ul>
    <hr>
    <small class="block">This data is retrieved from the contents of <code>table_schema.php</code>. Check the documentation for explanations on how to modify this values.</small>
</script>
<?php footer_section_end(); ?>